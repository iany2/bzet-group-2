def find_shortest_path_matrix(graph, start, end, path=[]):
        path = path + [start]
        if start == end:
            return path
        if start not in graph.list_nodes():
            return None
        shortest = None

        for node in graph.list_neighbors(start):
            if node not in path:
                newpath = find_shortest_path_matrix(graph, node, end, path)
                if newpath:
                    if not shortest or len(newpath) < len(shortest):
                        shortest = newpath
        return shortest



