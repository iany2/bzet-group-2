
from graphClass import *
from dijkstraMatrix import *

from bzGraphProf import *
from dijkstraProf import *

from bzGraphGroup import *
from dijkstraGroup import *

import random
import time


print("\nCreating test data...")
bzGraphProf = bzGraph()
matrixGraph = graph()
bzGroup = bzGraphGroup()

size = 100

#create graph with path from start to end

for node in range(0,size) :
        bzGraphProf.add_node(node)
        matrixGraph.add_node(node)
        bzGroup.add_node(node)
        # path from start to end

# Connect nodes randomly
random.seed()
for node in range(0,size) :
        node1 = random.randint(0, size-1)
        node2 = random.randint(0, size-1)
        bzGraphProf.add_edge(node1, node2)
        matrixGraph.add_edge(node1, node2)
        bzGroup.add_edge(node1,node2)

#show the graph
print(matrixGraph.list_graph())

print("\nRunning performance tests...\n")

startNode = random.randint(0, size-1)
endNode = random.randint(0, size-1)

print("Finding path from arbitrary starting node %i to" % startNode)
print("ending node %i... \n" % endNode)

start = time.clock()
print(find_shortest_path_matrix(matrixGraph, startNode, endNode))
t1 = (time.clock() - start)
print("Dijkstra's Algorithm on the regualar adjacency matrix took %f seconds\n" % t1)

start = time.clock()
print(find_shortest_path_group(bzGroup,startNode,endNode))
t3 = (time.clock() - start)
print("Dijkstra's Algorithm on a bzet adjacency matrix using our code took %f seconds\n" % t3)

start = time.clock()
print(find_shortest_path_prof(bzGraphProf,startNode, endNode))
t2 = (time.clock() - start)
print("Dijkstra's Algorithm on a bzet adjacency matrix using the professors code took %f seconds" % t2)

      
