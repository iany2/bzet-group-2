import random

def randomBytes(length,  sparseness):
    
    b = bytearray()
    i = 0
    bitType = random.randint(0,1)
    
    while i < length:
        if random.randint(0,100) > sparseness:
            bitType = bitType ^ 1
            b.append(0x00 + random.randint(0,255))
        elif (bitType == 0):
            b.append(0x00) 
        else:
            b.append(0xFF)
        i+=1
    
    return bytes(b)
