
from BZet import *
from binopCheckBytes import *
from randomBytes import *
from bitsv09 import *
from bits_extension import *
import random
import time

#create two random byte arrays.
#The first argument is the size of the array
#and the second argument is an integer between 0
# and 100 which determines how sparse the array 
#data is.  0 is least sparse, and 100 is most sparse.

print("\nCreating test data...")

random.seed()
len = 1000 * 100
for i in range(21):
	sparse = i * 5
	b1 = randomBytes(len, sparse)
	b2 = randomBytes(len, sparse)
    
	print("\nCompressing data...")
  
	#compress arrays into bzet form
	bzet1 = BZet()
	bzet1.CreateBZetFromArray(b1, len)

	bzet2 = BZet()
	bzet2.CreateBZetFromArray(b2, len)

	#compress arrays into bits form
	bits1 = createFromByteArray(b1)
	bits2 = createFromByteArray(b2)

	#do performance test AND
	print("\nRunning performance tests...\n\nOperation: AND\nSparseness: %d\n" % sparse)

	start = time.clock()
	bzetTest = bzet1.AND(bzet2)
	t1 = (time.clock() - start)
	compress = (bzetTest.Size() / len) * 100
	print("C code compression is at %f percent" % compress)
	print("C code took %f seconds\n" % t1)
	
	start = time.clock()
	bitsTest = bits1.AND(bits2)
	t2 = (time.clock() - start)
	compress = (bitsTest.size() / len) * 100
	print("Python code compression is at %f percent" % compress)
	print("Python code took %f seconds" % t2)

	if (t1 == 0):
		t1 = .000001
	print("C code ran %f times as fast" % (t2 / t1))

	#do performance test OR
	print("\nRunning performance tests...\n\nOperation: OR\nSparseness: %d\n" % sparse)

	start = time.clock()
	bzetTest = bzet1.OR(bzet2)
	t1 = (time.clock() - start)
	compress = (bzetTest.Size() / len) * 100
	print("C code compression is at %f percent" % compress)
	print("C code took %f seconds\n" % t1)

	start = time.clock()
	bitsTest = bits1.OR(bits2)
	t2 = (time.clock() - start)
	compress = (bitsTest.size() / len) * 100
	print("Python code compression is at %f percent" % compress)
	print("Python code took %f seconds" % t2)

	if (t1 == 0):
		t1 = .000001
	print("C code ran %f times as fast" % (t2 / t1))
	
	#do performance test NOT
	print("\nRunning performance tests...\n\nOperation: NOT\nSparseness: %d\n" % sparse)

	start = time.clock()
	bzetTest = bzet1.Invert()
	t1 = (time.clock() - start)
	compress = (bzetTest.Size() / len) * 100
	print("C code compression is at %f percent" % compress)
	print("C code took %f seconds\n" % t1)

	start = time.clock()
	bitsTest = bits1.NOT()
	t2 = (time.clock() - start)
	compress = (bitsTest.size() / len) * 100
	print("Python code compression is at %f percent" % compress)
	print("Python code took %f seconds" % t2)

	if (t1 == 0):
		t1 = .000001
	print("C code ran %f times as fast" % (t2 / t1))
	
	#do performance test XOR
	print("\nRunning performance tests...\n\nOperation: XOR\nSparseness: %d\n" % sparse)

	start = time.clock()
	bzetTest = bzet1.XOR(bzet2)
	t1 = (time.clock() - start)
	compress = (bzetTest.Size() / len) * 100
	print("C code compression is at %f percent" % compress)
	print("C code took %f seconds" % t1)
	
	#do performance test NAND
	print("\nRunning performance tests...\n\nOperation: NAND\nSparseness: %d\n" % sparse)

	start = time.clock()
	bzetTest = bzet1.NAND(bzet2)
	t1 = (time.clock() - start)
	compress = (bzetTest.Size() / len) * 100
	print("C code compression is at %f percent" % compress)
	print("C code took %f seconds" % t1)
	
	#do performance test NOR
	print("\nRunning performance tests...\n\nOperation: NOR\nSparseness: %d\n" % sparse)

	start = time.clock()
	bzetTest = bzet1.NOR(bzet2)
	t1 = (time.clock() - start)
	compress = (bzetTest.Size() / len) * 100
	print("C code compression is at %f percent" % compress)
	print("C code took %f seconds" % t1)
	
	#do performance test XNOR
	print("\nRunning performance tests...\n\nOperation: XNOR\nSparseness: %d\n" % sparse)

	start = time.clock()
	bzetTest = bzet1.NOTXOR(bzet2)
	t1 = (time.clock() - start)
	compress = (bzetTest.Size() / len) * 100
	print("C code compression is at %f percent" % compress)
	print("C code took %f seconds" % t1)

	
"""
NUMBYTES = 100
blist1 = []
blist2 = []
for i in range(0, NUMBYTES):
		blist1.append(random.randint(0x00, 0xFF))
		blist2.append(random.randint(0x00, 0xFF))
	
x1 = bytes(blist1)
x2 = bytes(blist2)
"""
