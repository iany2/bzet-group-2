#include "stack.h"
#include <stdlib.h>



//Stack BYTE implementation

void StackInit(Stack *s) {

	s->top = NULL;
	s->size = 0;
}

int StackPush(Stack* s, BYTE value) {
	StackNode* newTop;
	newTop = NULL;
	s->size++;
	newTop = (StackNode*) malloc( sizeof(StackNode) );
	if ( newTop == NULL ) {
		return 0;
	}
	else {
		newTop->previous = s->top;
		newTop->data = value;
		s->top = newTop;
		return 1;
	}
}

int StackPop(Stack* s, BYTE* destination) {
	StackNode* temp;
	if ( s->size == 0 || destination == NULL ) {
		return 0;
	}
	else {
		*destination = s->top->data;
		temp = s->top;
		s->top = s->top->previous;
		free( temp );
		s->size--;
		return 1;
	}
}

void StackDestruct(Stack *s) {
	StackNode* current;
	StackNode* temp;
	current = s->top;
	while ( current != NULL ) {
		temp = current->previous;
		free ( current );
		current = temp;
	}
	s->size = 0;
}

/*BYTE StackByteAccess(const StackByte *s, int n) 
{
	int x;
	StackByteNode* current;
	current = s->top;
	for (x = 0; x < s->size; x++) {
		if ( n == x )
			return current->data;
		else
			current = current->previous;
	}

	return current->data;		
}*/


void dataStackInit(dataStack *s)
{
	s->top = 0;
	s->size = 0;
}

int dataStackPush(dataStack* s, int p, int lev, int sub)
{
	dataStackNode* newTop;
	newTop = (dataStackNode*) malloc( sizeof(dataStackNode) );
	if ( newTop == NULL ) {
		return 0;
	}
	else {
		newTop->previous = s->top;
		newTop->pos = p;
		newTop->newLev = lev;
		newTop->subTrees = sub;
		s->top = newTop;
		s->size++;
		return 1;
	}
}

dataStackNode* dataStackPop(dataStack* s)
{
	dataStackNode* temp;
	if ( s->size == 0 ) {
		return 0;
	}
	else {
		temp = s->top;
		s->top = s->top->previous;
		s->size--;
		return temp;
	}
}

void dataStackDestruct(dataStack *s)
{
	dataStackNode* current;
	dataStackNode* temp;
	current = s->top;
	while ( current != NULL ) {
		temp = current->previous;
		free ( current );
		current = temp;
	}
	s->size = 0;
}

dataStackNode* dataStackTop(const dataStack *s)
{
	return s->top;
}



//Implementation of Array of Stacks

/*void StackArrInit(StackArr *s) {

	s->top = NULL;
	s->size = 0;

}

int StackArrPush(StackArr* s, int* value) {
	StackArrNode* newTop;
	newTop = NULL;
	s->size++;
	newTop = (StackArrNode*) malloc( sizeof(StackArrNode) );
	if ( newTop == NULL ) {
		return 0;
	}
	else {
		newTop->previous = s->top;
		newTop->data = value;
		s->top = newTop;
		return 1;
	}
}

int StackArrPop(StackArr* s, int** destination) {
	StackArrNode* temp;
	if ( s->size == 0 || destination == NULL ) {
		return 0;
	}
	else {
		*destination = s->top->data;
		temp = s->top;
		s->top = s->top->previous;
		free( temp );
		s->size--;
		return 1;
	}
}

void StackArrDestruct(StackArr *s) {
	StackArrNode* current;
	StackArrNode* temp;
	current = s->top;
	while ( current != NULL ) {
		temp = current->previous;
		free ( current );
		current = temp;
	}
	s->size = 0;
}

int* StackArrAccess(const StackArr *s, int n) 
{
	int x;
	StackArrNode* current;
	if (n >= s->size)
		return 0;
	current = s->top;
	for (x = 0; x < s->size; x++) {
		if ( n == x )
			return current->data;
		else
			current = current->previous;
	}

	return &(current->data);		
}*/