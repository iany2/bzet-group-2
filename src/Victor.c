
#include "Victor.h"

//Work function
//returns starting position of next subtree
//
//Parameters:
//bs is the bitset
//p is the index in the bitset for this node/data
//lev is the level in the tree the node is at
//ep is the element nuber(which branch of tree is the element? 0-7)
//val is the index value of the 1st bit represented

//actn and actd are functions that are specified by the calling function
// the total list of possible functions that can be invoked are
//get_node, get_data, not_n, not_d, dust_n, count_n, count_d,
//fbit_n, fbit_d, last_n, last_d 
//All of these functions must be able to be called by bznWork
//perhaps we should use a switch statement selection function to choose
// the correct functions for the cases

int bznSize(bzNodes* bn){ //need the total number of bytes for various
}                         // calls in bznWork/walk

int fbit_n(bzNodes* bitset, int pos, int lev, int branch, int xval){ //need to make val static****
    //nonlocal val                                                  
    BYTE possbits = bznGetByte(bitset,pos) | bznGetByte(bitset,pos+1); 
    int fb = first_bit(possbits);
    //if(fb == NONE)
        //return NONE;
    int tbit = 0x80 >> fb;
    if(bznGetByte(bitset,pos+1) & tbit)
    {
        //Not here but in this subtree
        // save value and get next node
        xval += pow8(lev) * fb;  //
        return;  // returns as if a void function in python
    }
    else
        return xval + pow8(lev) * fb;  // val in python code ***make static***
}
int fbit_d(bzNodes* bitset, int pos, int lev, int branch, int xval){ // not sure about pass by ref here
    //xval is the position in the total bitset
    //pos is the position in the array of bytes
    //nonlocal val need to declare val as static *******
    int fb = first_bit(bznGetByte(bitset,pos));
    return xval + fb; // in python code its val need to check this


}
void count_n(bzNodes* bitset, int pos, int lev, int branch, int xval){
    //nonlocal bitc *** need to make bitc static***
    //int bitc += pow8(lev) * bit_count(bznGetByte(bitset,pos)); //need bit_count*****
    return;
}
void count_d(bzNodes* bitset, int pos, int lev, int branch, int xval){
    //nonlocal bitc   ****make bitc static****
    //int bitc += bit_count(bznGetByte(bitset,pos)); //need bit_count****
    return;
}
void dust_n(bzNodes* bitset, int pos, int lev, int branch, int val){

    BYTE treeOff = bznGetByte(bitset,pos) & !bznGetByte(bitset,pos+1) & 0xff;
    bznSetByte(bitset,pos,treeOff);
    return;
}
void last_n(bzNodes* bitset, int pos, int lev, int branch, int xval){
    //non local xbit  ***make xbit static***
    if(bznGetByte(bitset,pos) && !bznGetByte(bitset,pos+1))
    {
        //Node is a leaf node
        //calculate last bit of sequence
        int p8 = pow8(lev);
        int xbit = xval + p8 - 1; //
    }
    return;

}
void last_d(bzNodes* bitset, int pos, int lev, int branch, int xval){
    //nonlocal xbit ***make xbit static***
   // int xbit = xval + pow8(lev) * last_bit(bznGetByte(bitset,pos)); //needs last_bit
    return;
}
void not_n(bzNodes* bitset, int pos, int lev, int branch, int val){

    BYTE notNode = ~bznGetByte(bitset,pos) & 0xff;
    bznSetByte(bitset,pos,notNode);
    notNode = bznGetByte(bitset,pos) & (~bznGetByte(bitset,pos+1) & 0xff); // turn off tree bits
    bznSetByte(bitset,pos,notNode);
    return;
}
void not_d(bzNodes* bitset, int pos, int lev, int branch, int val){

    BYTE notData = ~bznGetByte(bitset,pos) & 0xff;
    bznSetByte(bitset,pos,notData);
    return;
}
void get_node(bzNodes* bitset, int pos, int lev, int branch, int val){
    //nonlocal rv, toplev,lastlev, offset, indent *** need to make these all static
   // 
}
void get_data(bzNodes* bitset, int pos, int lev, int branch, int val){
    //nonlocal rv
}





void bznWork(bzNodes* bn, int p, int lev, char actn, char actd){//p is position
                                                                //in byte
    int i;
    for(i=0; i<bznSize(bn);i++) { //for node in bits.walk
        //bznGetByte(bn,i)

    }



    
    
}




//Walk main routine
//Returns position of nodes and type for any subtree or data of bs
//Dependent on stack and stack functions
//as well as get_index_pos

//Parameters:
//bs is the bytes string encoding of the bs 
//pos is the 
//clev is the level of the tree

//ret_n is used as a bool that changes the result returned

walkNode* bznWalk(bzNodes* bn, int pos, int clev, int ret_n )
{
  // endbs = len(bs);
  // if(pos > endbs || pos < 0) 
  // printf("ERROR OUT OF RANGE");
   
  // int p = pos;   //position variable to work with
  // int tlev = clev; // current level variable to work with
   //Stack bsstack();
   int val = 0;
   int levpos = 0;
   
   
       
    
    
}