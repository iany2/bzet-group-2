#include "smokeTest.h"
#include "string.h"
#include "ctype.h"
#include <cassert>

//-----------------------------------------------------------------

BYTE charToHex(char c) {
	c = tolower(c);
	switch (c) {
	case '0': return 0x00;
	case'1': return 0x01;
	case '2': return 0x02;
	case '3': return 0x03;
	case '4': return 0x04;
	case '5': return 0x05;
	case '6': return 0x06;
	case '7': return 0x07;
	case '8': return 0x08;
	case '9': return 0x09;
	case 'a': return 0x0A;
	case 'b': return 0x0B;
	case 'c': return 0x0C;
	case 'd': return 0x0D;
	case 'e': return 0x0E;
	case 'f': return 0x0F;
	default: return 0x00;
	}
}

//-----------------------------------------------------------------

/*  Convert a string of the form [0xDD][0xDD]...
*	where D is a hex digit to an array of BYTES
*/
BYTE* strToByteArray(const char str[], int slen, int* blen) {
	int numBytes = slen / 6; //6 characters per byte
	int i;
	BYTE* bytes = (BYTE*)malloc(numBytes*sizeof(BYTE));
	*blen = numBytes;

	for (i = 0; i < numBytes; i++) {
		char leftDigit = str[6*i + 3];
		char rightDigit = str[6*i + 4];
		BYTE b = charToHex(rightDigit) | (charToHex(leftDigit) << 4);
		bytes[i] = b;
	}
	return bytes;
}

//-----------------------------------------------------------------

int _BzetEquals(Bzet* bz, BYTE* nodes, int numNodes) {

	int i;
	if (bz->numNodes != numNodes)
		return 0;
	for (i = 0; i < numNodes; i++) {
		if (bz->nodes[i] != nodes[i])
			return 0;
	}
	return 1;
}

//-----------------------------------------------------------------

/*  Returns 1 if the contents of the array pointed to
*	by bz->nodes is exactly the same as the contents of
*	nodes, and numNodes == bz->numNodes.  Else returns 0.
*/
int BzetEquals(Bzet* bz, const char nodes[]) {

	BYTE* bytes = NULL;
	int len = 0;
	int ret;
	bytes = strToByteArray(nodes, strlen(nodes), &len);
	ret = _BzetEquals(bz, bytes, len);
	free(bytes);
	return ret;
}

//-----------------------------------------------------------------

Bzet* BZcreateFromStr(const char str[]) {
	Bzet* bz = NULL;
	BYTE* nodes = NULL;
	int numNodes;

	nodes = strToByteArray(str, strlen(str), &numNodes);
	bz = BZallocate();
	bz->nodes = nodes;
	bz->numNodes = numNodes;
	bz->bufferSize = numNodes;
	return bz;
}

//-----------------------------------------------------------------

void printArray(const BYTE arr[], int len) {

	int i;
	for (i = 0; i < len; i++) {
		printf("%x\n", reverseBits(arr[i]));
	}
}

//-----------------------------------------------------------------

void fprintArray(FILE* fp, const BYTE arr[], int len) {

	int i;
	for (i = 0; i < len; i++) {
		fprintf(fp, "%x\n", reverseBits(arr[i]));
	}
}

//-----------------------------------------------------------------

void bzSmoke() {

	utilHarn();
	printf("utilHarn: all tests passed\n");
	//binopHarn();
	//printf("binopHarn: all tests passed\n");
	conversionHarn();
	printf("conversionHarn: all tests passed\n");
	bitMutatorHarn();
	printf("bitMutatorHarn: all tests passed\n");
	//rangeHarn();
	//printf("rangeHarn: all tests passed\n");
	strideHarn();
	printf("strideHarn: all tests passed\n");
}

//-----------------------------------------------------------------

void utilHarn() {
	Bzet* c = BZcreateFromStr("[0x02][0x01][0x00]");
	Bzet* a = BZcreate(64);
	Bzet* b = BZcreate(1);

	assert(BzetEquals(a, "[0x02][0x00][0x40][0x00][0x80][0x80]"));
	assert(BzetEquals(b, "[0x00][0x40]"));
	assert(BzetEquals(c, "[0x02][0x01][0x00]"));

	_BZalign(a,b);
	assert(a->nodes[0] == b->nodes[0]);
	//BZprint(a);
	//printf("\n");
	//BZprint(b);

	BZreleaseMemory(a);
	BZreleaseMemory(b);

	a = BZcreateEmpty();
	assert(a->numNodes == 2 && a->nodes[0] == 0x00 && a->nodes[1] == 0x00);

	assert(_mkbits(0,2) == 0xE0);
	assert(_mkbits(1,1) == 0x40);
	assert(_mkbits(1,2) == 0x60);
	assert(_mkbits(0,7) == 0xFF);
	BZreleaseMemory(a);

	a = _range(8,3);
	b = _range(1,2);
	assert(b->nodes[0] == 0 && b->nodes[1] == 0x60); //range in the first byte
	assert(a->nodes[0] == 1 && a->nodes[1] == 0x00 && a->nodes[2] == 0x40 && a->nodes[3] == 0xE0); //range less than 8, but not in first byte
	BZreleaseMemory(a);
	BZreleaseMemory(b);

	assert(first_bit(0x40) == 1);
	assert(first_bit(0x01) == 7);
	assert(first_bit(0x00) == NO_BIT);
	assert(first_bit(0x02) == 6);

	a = _range(7, 2);
	b = _range(0, 16);
	assert(a->nodes[0] == 1 && a->nodes[1] == 0 && a->nodes[2] == 0xC0 
			&& a->nodes[3] == 0x01 && a->nodes[4] == 0x80);
	assert(b->nodes[0] == 1 && b->nodes[1] == 0xC0 && b->nodes[2] == 0);
	BZreleaseMemory(a);
	BZreleaseMemory(b);
	a = _range(1, 16);
	b = _range(7, 9);

	BZreleaseMemory(a);
	BZreleaseMemory(b);
	BZreleaseMemory(c);
	//BZprint(a);
	//BZprint(b);

//Tests for removeTrailingZeroes

	//trees that need zeroes removed

	a = BZcreateFromStr("[0x00][0x45]");
	removeTrailingZeroes(a);
	assert(BzetEquals(a, "[0x00][0x45]"));
	BZreleaseMemory(a);

	a = BZcreateFromStr("[0x01][0x80][0x00]");
	removeTrailingZeroes(a);
	assert(BzetEquals(a, "[0x00][0xFF]"));
	BZreleaseMemory(a);

	a = BZcreateFromStr("[0x01][0x00][0x00]");
	removeTrailingZeroes(a);
	assert(BzetEquals(a, "[0x00][0x00]"));
	BZreleaseMemory(a);

	a = BZcreateFromStr("[0x01][0x00][0x00]");
	removeTrailingZeroes(a);
	assert(BzetEquals(a, "[0x00][0x00]"));
	BZreleaseMemory(a);

	a = BZcreateFromStr("[0x02][0x80][0x00]");
	removeTrailingZeroes(a);
	assert(BzetEquals(a, "[0x01][0xFF][0x00]"));
	BZreleaseMemory(a);

	a = BZcreateFromStr("[0x02][0x00][0x80][0xAA][0x44][0x45][0xDE]");
	removeTrailingZeroes(a);
	assert(BzetEquals(a, "[0x01][0xAA][0x44][0x45][0xDE]"));
	BZreleaseMemory(a);

	a = BZcreateFromStr("[0x02][0x00][0x80][0x00][0x80][0x45]");
	removeTrailingZeroes(a);
	assert(BzetEquals(a, "[0x00][0x45]"));
	BZreleaseMemory(a);

	a = BZcreateFromStr("[0x02][0x00][0x00]");
	removeTrailingZeroes(a);
	assert(BzetEquals(a, "[0x00][0x00]"));
	BZreleaseMemory(a);

	//trees that don't need zeroes removed.  They should be unchanged.

	a = BZcreateFromStr("[0x00][0xEF]");
	removeTrailingZeroes(a);
	assert(BzetEquals(a, "[0x00][0xEF]"));
	BZreleaseMemory(a);

	a = BZcreateFromStr("[0x01][0xC0]");
	removeTrailingZeroes(a);
	assert(BzetEquals(a, "[0x01][0xC0]"));
	BZreleaseMemory(a);

	a = BZcreateFromStr("[0x02][0x05][0x40][0x00][0x01][0xFE]");
	removeTrailingZeroes(a);
	assert(BzetEquals(a, "[0x02][0x05][0x40][0x00][0x01][0xFE]"));
	BZreleaseMemory(a);

//reverseBits

	assert(reverseBits(0xA4) == 0x25);
	assert(reverseBits(0) == 0);
	assert(reverseBits(0xFF) == 0xFF);
	assert(reverseBits(0xAA) == 0x55);
}

//-----------------------------------------------------------------

void binopHarn() {
	
//Test for trees of level 0.  In this case we are only operating on data.
	Bzet* bz1 = BZcreate(0);
	Bzet* bz2 = BZcreate(7);
	Bzet* result = NULL;
	result = binop(DOR, bz1, bz2);
	assert(result && BZgetNumLevels(result) == 0 && result->nodes[1] == 0x81 && result->numNodes == 2);
	BZreleaseMemory(bz1); BZreleaseMemory(bz2); BZreleaseMemory(result);

	bz1 = BZcreate(2);
	bz2 = BZcreate(3);
	result = binop(DXOR,bz1,bz2);
	assert(result && BZgetNumLevels(result) == 0 && result->nodes[1] == 0x30 && result->numNodes == 2);
	BZreleaseMemory(bz1); BZreleaseMemory(bz2); BZreleaseMemory(result);

	//IMPORTANT: the following test cases were written before binop
	//had the capability to remove trailing zeros from the result of
	//binary operations.  Once this capability is implemented, define REMOVE_ZEROS

//data-data comparison
	bz1 = BZallocate();
	bz1->nodes = (BYTE*)malloc(3*sizeof(BYTE));
	bz1->numNodes = 3;
	bz1->nodes[0] = 1; bz1->nodes[1] = 0xC0; bz1->nodes[2] = 0x00; 
	bz2 = BZallocate();
	bz2->nodes = (BYTE*)malloc(3*sizeof(BYTE));
	bz2->numNodes = 3;
	bz2->nodes[0] = 1; bz2->nodes[1] = 0x40; bz2->nodes[2] = 0x00;
	//BZprint(bz1);
	//BZprint(bz2);
	result = binop(DAND, bz1, bz2);
	assert(result->numNodes == 3 && result->nodes[0] == 1 && result->nodes[1] == 0x40 && result->nodes[2] == 0x00);
	BZreleaseMemory(result);

	result = binop(DXOR, bz1, bz2);
#ifndef REMOVE_ZEROS
	assert(result->numNodes == 3 && result->nodes[0] == 1 && result->nodes[1] == 0x80 && result->nodes[2] == 0x00);
#else
	assert(result->numNodes == 2 && result->nodes[0] == 0 && result->nodes[1] == 0xFF);
#endif
	BZreleaseMemory(result);

	bz2->nodes[1] = 0x3F;
	result = binop(DOR, bz1, bz2);
	assert(result->numNodes == 3 && result->nodes[0] == 1 && result->nodes[1] == 0xFF && result->nodes[2] == 0x00);
	BZreleaseMemory(result);

	result = binop(DNOTOR, bz1, bz2);
#ifndef REMOVE_ZEROS
	assert(result->numNodes == 3 && result->nodes[0] == 1 && result->nodes[1] == 0x00 && result->nodes[2] == 0x00);
#else
	assert(result->numNodes == 2 && result->nodes[0] == 0 && result->nodes[1] == 0x00);
#endif

	BZreleaseMemory(bz1);
	BZreleaseMemory(bz2);
	BZreleaseMemory(result);

//Recursion, but only with data-data comparisons
	bz1 = BZcreateFromStr("[0x01][0xC0][0x08][0xB0]");
	bz2 = BZcreateFromStr("[0x01][0x40][0x08][0xD8]");
	result = binop(DAND, bz1, bz2);
	assert(BzetEquals(result, "[0x01][0x40][0x08][0x90]"));
	BZreleaseMemory(bz1); 
	BZreleaseMemory(bz2);
	BZreleaseMemory(result);

	bz1 = BZcreateFromStr("[0x01][0xC0][0x08][0xF0]");
	bz2 = BZcreateFromStr("[0x01][0x40][0x08][0x0F]");
	result = binop(DOR, bz1, bz2);
	//BZprint(result);
	assert(BzetEquals(result, "[0x01][0xC8][0x00]"));
	BZreleaseMemory(bz1); 
	BZreleaseMemory(bz2);
	BZreleaseMemory(result);

//test negation
	negateHarn();

//The following tests are designed specifically to exercise do_tree_op

	//test do_tree_op by itself

	do_tree_op_Harness();

	//test do_tree_op in the context of binop

	bz1 = BZcreateFromStr("[0x01][0xC0][0x08][0x45]");
	bz2 = BZcreateFromStr("[0x01][0x48][0x00]");
	result = binop(DAND, bz1, bz2);
	//BZprint(result);
	assert(BzetEquals(result, "[0x01][0x40][0x08][0x45]"));
	BZreleaseMemory(result);

	result = binop(DOR, bz1, bz2);
	assert(BzetEquals(result, "[0x01][0xC8][0x00]"));
	BZreleaseMemory(result); BZreleaseMemory(bz1); BZreleaseMemory(bz2);

	bz1 = BZcreateFromStr("[0x03][0x00][0x80][0x2C][0x50][0x00][0x40][0xCD][0xF1][0x00]");
	bz2 = BZcreateFromStr("[0x03][0x00][0x80][0x19][0xA0][0xBC][0x00][0x00][0x08][0xAB]");
	result = binop(DAND, bz1, bz2);
	assert(BzetEquals(result, "[0x02][0x08][0x30][0x00][0x08][0xAB][0xF1][0x00]"));
	BZreleaseMemory(result);

	BZreleaseMemory(bz1);
	BZreleaseMemory(bz2);

	bz1 = BZcreateFromStr("[0x00][0xA2]");
	bz2 = BZcreateFromStr("[0x01][0x00][0x40][0x80]");
	result = binop(DOR, bz1, bz2);
	assert(BzetEquals(result, "[0x01][0x00][0xC0][0xA2][0x80]"));
	BZreleaseMemory(result); BZreleaseMemory(bz1); BZreleaseMemory(bz2);

//Tests for all binary operations
	
	bz1 = BZcreateFromStr("[0x01][0x90][0x24][0x20][0xCC]");
	bz2 = BZcreateFromStr("[0x01][0x48][0x30][0xC8][0xAA]");

	//AND
	result = binop(DAND, bz1, bz2);
	assert(BzetEquals(result, "[0x01][0x00][0x10][0xAA]"));
	BZreleaseMemory(result);

	//OR
	result = binop(DOR, bz1, bz2);
	assert(BzetEquals(result, "[0x01][0xD8][0x24][0xE8][0xCC]"));
	BZreleaseMemory(result);

	//XOR
	result = binop(DXOR, bz1, bz2);
	assert(BzetEquals(result, "[0x01][0xC8][0x34][0xE8][0x55][0xCC]"));
	BZreleaseMemory(result);

	//NOR
	result = binop(DNOTOR, bz1, bz2);
	assert(BzetEquals(result, "[0x01][0x03][0x24][0x17][0x33]"));
	BZreleaseMemory(result);

	//NAND
	result = binop(DNOTAND, bz1, bz2);
	assert(BzetEquals(result, "[0x01][0xEF][0x10][0x55]"));
	BZreleaseMemory(result);

	//NXOR
	result = binop(DNOTXOR, bz1, bz2);
	assert(BzetEquals(result, "[0x01][0x03][0x34][0x17][0xAA][0x33]"));
	BZreleaseMemory(result);

	//FALSE
	result = binop(ZERO, bz1, bz2);
	assert(BzetEquals(result, "[0x00][0x00]"));
	BZreleaseMemory(result);

	//TRUE
	result = binop(ONE, bz1, bz2);
	assert(BzetEquals(result, "[0x01][0xFF][0x00]"));
	BZreleaseMemory(result);
	
	BZreleaseMemory(bz1);
	BZreleaseMemory(bz2);

	bz1 = BZcreateFromStr("[0x00][0xFF]");
	bz2 = BZcreateFromStr("[0x01][0x00][0x40][0x80]");
	result = binop(DOR, bz1, bz2);
	assert(BzetEquals(result, "[0x01][0x80][0x40][0x80]"));

	BZreleaseMemory(result);
	result = invert(bz1);
	assert(BzetEquals(result, "[0x00][0x00]"));
	BZreleaseMemory(result);

	result = invert(bz2);
	assert(BzetEquals(result, "[0x01][0xBF][0x40][0x7F]"));

	BZreleaseMemory(bz1);
	BZreleaseMemory(bz2);
	BZreleaseMemory(result);

	bz1 = BZcreateFromStr("[0x01][0x00][0xd8][0x64][0x7f][0xdc][0x1c]");
	bz2 = BZcreateFromStr("[0x01][0x00][0xf8][0xb3][0xb3][0xb3][0xb3][0xb3]");
	result = binop(DNOTAND, bz1, bz2);

	if (result) {
		BYTE* b;
		int len;
		BYTE check[8] = {reverseBits(0xdf), reverseBits(0xcc), reverseBits(0xff), reverseBits(0x6f), reverseBits(0xef), reverseBits(0xff), reverseBits(0xff), reverseBits(0xff)};
		BZconvertToByteArray(result, &b, &len);
		assert(memcmp(check, b, len*sizeof(BYTE)) == 0);
	}

	BZreleaseMemory(bz1);
	BZreleaseMemory(bz2);
	BZreleaseMemory(result);
}

//-----------------------------------------------------------------------

void conversionHarn() {

	//BYTE bytes[2] = {0x45, 0x45}; //0xA2 when reversed
	//Bzet* bz1 = BZcreateFromByteArray(bytes, 2);
	//BZprint(bz1);
}

void bitMutatorHarn() {

	//tests for flip, set, unset, test
	Bzet* bz = BZcreateEmpty();
	BZset(bz, 1);
	BZset(bz, 3);
	assert(BZtest(bz,1) && BZtest(bz,3) && !BZtest(bz,0));

	BZunset(bz, 3);
	BZunset(bz, 0);
	assert(BZtest(bz,1) && !BZtest(bz,3) && !BZtest(bz,0));

	BZflip(bz, 0);
	BZflip(bz, 1);
	assert(!BZtest(bz,1) && !BZtest(bz,3) && BZtest(bz,0));

	BZreleaseMemory(bz);
}

void rangeHarn() {

	Bzet* bz = _range(0,1);

	_testRange(bz, 0, 1);
	BZreleaseMemory(bz);

	bz = _range(2,32);
	_testRange(bz, 2, 32);
	BZreleaseMemory(bz);

	bz = _range(50,952);
	_testRange(bz, 50, 952);
	BZreleaseMemory(bz);

	bz = _range(2, 99996);
	_testRange(bz, 2, 99996);
	BZreleaseMemory(bz);
}

void _testRange(Bzet* bz, int start, int len) {

	int i;
	int endRange = start + len;
	int nBits = 8 * pow8(bz->nodes[0]);

	for (i = 0; i < nBits; i++) {

		//Test that all bits before the range are 0
		if (i < start || endRange <= i) {
			assert(!BZtest(bz, i));
		}
		else {
			//i is in the range.  All bits should be 1
			assert(BZtest(bz, i));
		}
	}
}

void strideHarn() {

	Bzet* bz;
	BIGNUM start;
	BIGNUM strd;
	BIGNUM len;
	
	start = 0;
	strd = 2;
	len = 8;
	bz = stride(start,strd,len);
	_testStride(bz,start,strd,len);
	BZreleaseMemory(bz);

	start = 2;
	strd = 3;
	len = 8;
	bz = stride(start,strd,len);
	_testStride(bz,start,strd,len);
	BZreleaseMemory(bz);

	start = 63;
	strd = 2;
	len = 3;
	bz = stride(start,strd,len);
	_testStride(bz,start,strd,len);
	BZreleaseMemory(bz);

	start = 34;
	strd = 11; //what happens when stride is 1 here?
	len = 3456;
	bz = stride(start,strd,len);
	_testStride(bz,start,strd,len);
	BZreleaseMemory(bz);

	start = 34;
	strd = 1; //what happens when stride is 1 here?
	len = 15;
	bz = stride(start,strd,len);
	_testStride(bz,start,strd,len);
	BZreleaseMemory(bz);
}

void _testStride(Bzet* bz, BIGNUM start, BIGNUM stride, BIGNUM len) {

	BIGNUM i;
	BIGNUM endRange = start + len;
	BIGNUM nBits = 8 * pow8(bz->nodes[0]);

	//All bits before start must be 0
	for (i = 0; i < start; i++) {
		assert(!BZtest(bz, i));
	}

	//test stride bits
	for (i = start; i < endRange; i ++) {
		if (((i - start) % stride) == 0) {
			assert(BZtest(bz, i));
		}
		else {
			assert(!BZtest(bz, i));
		}
	}

	for (i = endRange; i < nBits; i++) {
		assert(!BZtest(bz, i));
	}
}