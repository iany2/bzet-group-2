from bitsv09 import *

ALL_ZERO = 0
ALL_ONE = 1
MIXED = 2

#  Returns the bzet representation of the parameter 'bitstring'
#  bitstring is a python bytes object representing a sequence of bits
def createFromByteArray(bitstring):
	
	level = 0
	size = len(bitstring)
	k = size
	padStart = size
	nodes = []
	
	while (k > 1):
		k /= 8
		level = level + 1
		
	powL = pow8(level)
	if (powL == size):
		#size is a power of 8
		nodes.append(level)
	else:
		#size is not a power of 8.  Extend size to be a power
		#of 8, and set padStart to the old value so we know to
		#treat everything from nodes[padStart] to nodes[size-1]
		#as if it were 0
		if powL < size:
			level+1
			size = 8*powL
		else:
			size = powL	
		nodes.append(level)
	
	_createFromByteArray(bitstring, 0, size, padStart, nodes, 1)
	
	return Bits(bytes(nodes))
	
def _createFromByteArray(bitstring, pos, size, padStart, nodes, numNodes):
	numNodes = len(nodes)
	isTreeRoot = (numNodes == 1)
	rootPos = numNodes
	padZero = (pos >= padStart)
	
	if (size < 8):
		if (size != 1):
			print("Error: expected size to be 1")
			exit()
		rootType = ALL_ZERO
		if (not padZero):
			rootType = _getTreeType(bitstring[pos], 0x00)
		if (rootType == MIXED or isTreeRoot):
			if (padZero):
				nodes.append(0x00)
			else:
				nodes.append(bitstring[pos])
		return rootType
		
	nodes.append(0x00)
	numNodes = numNodes + 1
	rootDataBytePos = numNodes - 1
	
	nodes.append(0x00)
	numNodes = numNodes + 1
	rootTreeBytePos = numNodes - 1
	
	partitionSize = size / 8
	
	for i in range(0, 8):
		
		ithBit = 0x80 >> i
		partitionStart =  int(i*partitionSize + pos)
	
		branchType = _createFromByteArray(bitstring, partitionStart, partitionSize, padStart, nodes, numNodes)

		if (branchType == ALL_ONE):
			#set data bit
			nodes[rootDataBytePos] |= ithBit
		elif (branchType == MIXED):
			#set tree bit
			nodes[rootTreeBytePos] |= ithBit
			
	rootType = _getTreeType(nodes[rootDataBytePos], nodes[rootTreeBytePos])
	if (rootType != MIXED and (not isTreeRoot)):
		nodes.pop()
		nodes.pop()
		numNodes = numNodes - 2
	
	return rootType

def _getTreeType(data, tree):
	if ((data == 0x00) and (tree == 0x00)):
		return ALL_ZERO
	elif (data == 0xFF):
		return ALL_ONE
	else:
		return MIXED
	
def checkBzetAgainstByteArray(bitstring, bzet):
	
	n = 0
	for byte in bitstring:
		for i in range(0, 8):
			bit = (0x80 >> i) & byte
			if (bzet.TEST(n + i) != bool(bit)):
				return False
		n = n + 8
		
	return True
	
#-------------------------------------------------------------------------------

"""
import random

NUMBYTES = 100
blist1 = []
blist2 = []
for i in range(0, NUMBYTES):
		blist1.append(random.randint(0x00, 0xFF))
		blist2.append(random.randint(0x00, 0xFF))
	
x1 = bytes(blist1)
x2 = bytes(blist2)

#for b in x:
#	print('{0:02x}' . format(b&0xff))

bzet1 = createFromByteArray(x1)
bzet2 = createFromByteArray(x2)

if (checkBzetAgainstByteArray(x1, bzet1) and checkBzetAgainstByteArray(x2, bzet2)):
	print("Success")
else:
	print("Failed")

result = bzet1.AND(bzet2)
result.INVERT()
"""