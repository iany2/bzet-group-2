#!/usr/bin/python3
# vi: ts=4 sw=4 et ai sm
# (c) Copyright 2011 Robert Uzgalis

# Version of 2011-11-30

#             BZET Primes Example
# Primes are difficult for compression because they stay
# relatively dense.
# This is an improvement over the prior version by setting
# bits after the current prime squared.  Speeding this up
# by a factor of 2 and lowered the checking to sqrt maxsize.

# maxsize = 10000
# report = 100
maxsize = 1000000
report = 1000

from math import sqrt
import sys
print( "Path includes these:" )
for d in sys.path:
    print(d)
print()

#from bitsv09 import Bits
# from BBzetPy3 import BZET    as Bits
# from Bzet4    import PyBzet4 as Bits
from BZet    import BZet    as Bits


# print( "This test used: from bitsv09 import Bits" )
# print( "This test used: from BBzetPy3 import BZET as Bits" )
# print( "This test used: from Bzet4 import PyBzet4 as Bits" )
print( "This test used: from Bzet8 import BZet as Bits" )


from time import *

def cpssn( bs ):
    return (bs/bmsize)*100.0


notprime = Bits()                   # Zero is not a prime
notprime.CreateBzet(0)
# Pmask    = Bits( [(3,maxsize-1),] )   # Bit mask for prime set
Pmask = Bits()
Pmask.RANGE( 2, maxsize-4 )   # Bit mask for prime set
print( "Starting at", ctime(time()))
print( "setting max size to", maxsize )
print( "setting report freq", report )
bmsize = (maxsize/8.0)+8 # 8 bits/byte + double-word length field
bsize  = notprime.Size() # tree-size   + 1 byte level field
print( "Starting bitset size uncompressed", bmsize, "bytes; compressed", \
       bsize, "%4.2f"%cpssn(bsize), "%\n" ) 


# Do Sieve of Eratosthenes to find Primes.

nextp = 1
count = 0
clock()
limit = int( sqrt(maxsize) ) + 1
while nextp < limit:    
    
    if count%report == 1 or count < 25:
        bsize = notprime.Size()
        tm = clock()
        print( "%6d"%count, "%7.1f"%tm, "sec; prime:", \
               "%6d;"%nextp, "size", "%6d"%bsize, "%6.2f"%cpssn(bsize) + \
               "%; primes/sec", "%5.2f"%( float(count)/tm) )
   
    count += 1
    for np in range(nextp+1,maxsize):
        if not notprime[np]: # Find the next prime
            nextp = np
            break
    # print( "Doing", "%06d"%count, "%06d"%nextp )
    startStride = nextp*nextp
    bitsToEnd = maxsize - startStride
    if (bitsToEnd > 0):
        temp = Bits()
        temp.stride(startStride, nextp, bitsToEnd)
        notprime = temp.OR(notprime)

print( "\nAt prime", count, nextp, "there are no more primes less than", \
       maxsize, "\n" )
primes = notprime.Invert()  # Get the primes
primes = primes.AND(Pmask)          # Dont let 1 or maxsize be prime
                         # This also cuts out any surious
                         # bits from the NOT
count = primes.CountSetBits()   # Count the one bits
bsize = notprime.Size()  # Final size
tm = clock()             # Compute time. End the timing.
print( "Ending at", ctime(time()), "%7.1f"%tm, "cpu seconds" )
h = tm // 3600           # hours of computing
xtm = tm - (h * 3600 )
m = xtm // 60            # minutes of computing
s = xtm % 60             # seconds of computing
print( "that is: ", "%2d"%h, "h", "%2d"%m, "m", "%2d"%(int(s+0.5)), "s" )
count = primes.CountSetBits()   # count the primes

# find the last prime
lastp = 0
for i in primes.List_t():
    lastp = i

print( "%5d"%count, "%7.1f"%tm, "sec; last prime:", \
               "%6d"%lastp, "; final size:", bsize, ("%6.2f"%cpssn(bsize)) + \
               "%; primes/sec", "%5.2f"%(float(count)/tm) )

# Print Table of Primes
"""
ix = 0
for p in primes.List_t():
    if p > maxsize: break
    if ix%10 == 0: print( "" )
    ix += 1
    print( "%6d"%p, "", end='' )
"""
    
