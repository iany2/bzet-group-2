#ifndef BINOP_FUNC_H
#define BINOP_FUNC_H

#include "bzet.h"

// cdr and ddr are the return values
void set_node(BYTE* cdr, BYTE* ddr, BYTE sbit, BYTE cd, BYTE dd, int cf, int df)
{
	BYTE nbit = ~sbit & 0xff;
	BYTE cdr, ddr;

	if (cf)		
		*cdr = (cd | sbit);
	else
		*cdr = (cd & nbit);

	if (df)
		*ddr = (dd | sbit);
	else
		*ddr = (dd & nbit);

}

// Implements actoins CA and CB
// est and bsret are the return values
void bscopy(int* est, Bzet** bsret, const Bzet* bs, int p, int lev)
{
	if (p >= bs->numNodes)
		*est = bs->numNodes;
	else
	{
		//bsret = Bits.walk(bs, p, lev, false);
		// FIXME:  *est = list(gest)[0]
	}
}

// Implements actions NA and NB
// est and bsret are the return values
void bsneg(int* est, Bzet** bsret, const Bzet* bs, int p, int lev)
{
	if (p >= bs->numNodes)
	{	
		*est = bs->numNodes;
		//FIXME: bytes([])	
	}

	//bsret = Bits.walk(bs, p, lev, false);
	//FIXME: *est = list(gest)[0];
}

// implements action DA and DB
int bsdrop(const Bzet* bs, int p, int lev)
{

}

// Helper function to do_tree_op
bool mkbool(char x)
{
	if ( x == '1')
		return true;
	else
		return false;
}

// Updates positions and returns a bool or a byte
// Bool if result a compressed tree
// Bytes value if it is a mixed tree
void do_tree_op(int* p1ret, int* p2ret, Bzet** dr, int op, int lev, const Bzet* bs1, int p1, const Bzet* bs2, int p2)
{
	int opx;

	if (opx == DA0 || opx == DA1) 
	{ 
		*p1ret = bsdrop(bs1, p1, lev);	
		/*dr = mkbool(op[2]);*/
	}
	else if (opx == DB0 || opx == DB1) 
	{ 
		*p2ret = bsdrop(bs2, p2, lev); 
		/*dr = mkbool(op[2]);*/
	}
	else if (opx == CA) 
		bscopy(p1ret, dr, bs1, p1, lev);
	else if (opx == CB) 
		bscopy(p2ret, dr, bs2, p2, lev);
	else if (opx == NA) 
		bsneg(p1ret, dr, bs1, p1, lev);
	else if (opx == NB) 
		bsneg(p1ret, dr, bs2, p2, lev);
	else
		printf("Error: Tree Op action: ", op, lev, p1, p2);
}

// data action
BYTE do_data_op(int op, BYTE d1, BYTE d2)
{
	BYTE dr;

	if (op == DAND)			dr = d1 & d2;
	else if (op == DOR)		dr = d1 | d2;
	else if (op == DXOR)	dr = d1 ^ d2;
	else if (op == DNOTAND)	dr = ~(d1 & d2);
	else if (op == DNOTOR)	dr = ~(d1 | d2);
	else if (op == DNOTXOR)	dr = ~(d1 ^ d2);
	else if (op == ZERO)	dr = false;	
	else if (op == ONE)		dr = true;	
	else 					printf("Error: Op is not a valid operation");

	return dr;

}

#endif