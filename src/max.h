#include "bzet.h"
#include <math.h>

int bc4[16] = {
	0, 1, 1, 2, 1, 2, 2, 3,
	1, 2, 2, 3, 2, 3, 3, 4 
};

int bit_count( int n)
{
	// print( "bit count of ", n, "is", bc4[n & 0xf] + bc4[ (n>>4)&0xf])
	return bc4[ n & 0xf] + bc4[ (n >> 4) & 0xf];
}

int first_bit (BYTE bitx)
{
	bitx &= 0xff;

	if (bitx == 0) 
		return NULL;	// FIXME: can't return NULL, cuz it's 0

	for (int i = 0; i < 8; i ++)
	{
		if (bitx & 0x80)
			return i;
		bitx <<= 1;
	}

	return -1;
}

int last_bit(BYTE bitx)
{
	bitx &= 0xff;

	if (bitx == 0 )	
		return NULL;	// FIXME: can't return NULL, cuz it's 0

	for (int i = 0; i < 8; i ++)
	{
		if (bitx & 0x01)
			return (7-i);
		bitx >>= 1;
	}
	return -1;
}

BYTE mkbits(int ibit, int ebit)
{
	BYTE abyte = 0;

	for (int ix = ibit; ix < ebit+1; ix ++)
		abyte |= 0x80 >> ix;

	return abyte;
}

// returns the integer x as a single bytes value
BYTE byte(int x)
{}


int powersof8[10] = {
	1, 8, 64, 512, 4096, 32768, 
	262144, 2097152, 16777216, 134217728
};

int pow8(int n )
{
	if (n < 0)
		return NULL;
	if (n < 10)
		return powersof8[n];

	return pow(8.0, n);
}

