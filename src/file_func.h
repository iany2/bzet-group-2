#ifndef FILE_FUNC_H
#define FILE_FUNC_H

// Include necessary libraries
#include "bzet.h"

#define FSIZEBUFF 8

typedef struct {

	Bzet* bz;					// bzet pointer data stored if necessary
	// FIXME shouldn't be int
	int filesize;				// file size of the orignal file
	char ofname[FILENAME_MAX];	// original file name
	
} FileData;

// define return results
enum 
{
	FILE_IO_ERROR, 
	BZ_COMPRESSION_FAILED, 
	BZ_EXTRACTION_FAILED, 
	BZ_CORRUPT_DATA,  
	BZ_SUCCESS
};

// function declarations for file i/o
extern __declspec(dllexport) int BZcompressFile(const char fname[], FileData* result);
extern __declspec(dllexport) int BZextractToFile(const char fname[], const FileData* bz);
extern __declspec(dllexport) int BZwriteCompressedContentToFile(const char fname[], const FileData* bz);
extern __declspec(dllexport) int BZreadCompressedFile(const char  fname[], FileData* result);


#endif
