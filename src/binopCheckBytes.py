from BZet import *
from random import randint
from sys import maxsize

do_data_op=lib.do_data_op
do_data_op.restype=c_char

def Test():
    bz1=BZet()
    bz2=BZet()
    print("Testing BZet Copy")
    bz1.CreateBzet(64)
    print("Created a bzet with 65th bit set")
    bz2.Copy(bz1)
    if(bz1.pynode.contents.raw==bz2.pynode.contents.raw):
        print("Copy Successful !!!")
        
    else:
        print("Copy Unsuccessful")

    print("Testing Invert")

    
    bz_inv_res=bz1.Invert()
    bz_inv_inv=bz_inv_res.Invert()
    bz_net=bz_inv_inv.pynode.contents.raw
    if(bz_net==bz1.pynode.contents.raw):
        
        print("Invert Successful")
        del bz_inv_res
        del bz_inv_inv

    else:

        print("Invert Unsuccessful")

    print("Testing CountSetBits")

    num=bz1.CountSetBits()
    
    if(num==1):
        print("CountSetBits Test Successful")
    else:
        print("CountSetBits Test Unsuccessful")

    print("Testing ReturnLevel")
    
    level=bz1.ReturnLevel()
    
    if(level==2):
        print("ReturnLevel Test Successful")
    else:
        print("ReturnLevel Test Unsuccessful")

    print("Testing Clean")
    bz3=BZet()
    bz3.CreateEmptyBZet()
    print("Created Empty BZet")

    print("Testing First and Last")

    first=bz1.First()

    if(first==64):
        print("First Successful")
    else:
        print("First Unsuccessful")

    last=bz1.Last()

    if(last==64):
        print("Last Successful")
    else:
        print("Last Unsuccessful")
        
    print("Testing Set and Unset")

    pynode_old=bz1.pynode.contents.raw
    print(pynode_old)
    bz1.Unset(64)
    bz1.Set(64)
    pynode_new=bz1.pynode.contents.raw
    
    if(str(pynode_old)==str(pynode_new)):
        print("Set and Unset successful")
    else:
        print("Set and Unset unsuccessful")

    print("Testing Flip")
    pynode_old=bz1.pynode
    bz1.Flip(64)
    bz1.Flip(64)
    pynode_new=bz1.pynode

    if(pynode_old.contents.raw==pynode_new.contents.raw):
        print("Flip Successful")
    else:
        print("Flip Unsuccessful")
            
    print("Testing Test")

    res=bz1.Test(64)

    if(res==1):
        print("Test Successful")
    else:
        print("Test Unuccessful")

    
    SIZE=1024*1024
    byte=c_char*SIZE
    b1=byte()
    b2=byte()
    for i in range(SIZE):
        b1[i]=c_char(0x00+randint(0,maxsize) % (0xFF+1))
        b2[i]=c_char(0x00+randint(0,maxsize) % (0xFF+1))
 #   return [b1,b2]
    
    if(not(binopCheckBytes('or',b1,b2,SIZE))):
        print("Not Same Check Failed")
        return 0
    else:
        print("binopcheckbytes successful")

    print("OR: binopCheckBytes succeeded\n")

    if(not(binopCheckBytes('and',b1,b2,SIZE))):
        print("Not Same Check Failed")
        return 0
    else:
        print("binopcheckbytes successful")

    print("AND: binopCheckBytes succeeded\n")

    if(not(binopCheckBytes('xor',b1,b2,SIZE))):
        print("Not Same Check Failed")
        return 0
    else:
        print("binopcheckbytes successful")

    print("XOR: binopCheckBytes succeeded\n")
    
    if(not(binopCheckBytes('nand',b1,b2,SIZE))):
        print("Not Same Check Failed")
        return 0
    else:
        print("binopcheckbytes successful")

    print("NAND: binopCheckBytes succeeded\n")

    if(not(binopCheckBytes('nor',b1,b2,SIZE))):
        print("Not Same Check Failed")
        return 0
    else:
        print("binopcheckbytes successful")

    print("NOR: binopCheckBytes succeeded\n")

    if(not(binopCheckBytes('notxor',b1,b2,SIZE))):
        print("Not Same Check Failed")
        return 0
    else:
        print("binopcheckbytes successful")

    print("NOTXOR: binopCheckBytes succeeded\n")


def binopCheckBytes(opname, data1,data2,length_bytearray):
    
    file=open('mylog.txt','a')
    checkResult=pointer((c_char*length_bytearray)())
    
    lookup=dict([['AND',2],['XOR',3],['OR',4],['NOR',5],['NOTXOR',6],['NAND',7]])

    try:
        opno=lookup[opname.upper()]
    except KeyError:
        print("Key Not Found !! Check Argument 2 !!")
        return

    for i in range(length_bytearray):
        checkResult.contents[i]=do_data_op(opno,c_char(data1[i]),c_char(data2[i]))
        #checkResult.contents[i]=c_char(checkResult.contents[i])

    bz1=BZet()
    bz1.CreateBZetFromArray(data1,length_bytearray)
   # Transform results into the actual byte array
    b_1=bz1.ConvertBzToByteArray()
    extracted_bytes=b_1[0:length_bytearray]
 
    areSame=(extracted_bytes == data1.raw)

    if(areSame==False):

        file.write("binopCheckBytes failed conversion check 1\n\nexpected data1:\n\n")
        list_1=[data1.raw,length_bytearray,len(data1.raw)]

        for item in list_1:
          file.write("%s\n" % item)
        
        file.write("\ndata1 after decompression\n\n")
        list_2=[extracted_bytes,len(extracted_bytes)]

        for item in list_2:
          file.write("%s\n" % item)

        file.write("\nbz1->nodes:\n\n");

        del bz1
        file.close()
        return 0
    else:
        print("Extraction Successful !!!")

    bz2=BZet()
    bz2.CreateBZetFromArray(data2,length_bytearray)
    b_2=bz2.ConvertBzToByteArray()
    extracted_bytes=b_2[0:length_bytearray]
    # Transform results into the actual byte array
    
    areSame=( extracted_bytes == data2.raw)

    if(areSame==False):

        file.write("binopCheckBytes failed conversion check 1\n\nexpected data1:\n\n")
        list_1=[data2.raw,length_bytearray]

        for item in list_1:
          file.write("%s\n" % item)

        file.write("\ndata1 after decompression\n\n")
        list_2=[extracted_bytes,length_bytearray]

        for item in list_2:
          file.write("%s\n" % item)

        file.write("\nbz1->nodes:\n\n");

        del bz2
        file.close()
        return 0
    else:
        print("Extraction Successful !!!")
        
    areSame=True
    bz3=bz1.BinaryOp(bz2,opname)
    b_3=bz3.ConvertBzToByteArray()
    extracted_bytes=b_3[0:length_bytearray]
    areSame=(extracted_bytes == checkResult.contents.raw) # check 2nd value

    if(areSame==False):
        file.write("binopCheckBytes failed \n\nexpected result:\n\n")
        list_1=[checkResult.contents.raw,len(checkResult.contents.raw)]

        for item in list_1:
          file.write("%s\n" % item)

        file.write("\ndata1 after decompression\n\n")
        list_2=[extracted_bytes,len(b_3.contents.raw)]

        for item in list_2:
          file.write("%s\n" % item)

        file.write("\nbz1->nodes:\n\n");

        del bz3 

        file.close()
        return [data1,data2,checkResult,bz3]
    else:
        print("BinaryOp Successful !!!")
        
    del bz1
    del bz2
    del bz3
    file.close()
    return 1
 
    
