
def find_shortest_path_group(bzGraphGroup, start, end, path=[]):
        path = path + [start]
        if bzGraphGroup.list_graph()[start].Test(end):
            path.append(end)
            return path
        if start not in bzGraphGroup.list_graph():
            return None
        shortest = None

        for bit_index in bzGraphGroup.list_graph()[start].List_t():
            if bit_index not in path:
                newpath = find_shortest_path_group(bzGraphGroup, bit_index, end, path)
                if newpath:
                    if not shortest or len(newpath) < len(shortest):
                        shortest = newpath
        return shortest