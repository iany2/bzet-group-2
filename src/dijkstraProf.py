def find_shortest_path_prof(bzGraph, start, end, path=[]):
        path = path + [start]
        if bzGraph.list_graph()[start].TEST(end):
            path.append(end)
            return path
        if start not in bzGraph.list_graph():
            return None
        shortest = None

        for bit_index in bzGraph.list_graph()[start].LIST_T():
            if bit_index not in path:
                newpath = find_shortest_path_prof(bzGraph, bit_index, end, path)
                if newpath:
                    if not shortest or len(newpath) < len(shortest):
                        shortest = newpath
        return shortest
