#ifndef RANDOM_BYTES_H
#define RANDOM_BYTES_H

// Include necessary libraries
#include "bzet.h"

extern __declspec(dllexport) void randomBytes(BYTE bytes[], int length, int sparseness);

#endif
