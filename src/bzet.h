#ifndef BZET_H
#define BZET_H

#include <stdio.h>
#include <stdlib.h>
#include "linkedlist.h"

#define DEBUG

typedef struct {

	unsigned int start;
	unsigned int end;

} Range;

typedef enum { ALL_ZERO, ALL_ONE, MIXED } TreeType;

typedef int BIGNUM;

//actions which can be performs for binary operations
enum { DC, DB0, DA0, DB1, DA1, CA, CB, NOTA, NOTB};
#define NUM_BINOPS 8
#define NUM_CASETYPES 7

//each constant is also the row index of the binopActions table
enum {ZERO = 0, ONE, DAND, DXOR, DOR, DNOTOR, DNOTXOR, DNOTAND};

//Error codes

/********************************************************************
*							  Bzet
*********************************************************************/
typedef unsigned char BYTE;

typedef struct {

	int numNodes;
	int bufferSize;
	BYTE* nodes;

} Bzet;

TreeType _arrToBzet(const BYTE bytes[], int pos, const int size, const int padStart, Bzet* result);
int arrToBzet(const BYTE bytes[], int size, Bzet** bz);
TreeType getTreeType(BYTE data, BYTE tree);

/*********************** Accessors and Setters ***********************/

#define GET_NUM_NODES(pbz) (pbz)->numNodes
#define SET_NUM_NODES(pbz,n) (pbz)->numNodes = (n)
#define GET_TREE_SIZE_IN_BYTES(pbz) ((pbz)->numNodes * sizeof(BYTE))
#define GET_NODES(pbz) (pbz)->nodes
#define SET_NODES(pbz, pBytes) (pbz)->nodes = (pBytes)

/******************** Construction and Destruction ******************/

/*  Allocate an unitialized Bzet.  This is not the same as an "empty" Bzet
*	Maybe it really should be...
*/
Bzet* BZallocate();

/*  Allocate a new Bzet.  The bzet's 'nodes' member will be a reference
*	to the same memory pointed to by the parameter 'nodes', and numNodes
*	should be equal to the number of bytes starting at 'nodes' to be referenced
*	starting from 'nodes'.  Example usage:
*
*	BYTE* nodes = (BYTE*)malloc(numNodes*sizeof(BYTE));
*	//Initialize each element of nodes...
*	...
*	...
*	Bzet* newBzet = BZallocateInit(nodes, numNodes);
*	//DO NOT FREE NODES!!  It is referenced by newBzet!
*/
Bzet* BZallocateInit(BYTE* nodes, int numNodes);

//---------------------------------------------------------------------

/*  Create a Bzet with only the nth bit set.
*	@param n: the index of the bit that will be set.
*	@return: a pointer to a newly allocated Bzet.
*/
extern __declspec(dllexport) Bzet* BZcreate(int n);

//---------------------------------------------------------------------

/* Creates a Bzet with no bits set.
*  return: a pointer to the new Bzet
*/
extern __declspec(dllexport) Bzet* BZcreateEmpty();

//---------------------------------------------------------------------

/*  Create a new Bzet in which for every indices[i], the ith bit
*   is set, and for every range [j,k], bits j through k inclusive
*	are set.
*	@param indices: array containing indices of the bits to be set
*	@param nIndices: the number of elements in the array 'indices'
*	@param ranges: an array of struct Range.  Each struct contains
*		a 'start' and 'end' index, specifying the range of bits to be set.
*	@param nRanges: the number of elements in the array 'ranges'
*	@return: pointer to a newly allocated Bzet
*/
Bzet* BZcreateAdv(int indices[], int nIndices, Range ranges[], int nRanges);

//---------------------------------------------------------------------

/*  Creates a new Bzet which encodes an arbitrary bitstring.
*	@param bytes: the bitstring to be encoded, represented as an array of bytes.
*	@param nBytes: the number of elements in the array 'bytes'
*	@return: a pointer to a new Bzet
*/
extern __declspec(dllexport) Bzet* BZcreateFromByteArray(const BYTE bytes[], int nBytes);

//---------------------------------------------------------------------

/* Create a new Bzet that is a copy of another Bzet
*  @param other: a pointer to the Bzet to be copied
*  @return: pointer to the new Bzet
*/
extern __declspec(dllexport) Bzet* BZcopy(const Bzet *other);

//---------------------------------------------------------------------

/*	Returns the integer level of the bitset, -1 if b is invalid
*	@param b: a pointer to the bzet which level wants to be found
*/
extern __declspec(dllexport) int BZlev(Bzet *b);

/*	Returns the number of memory bytes used by the Bzet, -1 if b is invalid
*	@param b: a pointer to the bzet which size wants to be found
*/
extern __declspec(dllexport) size_t BZsize(Bzet *b);

/*	Sets all of the values to 0.
*	@param b: a pointer to the bzet which will be cleaned
*/
extern __declspec(dllexport) void BZclean(Bzet *b);

/*	Prints a Bzet, non-formatted
*	@param b: pointer to a Bzet wanting to be printed
*/
extern __declspec(dllexport) void str(Bzet *b);

/*  Free memory allocated by a Bzet.
*	@param pBzet: a pointer to the Bzet to free.
*/
extern __declspec(dllexport) void BZreleaseMemory(Bzet* pBzet);


//---------------------------------------------------------------------

//Swap two values of type Bzet*
void swappbz(Bzet** bz1, Bzet** bz2);

/*********************** Utility **************************************/

/*  Returns 1 if the bzet pointed to by bz is all zero.  Else returns 0.
*/
int BZisZero(const Bzet* bz);

//---------------------------------------------------------------------

/*  Writes val to bzet->nodes[i].  If i out of range of the current
*   buffer, the buffer is expanded.  If i is out of range of the currently
*	active part of the buffer (eg. >= numNodes) then bzet->numNodes will be
*   expanded to hold an element at index i.
*/
static int _BZwriteToIndexAndBuffer(Bzet* bzet, int i, BYTE val); 

//---------------------------------------------------------------------

/*  Expands the current buffer to hold some number of elements
*	significantly larger than index+1.  In other words, prepares
*   The buffer to be written to at the position 'index'.  If 'index'
*   is smaller than the current buffer, does nothing.
*
*	returns: 1 if the buffer was resized, 2 if index < bufferSize, 0 if 
*   not enough memory.
*/
static int _BZbufferAtIndex(Bzet* bz, int index);

//---------------------------------------------------------------------

/*  If the current buffer is at least as big as newBufferSize,
*	does nothing.  Else, resizes to buffer to hold newBufferSize
*	elements.
*
*	returns: 1 if the buffer was expanded; 2 if the buffer was not expanded
*		because it was already larger than newBufferSize; 0 if memory 
*		allocation failure.
*/
static int _BZexpandBuffer(Bzet* bz, int newBufferSize);

//---------------------------------------------------------------------

/*  Appends val to the end of the bzet byte array.
*	returns: 1 on success, 0 on failure.
*/
static int _BZpushBack(Bzet* bz, BYTE val);

//---------------------------------------------------------------------

/*  Reverses the bits in the BYTE x, such that the first bit 
*	becomes the last, and the second the second to last, etc.
*	returns: a BYTE with reversed bits
*/
BYTE reverseBits(BYTE x);

//---------------------------------------------------------------------

//find the index of the next subtree
//parameters are starting position of tree byte and the level which it is at
int findEndTree(const Bzet* bitset, int pos, int level);

//---------------------------------------------------------------------

static int count_bits(BYTE byte);

//---------------------------------------------------------------------

/*  Resizes the Bzet to hold numBytes bytes
*/
int BZrealloc(Bzet* bz, int numBytes);

//---------------------------------------------------------------------

//print the contents of b
extern __declspec(dllexport) void BZprint(Bzet* b);

//---------------------------------------------------------------------

void BZfprint(FILE* fp, Bzet* b);

//---------------------------------------------------------------------

/*  Conceptually, zero extends either bzet1 or bzet2 so that their  
*	levels are the same.
*	@param bzet1, bzet2: pointers to bzets to be aligned
*	@return: 1 on success, 0 otherwise.
*/
int _BZalignNormalized(Bzet* bzet1, Bzet* bzet2);

//This function is deprecated.  Use _BZalignNormalized instead
int _BZalign(Bzet* bzet1, Bzet* bzet2);

//---------------------------------------------------------------------

/*  Returns a Bzet with bits set starting at
*	start and extending for length bits.
*/
extern __declspec(dllexport) Bzet* _range(int start, int length);

//---------------------------------------------------------------------

/*  Helper function for _range to handle ranges that are
*   split across multiple bytes
*/
static Bzet* _rangeSplitAcrossBytes(int start, int length);

//---------------------------------------------------------------------

/*
*  Create a bzet with the following property:
*  For all bits i, the ith bit is set if and only if i == n*stride + start 
*  and i < start + length, where n is any integer >= 0.
*/
extern __declspec(dllexport) Bzet* stride(BIGNUM start, BIGNUM stride, BIGNUM length);

//-----------------------------------------------------------------

static TreeType _stride(BIGNUM start, BIGNUM stride, BIGNUM end, BIGNUM subtreeNumBits, Bzet* result, int* rpos, BIGNUM* bitpos, int level, BIGNUM bitstart);

//-----------------------------------------------------------------

static BYTE _mkStrideBits(BIGNUM* bitpos, BIGNUM stride, BIGNUM end);

//-----------------------------------------------------------------

static int _norm(BYTE* ba, int len);

//---------------------------------------------------------------------

/*  Returns a BYTE with bits set starting at
*	start, and extending for n bits.  length
*	must be <= 8.  If length is <= 0, returns 0x00.
*/
BYTE _mkbits(int start, int length);

//---------------------------------------------------------------------

#define NO_BIT -1
/*  Returns the index of the first bit set in 'byte', where
*	the least significant bit is considered to be the right-most bit.
*	If no bit is set, returns NO_BIT.
*	Example: first_bit(0x40) returns 1
*			 first_bit(0x01) returns 7
*/
int first_bit(BYTE byte);

//same functionality as first_bit except the opposite
//	Example: first_bit(0x0F) returns 7
//			 first_bit(0xC0) returns 1 
int last_bit(BYTE byte);
//---------------------------------------------------------------------

/*  Returns the height of the tree representation of bz
*/
int BZgetNumLevels(const Bzet* bz);

//---------------------------------------------------------------------

/*  Computes 8^n
*/
int pow8(int n);

//---------------------------------------------------------------------

/*  Compresses a bzet to the minimum size that is needed
*   to specify it's contents.  This only works on Bzets which
*	are in all other ways valid, except that they have more trailing
*	zeros than are necessary to make the bzet a power of 8
*
*   @param bz: pointer to the bzet to compress
*/
void removeTrailingZeroes(Bzet* bz);

/*  Frees an array of BYTE's pointed to by the parameter 'bytes'.
*/
extern __declspec(dllexport) void freeByteArray(BYTE* bytes);


/*********************** Binary Operations ****************************/

/*  Public interface for _Binop
*   @param: bz1, bz2: pointers to 2 bzets to perform a binary operation on.
*	@param: opno: the code for which binary operation to perform.  Valid codes are
*		ZERO, DAND, DXOR, DOR, DNOTOR, DNOTXOR, DNOT, DNOTAND, ONE
*	@return: a new bzet which encodes the resulting bitstring of the binary operation.
*/
extern __declspec(dllexport) Bzet* binop(int opno, const Bzet* bz1, const Bzet* bz2);

//---------------------------------------------------------------------

/*  Returns the complement of the bzet pointed to by bz.
*	The result will not be normalized (the level will stay
*	the same as the uncomplemented bzet, even if it could be
*	reduced).
*/
extern __declspec(dllexport) Bzet* invert(const Bzet* bz);

//---------------------------------------------------------------------

/*
*
*	opno: the code for what binary operation to perform
*	bz1: pointer to bzet in the left operand
*	bz2: pointer to bzet in the right operand
*	p1: current position in bz1.  It is passed by pointer so it can be changed by recursive calls
*	p2: current position in bz2.  It is passed by pointer so it can be changed by recursive calls
*	result: pointer to result of teh operation.  It will be accumulated by recursive calls
*	rpos: current position in the result.  It is passed by pointer so it can be changed by recursive calls
*
*	returns: a code indicating what type of tree resulted from the operation.  Either ALL_ONE, ALL_ZERO, or MIXED
*/
static TreeType _Binop(int opno, const Bzet* bz1, int* p1, const Bzet* bz2, int* p2, Bzet* result, int* rpos, int lev);

//---------------------------------------------------------------------

/*  Performs a binary operation encoded by op on the data d1 and d2
*	returns: the result of the binary operation.
*/
extern __declspec(dllexport) BYTE do_data_op(int op, BYTE d1, BYTE d2);

//---------------------------------------------------------------------

static TreeType do_tree_op(int opno, int case_type, const Bzet* bz1, int* p1, const Bzet* bz2, int* p2, Bzet* result, int* rpos, int lev);

//---------------------------------------------------------------------

/*  Returns the complement of the subtree of bz starting at
*   index *p.  lev is the level of the subtree's root.  *p
*	will be set to the index of the start of the next subtree
*	after this function returns, or to bz->numNodes if there is
*   no subtree after the current one.
*/
static Bzet* _getNegatedSubtree(const Bzet* bz, int* p, int lev);

//---------------------------------------------------------------------

static void _traverseAndNegate(const Bzet* bitset, int* pos, int level, Bzet* result, int* rpos);

/**************************** Test Harness ****************************/

#ifdef DEBUG

static TreeType do_tree_op_DEBUG(int action, const Bzet* bz1, int* p1, const Bzet* bz2, int* p2, Bzet* result, int* rpos, int lev);
static void treeOpResetResult(Bzet* result);
static void treeOpResetResult2(Bzet* result);
void do_tree_op_Harness();

void negateHarn();

int binopCheckBytes(int opno, const BYTE data1[], const BYTE data2[], int len);

#endif // end if DEBUG

/**************************** Byte Array Conversion ****************************/

/*  After this function returns, '*bytes' will point to a a newly allocated array which
*   contains the bitstring encoded in the Bzet 'bz'.  *len will be the length of the array.
*  Example usage:
*
*   Bzet* bz = BZcreate(234);
*   BYTE* bytes = NULL;
*   int len;
*   BZconvertToByteArray(bz, &bytes, &len);
*/
extern __declspec(dllexport) int BZconvertToByteArray(const Bzet* bz, BYTE** bytes, int* len);

int recurseCreatearr(int lev,int* posBzet,int posArr,const Bzet* bz, BYTE* bytes);
BYTE reverseByte(BYTE toBeRev);

/********************Various Functions*************************/

extern __declspec(dllexport) int COUNT(const Bzet* bz);  // returns number of true bits in bz
extern __declspec(dllexport) int FIRST(const Bzet* bz);  // returns index of first true bit in bz
extern __declspec(dllexport) int LAST(const Bzet* bz);   // returns index of last true bit in bz
extern __declspec(dllexport) void LIST_T(const Bzet* bz,int** indicies,int* numIndicies);// prints index of every true bit

//used to recursively search the tree for the COUNT, FIRST,LAST, and LIST_T operations
// arguments for function parameter:
// 'c' for COUNT   'f' for FIRST
// 'L' for LAST    'p' for LIST_T
static int recurseSearch(char function, int lev,int* posBzet,int* posArr,const Bzet* bz, int data,int* done,int* true_array);



//recursive helper function for COUNT, FIRST, LAST, and LIST_T
int recurseCount(int lev,int* posBzet,const Bzet* bz,int data,int done);

//sets the nth bit in bz to 1
extern __declspec(dllexport) void BZset(Bzet* bz, int n);

//sets the nth bit in bz to 0
extern __declspec(dllexport) void BZunset(Bzet* bz, int n);

//If the nth bit in bz is 1, sets it to 0.  Else sets it to 1.
extern __declspec(dllexport) void BZflip(Bzet* bz, int n);

//returns 1 if bit n is set, else returns 0
extern __declspec(dllexport) int BZtest(const Bzet* bz, int n);

#endif // end ifedf BZET_H
