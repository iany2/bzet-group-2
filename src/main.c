#include <stdio.h>
#include "bzet.h"
#include "smokeTest.h"
#include "file_func.h"
#include <cassert>
#include "time.h"
#include "cli.h"
#include "randomBytes.h"

void notTest(const BYTE bytes[], int numBytes) {

	Bzet* original = BZcreateFromByteArray(bytes, numBytes);
	Bzet* inverted = invert(original);
	Bzet* uninverted = invert(inverted);

	if (memcmp(uninverted->nodes, original->nodes, (original->numNodes) * sizeof(BYTE)) != 0) {
		printf("Inversion test failed\n");
		exit(1);
	}

	printf("NOT succeeded\n");
	BZreleaseMemory(original);
	BZreleaseMemory(inverted);
	BZreleaseMemory(uninverted);
}

//Runs in 2.7 seconds without buffered binop
#define SIZE 1024*1024
//BYTE b1[SIZE] = {0x80, 0x6D, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

int main(int argc, char* argv[])
{	
#ifdef CMDLINE	
	return cmdLine(argc, argv);
#else
	clock_t start, stop;
	double t = 0.0;

	//BYTE b1[SIZE] = {0xF0, 0x45, 0x00, 0x00, 0xFD};  NAND fails!
	//BYTE b2[SIZE] = {0x0F, 0xC0, 0x11, 0xD0, 0x00};

	//BYTE b1[SIZE] = {0xFF, 0x03, 0x45, 0x00, 0x00, 0xFF, 0xFF, 0x12};   NAND fails!
	//BYTE b2[SIZE] = {0x00, 0x00, 0x00, 0xFF, 0xF0, 0x80, 0x03, 0x12};
	
	BYTE* b1 = (BYTE*)malloc(SIZE*sizeof(BYTE));
	BYTE* b2 = (BYTE*)malloc(SIZE*sizeof(BYTE));
	//BYTE b1[SIZE] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	//BYTE b2[SIZE] = { 0x62, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};

	start = clock();
	
	srand((unsigned int)time(NULL));

	randomBytes(b1, SIZE, 50);
	randomBytes(b2, SIZE, 50);

	bzSmoke();


	//OR
	if (!binopCheckBytes(DOR, b1, b2, SIZE)) {
		return 0;
	}
	printf("OR: binopCheckBytes succeeded\n");

	//AND
	if (!binopCheckBytes(DAND, b1, b2, SIZE)) {
		return 0;
	}
	printf("AND: binopCheckBytes succeeded\n");

	//XOR
	if (!binopCheckBytes(DXOR, b1, b2, SIZE)) {
		return 0;
	}
	printf("XOR: binopCheckBytes succeeded\n");

	//NOR
	if (!binopCheckBytes(DNOTOR, b1, b2, SIZE)) {
		return 0;
	}
	printf("NOR: binopCheckBytes succeeded\n");

	//NAND 
	if (!binopCheckBytes(DNOTAND, b1, b2, SIZE)) {
		return 0;
	}
	printf("NAND: binopCheckBytes succeeded\n");
	

	//DNOTXOR
	if (!binopCheckBytes(DNOTXOR, b1, b2, SIZE)) {
		return 0;
	}
	printf("NXOR: binopCheckBytes succeeded\n");

	//NOT
	notTest(b1, SIZE);

	free(b1);
	free(b2);
	
	stop = clock();
	t = (double) (stop-start)/CLOCKS_PER_SEC;
	printf("Run time: %f\n", t);
	
	return 0;
#endif
}
