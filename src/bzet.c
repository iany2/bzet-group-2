#include "bzet.h"
#include "smokeTest.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "stack.h"
#include "linkedlist.h"
#include <cassert>

//This array uses a 4 bit number as a hash key to
//find the matching case type for binary operations
const int cases[16] = { 
				 0,  //  0     0,0,  0, 0  DD
				 0,  //	 1     0,0,  0, 1  DD
                 0,  //	 2     0,0,  1, 0  DD
                 0,  //  3     0,0,  1, 1  DD
                 1,  //  4     0,1,  0, 0  0T
                 6,  //  5     0,1,  0,x1  0T
                 3,  //  6     0,1,  1, 0  1T
                 6,  //  7     0,1,  1,x1  1T
                 2,  //  8     1,0,  0, 0  T0
                 4,  //  9     1,0,  0, 1  T1
                 6,  // 10     1,0, x1, 0  T0
                 6,  // 11     1,0, x1, 1  T1
                 5,  // 12     1,1,  0, 0  TT
                 6,  // 13     1,1,  0,x1  TT
                 6,  // 14     1,1, x1, 0  TT
                 6   // 15     1,1, x1,x1  TT
				 };

//Rows are the binary operation, columns are the case type.
//We only care about columns 1 through 4, but since there are
//7 possible case types and we don't want an index error just
//because the wrong type is given, we label the other columns
//as "DC", for don't care.
int binopActions[NUM_BINOPS][NUM_CASETYPES]  = {            
	{ DC,  DB0,   DA0,   DB0,  DA0, DC, DC }, //FALSE
	{ DC,  DB1,   DA1,   DB1,  DA1, DC, DC }, //TRUE
	{ DC,  DB0,   DA0,    CB,   CA, DC, DC }, //AND
	{ DC,  CB,     CA,  NOTB, NOTA, DC, DC }, //XOR
	{ DC,  CB,     CA,   DB1,  DA1, DC, DC }, //OR	
	{ DC,  NOTB, NOTA,   DB0,  DA0, DC, DC }, //NOR
	{ DC,  NOTB, NOTA,    CB,   CA, DC, DC }, //EQ
	{ DC,  DB1,    DA1,  NOTB, NOTA, DC, DC }}; //NAND
 // Name     raw   Case1  Case2  Case3  Case4    
 // Inputs         0T     T0     1T     T1    
 //          0     1      2      3      4 

/*
int binopActions[16][6]  = {
    // 0        1     2      3      4      5        Tuple Index
    //                                               a= 0011
    //                                        operation ....
    //                                               b= 0101  Case                                 
    //                       index = a<<1 | b  ===>     0123    0               
	{ DC,   ZERO,  DB0, DA0, DB0,  DA0 }, //00 0000 Result		//FALSE
	{ DC,   DAND,  DB0, DA0,  CB,  CA  }, //01 0001    |		//AND
	{ DC,	  DC,	CB,  CA,  NB,  DA0 }, //02 0010    |		//A<-B
	{ DC,     DC,  DB0,  CA,  DB0, CA  }, //03 0011    V		//A
	{ DC,	  DC,  DB0, DA0,  DB0, CA  }, //04 0100				//????
	{ DC,     DC,  CB,  DA0,  CB,  DA0 }, //05 0101				//B
	{ DC,   DXOR,  CB,   CA,  NB,  NA  }, //06 0110				//XOR
	{ DC,    DOR,  CB,   CA,  DB1, DA1 }, //07 0111				//OR	
	{ DC, DNOTOR,  NB,   NA,  DB0, DA0 }, //08 1000				//NOR
	{ DC, DNOTXOR, NB,   NA,   CB,  CA }, //09 1001				//EQ
	{ DC,   NOTB,  NB,  DA0,   NB, DA0 }, //10 1010				//~B
	{ DC,	  DC,  CB,   CA,   CB,  CA }, //11 1011				//????
	{ DC,   NOTA, DB0,	 NA,  DB0,  NA }, //12 1100				//~A
	{ DC,	  DC, DB0,   NA,   CB,  CA }, //13 1101				//A->B
	{ DC, DNOTAND, CB,   DA1,  NB,  NA }, //14 1110				//NAND
	{ DC,    ONE,  DB1,  DA1, DB1, DA1 }}; //15 1111			//TRUE
    //       Shortcut  <-- C1 to C4 Actions -->  Recur  Result
 // Name     raw   Case1  Case2  Case3  Case4 Case5  Case 0   
 // Inputs         0T     T0     1T     T1    TT
 // 0        1     2      3      4      5 
 */

TreeType getTreeType(BYTE data, BYTE tree) {
	if (data == 0x00 && tree == 0x00)
		return ALL_ZERO;
	else if (data == 0xFF)
		return ALL_ONE;
	else
		return MIXED;
}

int arrToBzet(const BYTE bytes[], int size, Bzet** bz) {

	TreeType ret;
	int level = 0;
	int k = size;
	int powL;
	int padStart = size;
	Bzet* result = BZallocate();
	result->numNodes = 1;
	result->bufferSize = 1024;
	result->nodes = (BYTE*)malloc((result->bufferSize)*sizeof(BYTE)); //create a buffer

	while (k > 1) {
		k /= 8;
		level++;
	}

	powL = pow8(level);
	if (powL == size) {
		//size is a power of 8
		result->nodes[0] = level;
	}
	else {
		//size is not a power of 8.  Extend size to be a power
		//of 8, and set padStart to the old value so we know to
		//treat everything from bytes[padStart] to bytes[size-1] as zero
		level = powL < size ? level+1 : level;
		size = powL < size ? 8*powL : powL;
		result->nodes[0] = level;
	}

	ret = _arrToBzet(bytes, 0, size, padStart, result);
	//removeTrailingZeroes(result);

	*bz = result;
	return 1;
}

TreeType _arrToBzet(const BYTE bytes[], int pos, const int size, const int padStart, Bzet* result) {

	int isTreeRoot = (result->numNodes == 1);
	TreeType rootType;
	int rootPos = result->numNodes;
	int rootDataBytePos;
	int rootTreeBytePos;
	int partitionSize;
	int i;
	int padZero = (pos >= padStart);

	if (size < 8) {
		assert(size == 1);
		rootType = padZero ? ALL_ZERO : getTreeType(bytes[pos], 0x00);

		if (rootType == MIXED || isTreeRoot) {
			_BZpushBack(result, padZero ? 0x00 : reverseBits(bytes[pos]));
		}
		return rootType;
	}

	_BZpushBack(result, 0x00);
	rootDataBytePos = result->numNodes - 1;
	_BZpushBack(result, 0x00);
	rootTreeBytePos = result->numNodes - 1;
	partitionSize = size / 8;

	for (i = 0; i < 8; i++) {

		BYTE ithBit = 0x80 >> i;
		int partitionStart = i*partitionSize + pos;
		TreeType branchType = _arrToBzet( bytes, partitionStart, partitionSize, padStart, result );

		if (branchType == ZERO) {
		//leave tree and data bits as zero
		}
		else if (branchType == ONE) {
			//set data bit
			result->nodes[rootDataBytePos] |= ithBit;
		}
		else {
			//set tree bit
			result->nodes[rootTreeBytePos] |= ithBit;
		}
	}
	rootType = getTreeType(result->nodes[rootDataBytePos], result->nodes[rootTreeBytePos]);
	if ( rootType != MIXED && !isTreeRoot) {
		result->numNodes -= 2;
	}
	return rootType;
}

Bzet* BZallocateInit(BYTE* nodes, int numNodes) {

	Bzet* bz = (Bzet*)malloc(sizeof(Bzet));
	if (!bz)
		return NULL;
	bz->nodes = nodes;
	bz->numNodes = numNodes;
	bz->bufferSize = numNodes;
	return bz;
}

Bzet* BZallocate() {

	Bzet* bz = (Bzet*)malloc(sizeof(Bzet));
	if (!bz)
		return NULL;
	bz->numNodes = 0;
	bz->bufferSize = 0;
	bz->nodes = NULL;
	return bz;
}

Bzet* BZcreate(int n)
{
	Bzet* pBzet = NULL;
	int lev = 0;
	int numNodes = 1; //one extra node specifies the number of levels in the tree
	BYTE byte;
	int i;
	Stack stack;
	StackInit(&stack);

	if (n < 0) {
		printf("Error: CreateBzet: n must be positive");
		StackDestruct(&stack);
		return NULL;
	}

	pBzet = (Bzet*)malloc(sizeof(Bzet));
	if (!pBzet) {
		printf("Error: CreateBzet: failed to allocate memory");
		StackDestruct(&stack);
		return NULL;
	}

	//hold the nodes on a stack until we know how big of an array
	//to allocate for pBzet.
	StackPush(&stack, 0x80 >> (n & 0x07)); //push the level 0 node.  It only has a data byte, no tree byte.
	numNodes++;

	if (n < 8) {
		//no need to continue.  Add the level node and the data node
		//and we're done.
		//printf("0x%x 0x%x", pBzet->m_nodes[0], pBzet->m_nodes[1]);
		pBzet->numNodes = numNodes;
		pBzet->bufferSize = numNodes;
		pBzet->nodes = (BYTE*)malloc(numNodes*sizeof(BYTE));
		pBzet->nodes[0] = 0x00;
		StackPop(&stack, &(pBzet->nodes[1]));
		StackDestruct(&stack);
		return pBzet;
	}

	n /= 8;
	while (n > 0) {
		StackPush(&stack, 0x80 >> (n & 0x07)); //tree byte
		StackPush(&stack, 0);				  //data byte
		numNodes += 2;
		n /= 8;
		lev++;
	}

	pBzet->numNodes = numNodes;
	pBzet->bufferSize = numNodes;
	pBzet->nodes = (BYTE*)malloc(numNodes*sizeof(BYTE));
	pBzet->nodes[0] = lev;

	//get the elements on the stack
	i = 1;
	while (StackPop(&stack, &byte)) {
		pBzet->nodes[i] = byte;
		i++;
	}

	StackDestruct(&stack);

	return pBzet;
}

//-----------------------------------------------------------------

Bzet* BZcreateEmpty() {

	Bzet* bzet = NULL;
	bzet = (Bzet*)malloc(sizeof(Bzet));
	if (!bzet)
		return NULL;
	bzet->nodes = (BYTE*)malloc(2*sizeof(BYTE));
	if (!bzet->nodes)
		return NULL;
	bzet->nodes[0] = 0x00;
	bzet->nodes[1] = 0x00;
	bzet->numNodes = 2;
	bzet->bufferSize = 2;

	return bzet;
}

//-----------------------------------------------------------------

Bzet* BZcreateAdv(int indices[], int nIndices, Range ranges[], int nRanges)
{
	//TODO: Must implement OR, AND, and RANGE functions from the python code
	//	  before we can implement this function.
	return NULL;
}

//---------------------------------------------------------------------


Bzet* BZcreateFromByteArray(const BYTE bytes[], int nBytes) {

	int byteIndex;
	int bitIndex;
	Bzet* ret;

	if (!bytes || nBytes < 0)
		return NULL;

	arrToBzet(bytes, nBytes, &ret);
	return ret;

	//Initialize result to 0
	ret = BZcreateEmpty();

	for (byteIndex = 0; byteIndex < nBytes; byteIndex++) {

		for (bitIndex = 0; bitIndex < 8; bitIndex++) {

			int bitIsSet = bytes[byteIndex] & (0x01 << bitIndex);
			if (bitIsSet) {
				Bzet* temp = ret;
				Bzet* nextBit = BZcreate( 8*byteIndex + bitIndex );
				
				//printf("Bit %d set: bzet = ", 8*byteIndex + bitIndex);
				//BZprint(nextBit);

				ret = binop(DOR, temp, nextBit);

				//printf("OR results = ");
				//BZprint(ret);

				BZreleaseMemory(temp);
				BZreleaseMemory(nextBit);
			}
		}

		//if (byteIndex % 100 == 0)
			//printf("%d bytes completed\n", byteIndex);
	}

	return ret;
}

//-----------------------------------------------------------------

Bzet* BZcopy(const Bzet *other)
{
	Bzet* newBzet = NULL;
	if (!other)
		return NULL;
	newBzet = (Bzet*)malloc(sizeof(Bzet));
	if (newBzet) {

		newBzet->numNodes = other->numNodes;
		newBzet->bufferSize = other->bufferSize;
		if (other->nodes) {

			newBzet->nodes = (BYTE*)malloc( (other->numNodes) * sizeof(BYTE) );
			if (!newBzet->nodes) {
				free(newBzet);
				return NULL;
			}
			memcpy( newBzet->nodes, other->nodes, (other->numNodes) * sizeof(BYTE) );
		}
		else
			newBzet->nodes = NULL;

		return newBzet;
	}
	return NULL;
}

int BZlev(Bzet *b)
{
	if (b) {
		return b->nodes[0];
	}
	else
		return -1;
}

size_t BZsize(Bzet *b)
{
	if (b) {
		return b->numNodes * sizeof(BYTE);
	}
	else
		return -1;
}

void BZclean(Bzet *b)
{
	BZreleaseMemory(b);
	b = BZcreateEmpty();
}

void str(Bzet *b)
{
	BZprint(b);
}


//-----------------------------------------------------------------

void BZreleaseMemory(Bzet* pBzet)
{
	if (pBzet) {
		if (pBzet->nodes)
			free(pBzet->nodes);
		free(pBzet);
	}
}

/*******************************************************************
*							Utility
********************************************************************/

int BZisZero(const Bzet* bz) {
#ifdef DEBUG
	assert(bz && bz->nodes && bz->numNodes >= 2);
#endif
	if (bz->nodes[0] > 0) {
		//bz may be in extended form, have to check the tree node as well as data node
		return (bz->nodes[1] == 0x00) && (bz->nodes[2] == 0x00);
	}
	else {
		//bz is in normalized form, so zero always means [0x00][0x00]
		return (bz->nodes[1] == 0x00);
	}
}

//-----------------------------------------------------------------

static int _BZwriteToIndexAndBuffer(Bzet* bzet, int i, BYTE val) {

	//expand the current buffer if necessary
	if ( ! _BZbufferAtIndex(bzet, i) )
		return 0;

	//update the current size if necessary
	if (i >= bzet->numNodes)
		bzet->numNodes = i+1;

	//finally, write to the array
	bzet->nodes[i] = val;

	return 1;
}

//-----------------------------------------------------------------

int _BZbufferAtIndex(Bzet* bz, int index) {

#ifdef DEBUG
	assert(bz && index > 0);
#endif

	if (index < bz->bufferSize)
		return 2;
	if (_BZexpandBuffer(bz, 2*index))
		return 1;
	return 0;
}

//-----------------------------------------------------------------

int _BZexpandBuffer(Bzet* bz, int newBufferSize) {

	BYTE* newNodes = NULL;
#ifdef DEBUG
	assert(bz && newBufferSize >= 0);
#endif

	if (bz->numNodes >= newBufferSize)
		return 2;
	
	if (newNodes = (BYTE*)realloc(bz->nodes, newBufferSize*sizeof(BYTE))) {
		bz->nodes = newNodes;
		bz->bufferSize = newBufferSize;
		return 1;
	}
	return 0;
}

//-----------------------------------------------------------------

int _BZpushBack(Bzet* bz, BYTE val) {

	int numNodes;
	int bufferSize;

#ifdef DEBUG
	assert(bz);
#endif

	numNodes = bz->numNodes;
	bufferSize = bz->bufferSize;

	if (numNodes >= bufferSize) {
		//need to expand the buffer
		if (!_BZexpandBuffer(bz, 2 * bufferSize))
			return 0;
	}

	bz->nodes[numNodes] = val;
	bz->numNodes++;
	return 1;
}


//-----------------------------------------------------------------

BYTE reverseBits(BYTE x) {

	BYTE ret = 0x00;
	int i;
	for ( i = 0; i < 8; i++) {
		//move the ith bit into the 0th index and mask
		BYTE biti = (x >> i) & 0x01;
		//place the bit in its reversed location
		ret |= biti << (7-i);
	}
	return ret;
}

//-----------------------------------------------------------------

int count_bits(BYTE byte) // helper for findEndTree 
{			  //counts # of true bits in a byte
    int count = 0;
    int i;
    for(i=0;i<8;i++)
    {
        if((byte << i) & 0x80)
        {
            count++;
        }

    }
    return count;
}

//-----------------------------------------------------------------

int findEndTree(const Bzet* bitset, int pos, int level)
{
    int topLev = bitset->nodes[0];
    int initBranches;
    dataStack data; //stack to hold node Data
    int lev;
	int subTreesToExplore;

    if(topLev ==0 ) // one level node and one data so return 2
        return 2;
    if(level != 0) {
        initBranches = count_bits(bitset->nodes[pos+1]); //initial amt of sub trees to explore.  pos give the index of the data node, so pos+1 gives the treenode
	}
    else {
        initBranches = 0;
	}

	dataStackInit(&data);
    dataStackPush(&data, pos, level, initBranches); //pushes a new node w/ these vals

	pos = level != 0 ? pos + 2 : pos + 1;

    while(data.size != 0) // while still nodes to explore in subtree
    {
        dataStackNode* currNode = dataStackTop(&data); // current node is on top
		if (!currNode) {
			printf("fail");
		}
         if(currNode->subTrees > 0) // if still trees below the node
         {
               lev = currNode->newLev-1; //go down a level lev is the level of next subtree
               if(lev == 0) //if at level 0
               {
                 pos += currNode->subTrees; // advance through all data bytes
                 currNode->subTrees = 0; // no more subtrees from this node
               }
               else
               {
                 subTreesToExplore = count_bits(bitset->nodes[pos+1]); // count number of subtree of new index
                 dataStackPush(&data, pos, lev, subTreesToExplore);// push a new node w/ these vals
                 currNode->subTrees--; // explored single subtree of node so decrement
				 pos += 2; // move pos to the new index
               }
         }
         else {
			 dataStackNode* top = NULL;
			 top = dataStackPop(&data); // no more subtrees so done with node.  remove it from stack
			 if (top)
				 free(top);
		 }
     }

	 dataStackDestruct(&data);
     return pos; //position of next subtree
}

//------------------------------------------------------------------
int BZrealloc(Bzet* bz, int numBytes) {

	BYTE* temp = NULL;
	if (!bz)
		return 0;
	if (numBytes > bz->bufferSize) {
		//expand the buffer
		temp = (BYTE*)realloc(bz->nodes, 2*numBytes*sizeof(BYTE));
		if (!temp) {
			return 0;
		}
		bz->bufferSize = 2*numBytes;
		bz->nodes = temp;
	}
	
	if (numBytes > bz->numNodes) {
		bz->numNodes = numBytes;
	}
	return 1;
}

//-----------------------------------------------------------------

void BZprint(Bzet* b) {

	int i;
	for (i = 0; i < b->numNodes; i++) {

		if (!(b->nodes))
			printf("b has bad data");

		if (i == 0) {
			printf("Level %d\n", b->nodes[i]);
		}
		else {
			printf("0x%x\n", b->nodes[i]);
		}
	}
}

//-----------------------------------------------------------------

void BZfprint(FILE* fp, Bzet* b) {

	int i;
	for (i = 0; i < b->numNodes; i++) {

		if (!(b->nodes))
			fprintf(fp, "b has bad data");

		if (i == 0) {
			fprintf(fp, "Level %d\n", b->nodes[i]);
		}
		else {
			fprintf(fp, "0x%x\n", b->nodes[i]);
		}
	}
}

//-----------------------------------------------------------------

int _BZalign(Bzet* bzet1, Bzet* bzet2) {

	return _BZalignNormalized(bzet1,bzet2);

	//Deprecated code
	/*
	int levelDiff;
	int level;
	BYTE* tempNodes = NULL;
	int i;
	int k;
	int j;
	int newNumNodes;

	if (!bzet1 || !bzet2 || !bzet1->nodes || !bzet2->nodes)
		return 0;

	levelDiff = bzet1->nodes[0] - bzet2->nodes[0];
	if (levelDiff == 0)
		return 1; //bzets are already aligned
	if (levelDiff < 0) {
		//bzet2 is larger.  Swap the pointers bzet1 and bzet2
		//so that we can always call bzet1 the larger one instead
		//of writing a conditional branch for the case where bzet2 is larger
		swappbz(&bzet1, &bzet2);
	}

	levelDiff = abs(levelDiff);

	//bzet1 must always point to the larger Bzet by now
	{
		newNumNodes = 2*levelDiff + bzet2->numNodes;
		level = bzet1->nodes[0];

		tempNodes = (BYTE*)malloc( (newNumNodes)*sizeof(BYTE) );
		if (!tempNodes)
			return 0;

		//insert 2*levelDiff nodes at the front of bzet2
		tempNodes[0] = bzet1->nodes[0]; //the level node of the larger bzet
		k = levelDiff;
		i = 1;

		while (k > 0) {

#ifdef DEBUG
			assert(i+1 < newNumNodes);
#endif
			tempNodes[i] = 0x00;	//data byte
			tempNodes[i+1] = 0x80;  //tree byte
			i += 2;
			k--;
		}

		j = 1; //the first node of bzet2 is the level node, which we already got.
		while (i < newNumNodes) {
			//copy in the original bzet2 nodes
#ifdef DEBUG
			assert(j < bzet2->numNodes);
#endif
			tempNodes[i] = bzet2->nodes[j];
			i++;
			j++;
		}
	}
	free(bzet2->nodes);
	bzet2->nodes = tempNodes;
	bzet2->numNodes = newNumNodes;
	return 1;
	*/
}

//-----------------------------------------------------------------

int _BZalignNormalized(Bzet* bzet1, Bzet* bzet2) {
	int levelDiff;
	int level;
	BYTE* tempNodes = NULL;
	int i;
	int k;
	int j;
	int newNumNodes;
	int collapse;

	if (!bzet1 || !bzet2 || !bzet1->nodes || !bzet2->nodes)
		return 0;

	levelDiff = bzet1->nodes[0] - bzet2->nodes[0];
	if (levelDiff == 0)
		return 1; //bzets are already aligned
	if (levelDiff < 0) {
		//bzet2 is larger.  Swap the pointers bzet1 and bzet2
		//so that we can always call bzet1 the larger one instead
		//of writing a conditional branch for the case where bzet2 is larger
		swappbz(&bzet1, &bzet2);
	}

	levelDiff = abs(levelDiff);

	//Special case for all-zero bitstrings.  Just change the level node and return
	
	if (BZisZero(bzet2)) {
		free(bzet2->nodes);
		bzet2->numNodes = 3;
		bzet2->bufferSize = 3;
		bzet2->nodes = (BYTE*)malloc(3*sizeof(BYTE));
		bzet2->nodes[0] = bzet1->nodes[0]; bzet2->nodes[1] = 0x00; bzet2->nodes[2] = 0x00;
		return 1;
	}
	
	//bzet1 must always point to the larger Bzet by now
	{
		collapse = (bzet2->nodes[1] == 0xFF);

		newNumNodes = 2*levelDiff + bzet2->numNodes;
		if (collapse) {
			//We are going to collapse the node pair or leaf node
			//at bzet2->nodes[1].
			if (bzet2->nodes[0] == 0)
				newNumNodes--; //level is 0. remove a leaf node
			else
				newNumNodes -= 2; //remove a data-tree node pair

		}
		level = bzet1->nodes[0];

		tempNodes = (BYTE*)malloc( (newNumNodes)*sizeof(BYTE) );
		if (!tempNodes)
			return 0;

		//insert 2*levelDiff nodes at the front of bzet2
		tempNodes[0] = bzet1->nodes[0]; //the level node of the larger bzet
		k = levelDiff;
		i = 2*(levelDiff-1) + 1;

		//Special handling for collapsable data nodes.
		if (collapse) {
			//prepend [0x80 0x00] to the result
			tempNodes[i] = 0x80;
			tempNodes[i+1] = 0x00;
			i -= 2;
			k--;
		}

		while (k > 0) {

#ifdef DEBUG
			assert(i+1 < newNumNodes);
#endif
			//prepend [0x00 0x80] to the result
			tempNodes[i] = 0x00;	//data byte
			tempNodes[i+1] = 0x80;  //tree byte
			i -= 2;
			k--;
		}

		j = 1; //the first node of bzet2 is the level node, which we already got.
		for (i = 2*levelDiff+1; i < newNumNodes; i++) {
			//copy in the original bzet2 nodes
#ifdef DEBUG
			assert(j < bzet2->numNodes);
#endif
			tempNodes[i] = bzet2->nodes[j];
			j++;
		}
	}
	free(bzet2->nodes);
	bzet2->nodes = tempNodes;
	bzet2->numNodes = newNumNodes;
	if (bzet2->bufferSize < newNumNodes)
		bzet2->bufferSize = newNumNodes;
	return 1;
}

//-----------------------------------------------------------------

Bzet* _range(int start, int length) {
	
	Bzet* bzet = NULL;
	BYTE sm8;

	if (start < 0 || length < 0)
		return NULL;

	if (length == 0) {
		//return an empty Bzet
		return BZcreateEmpty();
	}

	//shortcut for very small bitsets
	if (start + length < 8) {
		bzet = (Bzet*)malloc(sizeof(Bzet));
		if (!bzet)
			return NULL;
		bzet->nodes = (BYTE*)malloc(2*sizeof(BYTE));
		if (!bzet->nodes) {
			free(bzet);
			return NULL;
		}
		bzet->nodes[0] = 0; //level 0
		bzet->nodes[1] = _mkbits(start, start + length - 1);
		bzet->numNodes = 2;
		bzet->bufferSize = 2;
		return bzet;
	}

	//shortcut for ranges 16 bits long or less
	sm8 = start & 0x07;
	if (sm8 + length <= 8) {
		//the bits within range are all in the same byte
		bzet = BZcreate(start);

		//withFirstBitSet has the special property that the very last
		//node will always be the byte that contains the set bit.  Since
		//the range is within this one byte, we only need to replace it
		//with the correct data byte.
		bzet->nodes[bzet->numNodes-1] = _mkbits(sm8, sm8 + length - 1);
		return bzet;
	}
	else if (sm8 + length <= 16) {
		//Do we actually need a special case for this?
		return _rangeSplitAcrossBytes(start, length);
	}
	else {
		//The range is split across multiple bytes
		return _rangeSplitAcrossBytes(start, length);
	}
}

//-----------------------------------------------------------------


Bzet* _rangeSplitAcrossBytes(int start, int length) {
	
	Bzet* begin = NULL;
	Bzet* end = NULL;
	BYTE* sx = NULL;
	BYTE* ex = NULL;
	int sxlen;
	int exlen;

	BYTE sbit, ebit;
	List head, tail;

	BYTE* headArr;
	BYTE* tailArr;
	BYTE* retArr;
	int headNormLen, tailNormLen, retLen;
	Bzet* ret;

	int ixs = 1;
	int ixe = 1;
	int do_lead = 1;
	int do_sx = 1;
	int dflag = 0;
	int ends = 0;
	int i;

	begin = BZcreate(start); //with just the first bit in the range set
	end = BZcreate(start + length - 1); //with just the last bit in the range set
	_BZalign(begin, end);
	sx = begin->nodes; //get pointers to the bytes, so we dont have to keep doing memory references
	ex = end->nodes;
	sxlen = begin->numNodes;
	exlen = end->numNodes;

	ListInit(&head);
	ListInit(&tail);
	ListAppend(&head, sx[0]); //append sx[0] to head
	ListAppend(&tail, 0x00); //append 0x00 to tail

    while (1) {
        // print( do_lead, do_sx, dflag, ixs, ixe )
        ends = sxlen;
        if (do_lead) {
            if (sx[ixs+1] == ex[ixe+1]) { // Check tree bytes
                // Both have same subtree
                // Both must have all zero data bits
                ListAppend(&head, sx[ixs]); // Data bytes always 0x00             
                ListAppend(&head, sx[ixs+1] ); // Tree bit
                ixs += 2;
                ixe += 2;
                continue;
			}
            else {
                // Subtrees have separated
                // Fill data portion with ones
                // between the two trees.
                do_lead = 0;
                dflag = 0;
                sbit = first_bit(sx[ixs+1])+1;
                ebit = first_bit(ex[ixe+1])-1;
                if (ebit - sbit > 0)
                    ListAppend(&head, _mkbits(sbit,ebit) ); // Data TODO: make sure _mkbits does the right thing here
                else
                    ListAppend(&head, 0x00 );              // NaDa
                ListAppend(&head, sx[ixs+1] | ex[ixe+1] ); // Table

                if (length <= 64 && ixs == sxlen-3) {
                    // Data has separated but no trees yet
                    // Just two data bytes.
                    // First data byte
                    sbit = first_bit(sx[ixs+2]);
                    ListAppend(&head, _mkbits(sbit,7) );

                    // Second data byte
                    ebit = first_bit(ex[ixe+2]);
                    ListAppend(&head, _mkbits(0,ebit) ); //TODO: make sure this does the right thing
                    
					
                    //ret = bytes( norm( head ) ); //TODO: what does this do?
					headArr = convertArr(&head);
					retLen = _norm(headArr, ListLength(&head));
					ret = (Bzet*)malloc(sizeof(Bzet));
					ret->nodes = headArr;
					ret->numNodes = retLen;
					ret->bufferSize = retLen;

					//cleanup
					BZreleaseMemory(begin);
					BZreleaseMemory(end);
					ListDestruct(&head);
					ListDestruct(&tail);

					return ret;
				}                   
                ixs += 2;
                ixe += 2;                    
                continue;
			}
		}
		else if (do_sx) {
            // Copy and post fill the rest of sx
            sbit = first_bit( sx[ixs+1] );
            if (dflag) {               
                ListAppend(&head, _mkbits(sbit,7));   // Data TODO: make sure this does the right thing
                do_sx = 0;
                dflag = 0;
                continue;
			}
            else {
                ListAppend(&head, _mkbits(sbit+1,7)); // Data TODO: make sure this does the right thing
                ListAppend(&head, sx[ixs+1]);        // Table
			}
            // print( "do_head ", Bits.hexx(head) )

            // Warn me if the end is near!
            // The end of sx always has only a data block
            // with a single bit in it.
            if (ixs+3 == ends) {
                dflag = 1;
                ixs += 1;
			}
            else
                ixs += 2;
            continue;
		}
        else {  // do_ex
            // Copy and pre fill the rest of ex
            ebit = first_bit( ex[ixe+1] );
            if (dflag) {                 
                ListAppend(&tail, _mkbits(0,ebit));  // Data TODO: make sure this does the right thing
                break;
			}
            else {
                ListAppend(&tail, _mkbits(0,ebit-1));  // Data TODO: make sure this does the right thing
                ListAppend(&tail, ex[ixe+1]);         // Table
			}
            // print( "do_tail ", Bits.hexx(tail) )
                
            // Warn me if the end is near!
            // Likewise for ex.
            if (ixe+3 == exlen) {
                dflag = 1;
                ixe += 1;
			}
            else
                ixe += 2;
            continue;
		}
	}
         
	headArr = convertArr(&head);
	tailArr = convertArr(&tail);
	headNormLen = _norm(headArr, ListLength(&head));
	tailNormLen = _norm(tailArr, ListLength(&tail));
	retLen = headNormLen + tailNormLen - 1;

	//combine headArr and tailArr, excluding the first element of tailArr
	retArr = (BYTE*)malloc((retLen)*sizeof(BYTE));
	for(i = 0; i < retLen; i++) {
		if (i < headNormLen) {
			retArr[i] = headArr[i];
		}
		else {
			retArr[i] = tailArr[1 + i - headNormLen];
		}
	}

	free(headArr);
	free(tailArr);

	ret = (Bzet*)malloc(sizeof(Bzet));
	ret->nodes = retArr;
	ret->numNodes = retLen;
	ret->bufferSize = retLen;
    //ret = bytes(norm(head) + norm( tail )[1:]); //what does this do? TODO:

	//cleanup
	BZreleaseMemory(begin);
	BZreleaseMemory(end);
	ListDestruct(&head);
	ListDestruct(&tail);
	
	return ret;
}

int _norm(BYTE* ba, int len ) {

	int i;
	int end_ba;
	BYTE x, y;
	if (len & 0x01) {
        // size is odd ... double data at end
        // See if it collapses
        if (ba[len-1] == 0xff) {
            int tfb = first_bit(ba[len-3]); // Find bit
#ifdef DEBUG
			assert(len-4 >= 0);
#endif
            x   = 0x80 >> tfb;			// Recreate the first bit
            y   = ba[len-3] ^ x;		// Remove first bit, y is second bit
            ba[len-3] ^= y;			    // Remove second bit from Tree
            ba[len-4] |= y;			    // Compress result by setting data bit
            len--;						// Remove the 0xff data byte
        // If it doesn't collapse -- nothing else can collapese either
		}
        else
			return len; 
	}      
    // Now process the rest... there is no double data any more          
    // Because we either compressed the last byte or we returned
    // knowing no more compression was possible.
	end_ba = len-1;
	for(i = end_ba; i > 1; i -= 2) {
        if (ba[len-1] == 0xff) {        // Is data full
#ifdef DEBUG
			assert(len-3 >= 0);
#endif
            ba[len-3] |= ba[len-2];     // yes, collapse into prior data
            ba[len-2] = 0;				// remove subtree
            if (i == end_ba)			// lonely D?
                len--;					// Delete it
            else
                len -= 2;				//full node. delete it.
		}
        else
			break;
	}

	return len;
}
	
//-----------------------------------------------------------------

BYTE _mkbits(int start, int end) {

	BYTE ret = 0;
	int i;

	if (start < 0 || start > 7 || end < start || end >= 8)
		return 0x00;

	for (i = start; i < end+1; i++) {
		ret |= (0x80 >> i);
	}
	return ret;
}

//-----------------------------------------------------------------

int first_bit(BYTE byte) {
	int i;
	for (i = 0; i < 8; i++) {
		if (0x80 & byte)
			return i;
		byte <<= 1;
	}
	return NO_BIT;
}

int last_bit(BYTE byte){
	int i;
	for(i = 0; i < 8; i++) {
		if(0x01 & byte)
			return 7-i;
		byte >>= 1;
	}
	return NO_BIT;
}

//-----------------------------------------------------------------

Bzet* stride(BIGNUM start, BIGNUM strd, BIGNUM length) {

	BIGNUM end; 
	int level = 0;
	Bzet* result;
	int buffer = 1024;
	int rpos = 1;
	BIGNUM k;
	BIGNUM bitpos = start;

	if (length < 0 || strd < 1 || start < 0)
		return NULL;

	end = start + length - 1;
	k = end;
	while (k >= 8) {
		k /= 8;
		level++;
	}

	result = BZallocate();
	result->bufferSize = buffer;
	result->nodes = (BYTE*)malloc(buffer*sizeof(BYTE));
	result->numNodes = 1;
	result->nodes[0] = level;
	_stride(start, strd, end, pow8(level), result, &rpos, &bitpos, level, 0);

	return result;
}

//-----------------------------------------------------------------

TreeType _stride(BIGNUM start, BIGNUM stride, BIGNUM end, BIGNUM subtreeNumBits, Bzet* result, int* rpos, BIGNUM* bitpos, int level, BIGNUM bitstart) {

	TreeType subtreeType;
	TreeType rootTreeType;
	int i;
	BYTE treeByte = 0x00;
	BYTE dataByte = 0x00;
	int rootPos = *rpos;

	//TODO: what to do about strides longer than one byte?
	//how to tell when to start advancing bitpos?
	if (level == 0) {

		rootTreeType = ALL_ZERO;

		//Are there bits set in this byte?
		if (bitstart <= *bitpos && *bitpos < bitstart+8 && *bitpos <= end) {

			dataByte = _mkStrideBits(bitpos, stride, end);
			rootTreeType = getTreeType(dataByte, 0x00);
			if (rootTreeType == MIXED) {
				//Only add a data byte if it can't be represented by
				//it's parent's data byte.
				_BZwriteToIndexAndBuffer(result, rootPos, dataByte);
				*rpos += 1;
			}
		}

		return getTreeType(dataByte, 0x00);
	}

	*rpos += 2;

	for (i = 0; i < 8; i++) {

		BYTE ithBit = 0x80 >> i;
		subtreeType = _stride(start, stride, end, subtreeNumBits/8, result, rpos, bitpos, level-1, bitstart + i*subtreeNumBits);

		//Fill in tree and data bits
		if (subtreeType == ALL_ONE) {
			dataByte |= ithBit;
		}
		else if (subtreeType == MIXED) {
			treeByte |= ithBit;
		}
	}

	rootTreeType = getTreeType(dataByte, treeByte);
	if (rootTreeType != MIXED) {
		*rpos -= 2;
	}
	else {
		_BZwriteToIndexAndBuffer(result, rootPos, dataByte);
		_BZwriteToIndexAndBuffer(result, rootPos+1, treeByte);
	}

	return rootTreeType;
}

//-----------------------------------------------------------------

BYTE _mkStrideBits(BIGNUM* bitpos, BIGNUM stride, BIGNUM end) {

	BYTE ret = 0x00;
	BIGNUM start = (*bitpos) & 0x7;
	BIGNUM i = start;
	while (i < 8 && (*bitpos <= end)) {
		ret |= (0x80 >> i);
		i += stride;
		*bitpos += stride;
	}
	return ret;
}

//-----------------------------------------------------------------

int BZgetNumLevels(const Bzet* bz) {

#ifdef DEBUG
	if (!bz || !bz->nodes) {
		printf("Error: bitset nodes have not been allocated.  Cannot determine leve.\n");
		exit(1);
	}
#endif
	if (bz->nodes && bz->numNodes > 0)
		return bz->nodes[0];
	return 0;
}

//-----------------------------------------------------------------

void swappbz(Bzet** bz1, Bzet** bz2) {
	Bzet* temp = *bz1;
	*bz1 = *bz2;
	*bz2 = temp;
}

//-----------------------------------------------------------------

void removeTrailingZeroes(Bzet* bz) {
	BYTE* nodes = bz->nodes;
	int lev = nodes[0];
	int pos = 1;
	int n, i;
	BYTE dataByte, treeByte;

	//special case of an all zero tree with too many levels
	if (lev > 0 && nodes[1] == 0x00 && nodes[2] == 0x00) {
		bz->numNodes = 2;
		bz->bufferSize = 2;
		nodes[0] = 0x00;
		nodes[1] = 0x00;
		return;
	}

	while (lev > 0) {

		dataByte = nodes[pos];
		treeByte = nodes[pos+1];

		if (treeByte == 0x80 && dataByte == 0x00) {
			pos += 2;
			lev--;
		}
		else {
			break;
		}
	}

	if (lev > 0) {
		//either the tree is ok from this point on, or it is a [0x80 0x00] or [0x00 0x00]
		//pair that needs to be collapsed by one level.
		dataByte = nodes[pos];
		treeByte = nodes[pos+1];
		if (treeByte == 0x00 && (dataByte == 0x80 || dataByte == 0x00)) {
			//this pair of bytes needs to be collapsed to one level lower
			lev--;
			nodes[0] = lev;
			nodes[1] = dataByte == 0x80 ? 0xFF : 0x00;
			if (lev > 0) {
				nodes[2] = treeByte;
				bz->numNodes = 3;
				bz->bufferSize = 3;
			}
			else {
				bz->numNodes = 2;
				bz->bufferSize = 2;
			}
			return;
		}

		//else the tree is ok from this point on.  keep what remains

		if (pos == 1) {
			//no zeroes need removing.  No need to waste time in the for loop below
			return;
		}
		n = 1 + bz->numNodes - pos;
		nodes[0] = lev;
		for (i = 1; i < n; i++) {
			nodes[i] = nodes[pos + i - 1];
		}
		bz->numNodes = n;
		if (bz->bufferSize < n)
			bz->bufferSize = n;
		return;
	}
	else {
		//level is 0.  Just one data byte to copy
		nodes[0] = lev;
		nodes[1] = nodes[pos];
		bz->numNodes = 2;
		bz->numNodes = 2;
		return;
	}

}

void freeByteArray(BYTE* bytes) {

	if (bytes)
		free(bytes);
}

/******************* Binary Operations *****************************/

Bzet* binop(int opno, const Bzet* bz1, const Bzet* bz2) {

	Bzet* result = BZallocate();
	Bzet* bz1aligned = BZcopy(bz1);
	Bzet* bz2aligned = BZcopy(bz2);
	int p1, p2, rpos, lev;

	result->nodes = (BYTE*)malloc(1024*sizeof(BYTE));
	assert(result->nodes);
	result->numNodes = 2;
	result->bufferSize = 1024;

	_BZalignNormalized(bz1aligned, bz2aligned);
	lev = BZgetNumLevels(bz1aligned); //get the level.  bz1aligned and bz2aligned should be aligned so their levels are the same
	result->nodes[0] = lev;

	p1 = p2 = rpos = 1;
	_Binop(opno, bz1aligned, &p1, bz2aligned, &p2, result, &rpos, lev);

	BZreleaseMemory(bz1aligned);
	BZreleaseMemory(bz2aligned);

	//remove extraneous trailing zeros, and change the level node to reflect this.
	//removeTrailingZeroes(result);

	return result;
}

//-----------------------------------------------------------------

TreeType _Binop(int opno, const Bzet* bz1, int* p1, const Bzet* bz2, int* p2, Bzet* result, int* rpos, int lev) {

	BYTE ithBit = 0x80;
	int rootPos = *rpos;
	BYTE rootTreeByte = 0x00;
	BYTE rootDataByte = 0x00;
	BYTE bz1RootDataByte, bz2RootDataByte, bz1RootTreeByte, bz2RootTreeByte;
	TreeType subTreeType;
	TreeType ret;
	int i;
	int isTreeRoot = (*rpos == 1); //some special handling is needed if we are dealing with the tree root, and not a subtree
	int case_type;

	//handle level 0 trees specially
	if (lev == 0) {
		//There are only data bytes, no tree bytes, and no subtrees
#ifdef DEBUG
	assert(*p1 < bz1->numNodes && *p2 < bz2->numNodes);
#endif
		rootDataByte = do_data_op(opno, bz1->nodes[*p1], bz2->nodes[*p2]);
		if (*p1 < bz1->numNodes)
		*p1 += 1;
		if (*p2 < bz2->numNodes)
		*p2 += 1;

		//Only add the data byte if it is a mixture of 1's and 0's
		//Otherwise, it is already represented by its parent node
		//A special case is needed if this is the tree root, since it has no parent!
		if ((rootDataByte != 0x00 && rootDataByte != 0xFF) || isTreeRoot) {
			//make sure result has room to write to
			BZrealloc(result, rootPos+1);
			result->nodes[rootPos] = rootDataByte; 
			*rpos += 1;
		}
		if (rootDataByte == 0x00)
			return ALL_ZERO;
		else if (rootDataByte == 0xFF)
			return ALL_ONE;
		else
			return MIXED;
	}

	//"Add space" for the root by incrementing rpos.  We won't actually
	//fill in the root node until we know for sure that we need to.  We "think"
	//the root is going to take up 2 bytes, one for data, one for tree, but it
	//could be that it is all 1's or all 0's and can be represented by its parent node.
	*rpos += 2;

#ifdef DEBUG
	assert((*p1+1) < bz1->numNodes && (*p2+1) < bz2->numNodes);
#endif

	bz1RootDataByte = bz1->nodes[*p1]; bz2RootDataByte = bz2->nodes[*p2];
	bz1RootTreeByte = bz1->nodes[*p1+1]; bz2RootTreeByte = bz2->nodes[*p2+1];

	//move position pointers so they point to the left-most subtree,
	//or perhaps past the end of the tree if there are no subtrees.
	if (*p1 < bz1->numNodes)
		*p1 += 2;
	if (*p2 < bz2->numNodes)
		*p2 += 2;

	//process subtrees, and fill in the bits of the root node
	for (i = 0; i < 8; i++) {

		BYTE bz1DataBit, bz2DataBit, bz1TreeBit, bz2TreeBit;
		BYTE caseIndex;

		ithBit = 0x80 >> i;
		//mask the ith bit in the data bytes and tree bytes of bz1 and bz2
		bz1DataBit = (ithBit & bz1RootDataByte);
		bz2DataBit = (ithBit & bz2RootDataByte);
		bz1TreeBit = (ithBit & bz1RootTreeByte);
		bz2TreeBit = (ithBit & bz2RootTreeByte);

		//shift bz1TreeBit, bz2TreeBit, bz1DataBit, and bz2DataBit into bit indices 3,2,1,0,
		//respectively, where index 0 is the rightmost index (contrary to the bzet bit representation
		//where index 0 is the leftmost bit.  But we need index 0 to be the rightmost for the purpose
		//of calculating an index into the cases array).
		caseIndex = (!!bz1TreeBit << 3) | (!!bz2TreeBit << 2) | (!!bz1DataBit << 1) | (!!bz2DataBit);
		case_type = cases[caseIndex];

		if (case_type == 0) {
			//Data and Data case
			BYTE isDataBitSet = do_data_op(opno, bz1DataBit, bz2DataBit);
			isDataBitSet = ithBit & isDataBitSet; //we are only interested in the ith bit
			rootDataByte |= isDataBitSet; //set the bit in rootDataByte at index i to 1 iff the ith bit in isDataBitSet is 1 
		}
		else if ( (1 <= case_type) && (case_type <= 4) ) {
			//Data and mixed tree
			subTreeType = do_tree_op(opno, case_type, bz1, p1, bz2, p2, result, rpos, lev-1);

			if (subTreeType == MIXED) {
				//set the tree bit
				rootTreeByte |= ithBit;
			}
			else if (subTreeType == ALL_ONE) {
				//set data bit
				rootDataByte |= ithBit;
			}
		}
		else if (case_type == 5) {
			//both bz1 and bz2 are mixed trees.  Do the ith subtree recursively

			//the recursive call
			subTreeType = _Binop(opno, bz1, p1, bz2, p2, result, rpos, lev-1);
			
			//set the ith tree and data bit in the root node according to what the ith subtree
			//corresponding turned out to be
			switch (subTreeType) {

			case ALL_ZERO:
					//set both tree and data bits to 0
					rootDataByte &= ~ithBit;
					rootTreeByte &= ~ithBit;
				break;

			case ALL_ONE:
					//set data bit to 1, tree bit to 0
					rootDataByte |= ithBit;
					rootTreeByte &= ~ithBit;
				break;

			case MIXED:
					//set data bit to 0, tree bit to 1
					rootDataByte &= ~ithBit;
					rootTreeByte |= ithBit;
				break;

			default: 
				printf("_Binop returned an unrecognized code.\n");
				exit(1); //PHAIL!
			}
		}
		else {
			printf("Bad data");
			assert(0);
		}
	}

	//Is the root node all zero or all ones?
	ret = MIXED;
	if ( (rootDataByte == 0x00 || rootDataByte == 0xFF) && (rootTreeByte == 0x00) && !isTreeRoot ) {

		ret = (rootDataByte == 0xFF) ? ALL_ONE : ALL_ZERO;
#ifdef DEBUG
		assert(*rpos == rootPos + 2); //recursive calls should not have added to the result, since they assume parent nodes will
									  //represent data that is all ones or zeros.
#endif
		//Don't need to add this root node.  It's parent will represent it in the data byte.
		//remove the space we set aside the root node by decrementing rpos
		*rpos -= 2;
	}
	else {
		//add the root bytes.  Make sure there is room first.
		//BZrealloc will only reallocate memory if we have exceeded the buffer size.
		//Note that we BZrealloc to *rpos, and NOT rootPos, because there may
		//have been data written to indices greater than rootPos.
		BZrealloc(result, *rpos);

#ifdef DEBUG
		assert((rootPos+1) < result->numNodes);
#endif
		result->nodes[rootPos] = rootDataByte;
		result->nodes[rootPos+1] = rootTreeByte;
	}

	//return this tree's type.  ALL_ZERO, ALL_ONE, or MIXED
	return ret;
}

BYTE do_data_op(int op, BYTE d1, BYTE d2)
{
	BYTE dr;
	switch ( op ) {
		case DAND:
			dr = d1 & d2;
			break;
		case DOR:
			dr = d1 | d2;
			break;
		case DXOR:
			dr = d1 ^ d2;
			break;
		case DNOTAND:
			dr = ~(d1 & d2);
			break;
		case DNOTOR:
			dr = ~(d1 | d2);
			break;
		case DNOTXOR:
			dr = ~(d1 ^ d2);
			break;
		case ZERO:
			dr = 0x00;
			break;
		case ONE:
			dr = 0xFF;
			break;
		default:
			printf("Op is not a valid argument, for function do_data_op.\n");
#ifdef DEBUG
			assert(0);
#endif
			break;
	}
	return dr;
}

int pow8(int n)
{
	int powersof8[10] = {1,8,64,512,4096,32768,262144,2097152,16777216,134217728};
	if (n < 0)
		return -1;
	if (n < 10)
		return powersof8[n];
	return (int)pow(8,n);
}


//-----------------------------------------------------------------

TreeType do_tree_op(int opno, int case_type, const Bzet* bz1, int* p1, const Bzet* bz2, int* p2, Bzet* result, int* rpos, int lev) {

	//Look up the appropriate action from table of binary operations
	int action = binopActions[opno][case_type];
	int end;
	int numElements;
	Bzet* temp = NULL;

	switch (action) {

	case DA1:
		//The result of bz1 OP bz2 is all one, and we should advance the position within bz1
		*p1 = findEndTree(bz1, *p1, lev);
#ifdef DEBUG
		assert(*p1 <= bz1->numNodes);
#endif
		return ALL_ONE;
		break;

	case DA0:
		//The result is all zero, and we should advance the position within bz1
		*p1 = findEndTree(bz1, *p1, lev);
#ifdef DEBUG
		assert(*p1 <= bz1->numNodes);
#endif
		return ALL_ZERO;
		break;

	case DB1:
		//the result is all 1.  advance position in bz2
		*p2 = findEndTree(bz2, *p2, lev);
#ifdef DEBUG
		assert(*p2 <= bz2->numNodes);
#endif
		return ALL_ONE;
		break;

	case DB0:
		//the result is all 0. advance position in bz2
		*p2 = findEndTree(bz2, *p2, lev);
#ifdef DEBUG
		assert(*p2 <= bz2->numNodes);
#endif
		return ALL_ZERO;
		break;

	case CA:
		//the result should be the subtree of bz1 starting at p1.
		//copy this tree into the result and advance position in bz1
		end = findEndTree(bz1, *p1, lev);
		numElements = end - *p1;
		BZrealloc(result, *rpos + numElements);
#ifdef DEBUG
		assert(result->nodes);
		assert(*rpos < result->numNodes && (*p1 + numElements) <= bz1->numNodes && (*rpos + numElements) <= result->numNodes);
#endif
		memcpy(result->nodes + *rpos, bz1->nodes + *p1, numElements*sizeof(BYTE));
		*rpos += numElements;
		*p1 = end;
		return MIXED;
		break;

	case CB:
		//the result should be the subtree of bz2 starting at p2.
		//copy this tree into the result and advance position in bz2
		end = findEndTree(bz2, *p2, lev);
		numElements = end - *p2;
		BZrealloc(result, *rpos + numElements);
#ifdef DEBUG
		assert(*rpos < result->numNodes && (*p2 + numElements) <= bz2->numNodes && (*rpos + numElements) <= result->numNodes);
#endif
		memcpy(result->nodes + *rpos, bz2->nodes + *p2, numElements*sizeof(BYTE));
		*rpos += numElements;
		*p2 = end;
		return MIXED;

		break;

	case NOTA: 

		//Copy the negation of the subtree of bz1 starting at *p1
		//into the result, starting at rpos.  Increment *p1 and *rpos
		//accordingly.
		_traverseAndNegate(bz1, p1, lev, result, rpos);

		return MIXED;

		break;

	case NOTB: 

		//Copy the negation of the subtree of bz2 starting at *p2
		//into the result, starting at *rpos.  Increment *p1 and *rpos
		//accordingly.
		_traverseAndNegate(bz2, p2, lev, result, rpos);

		return MIXED;

		break;

	default:
		printf("Error: bad index in do_tree_op: opno = %d, case_type = %d\n", opno, case_type);
#ifdef DEBUG
		assert(0);
#endif
		return ALL_ZERO; //this value is relatively harmless
	}
}

//-----------------------------------------------------------------

/*
Bzet* _getNegatedSubtree(const Bzet* bz, int* p, int lev) {

	//TODO: implement this function more efficiently.

	Bzet* result = NULL;
	List tempResult;
	ListInit(&tempResult);
	ListAppend(&tempResult, lev);

	*p = _traverseAndNegate(bz, *p, lev, &tempResult);

#ifdef DEBUG
		assert(*p <= bz->numNodes);
#endif

	//copy list into result
	result = BZallocate();
	SET_NODES(result, convertArr(&tempResult));
	SET_NUM_NODES(result, ListLength(&tempResult));
	result->bufferSize = ListLength(&tempResult);

	//remove trailing zeros that may have resulted from the operation
	//removeTrailingZeroes(result);

	ListDestruct(&tempResult);

	return result;
}
*/

//-----------------------------------------------------------------

void addNegatedNode(const Bzet* bz, int pos, Bzet* result, int* rpos) {

	BYTE dataByte = bz->nodes[pos];
	BYTE treeByte = bz->nodes[pos+1];
	BYTE negatedDataByte = (~dataByte) ^ treeByte;

	_BZwriteToIndexAndBuffer(result, *rpos, negatedDataByte);
	*rpos = *rpos + 1;

	_BZwriteToIndexAndBuffer(result, *rpos, treeByte);
	*rpos = *rpos + 1;
	//tree byte doesn't change
}

//-----------------------------------------------------------------

void addNegatedLeaf(const Bzet* bz, int pos, Bzet* result, int* rpos) {

	BYTE dataByte = bz->nodes[pos];
	_BZwriteToIndexAndBuffer(result, *rpos, ~dataByte);
	*rpos = *rpos + 1;
	//tree byte doesn't change
}

//-----------------------------------------------------------------

void _traverseAndNegate(const Bzet* bitset, int* pos, int level, Bzet* result, int* rpos) {

    int initBranches;
    dataStack data; //stack to hold node Data
    int lev;
	int i;
	int subTreesToExplore;
	BYTE* bnodes = GET_NODES(bitset);

    if(level != 0) {
        initBranches = count_bits(bitset->nodes[*pos+1]); //initial amt of sub trees to explore.  pos give the index of the data node, so pos+1 gives the treenode
	}
    else {
        initBranches = 0;
	}

	dataStackInit(&data);
    dataStackPush(&data, *pos, level, initBranches); //pushes a new node w/ these vals
	if (level > 0) {
#ifdef DEBUG
		assert((*pos+1) < bitset->numNodes);
#endif
		//add a tree and data byte
		addNegatedNode(bitset, *pos, result, rpos);
	}
	else {
#ifdef DEBUG
		assert((*pos) < bitset->numNodes);
#endif
		//add only a data byte
		addNegatedLeaf(bitset, *pos, result, rpos);
	}

	*pos = level != 0 ? *pos + 2 : *pos + 1;

    while(data.size != 0) // while still nodes to explore in subtree
    {
        dataStackNode* currNode = dataStackTop(&data); // current node is on top
         if(currNode->subTrees > 0) // if still trees below the node
         {
               lev = currNode->newLev-1; //go down a level lev is the level of next subtree
               if(lev == 0) //if at level 0
               {
					for (i = 0; i < currNode->subTrees; i++) {
#ifdef DEBUG
		assert((*pos) < bitset->numNodes);
#endif
						addNegatedLeaf(bitset, *pos, result, rpos);
						*pos = *pos + 1;
					}
                 //pos += currNode->subTrees; // advance through all data bytes
                 currNode->subTrees = 0; // no more subtrees from this node
               }
               else
               {
                 subTreesToExplore = count_bits(bitset->nodes[*pos+1]); // count number of subtree of new index
                 dataStackPush(&data, *pos, lev, subTreesToExplore);// puss a new node w/ these vals
					
#ifdef DEBUG
		assert((*pos+1) < bitset->numNodes);
#endif
				 addNegatedNode(bitset, *pos, result, rpos);

                 currNode->subTrees--; // explored single subtree of node so decrement
				 *pos += 2; // move pos to the new index
               }
         }
         else
			dataStackPop(&data); // no more subtrees so done with node.  remove it from stack
     }

	 dataStackDestruct(&data);
}

//-----------------------------------------------------------------

Bzet* invert(const Bzet* bz) {

	Bzet* complement = NULL;
	int lev;
	int from;
	int to;

	if (!bz)
		return NULL;

	lev = bz->nodes[0];
	to = 1;
	from = 1;
	complement = BZcreateEmpty();
	BZrealloc(complement, 1);
	complement->nodes[0] = lev;

	_traverseAndNegate(bz, &from, lev, complement, &to);

	return complement;
}


//-----------------------------------------------------------------
//					scaffolding for do_tree_op
//-----------------------------------------------------------------

#ifdef DEBUG

TreeType do_tree_op_DEBUG(int action, const Bzet* bz1, int* p1, const Bzet* bz2, int* p2, Bzet* result, int* rpos, int lev) {

	int r, c;
	
	//find action in the binop table
	for (r = 0; r < NUM_BINOPS; r++) {
		for (c = 0; c < NUM_CASETYPES; c++) {
			if (binopActions[r][c] == action) {
				return do_tree_op( r, c, bz1, p1, bz2, p2, result, rpos, lev);
			}
		}
	}

	printf("do_tree_op_DEBUG got a bad action code\n");
	return ALL_ZERO;
}

void treeOpResetResult(Bzet* result) {
	free(result->nodes);
	result->nodes = (BYTE*)malloc(2*sizeof(BYTE));
	result->numNodes = 2;
	result->bufferSize = 2;
	result->nodes[0] = 1;
}

void treeOpResetResult2(Bzet* result) {

	free(result->nodes);
	result->nodes = (BYTE*)malloc(4*sizeof(BYTE));
	result->numNodes = 4;
	result->bufferSize = 4;
	result->nodes[0] = 0xDE;
	result->nodes[1] = 0xAD;
	result->nodes[2] = 0xBE;
	result->nodes[3] = 0xEF;
}

void do_tree_op_Harness() {

//Create some artifial bzets to facilitate testing
	Bzet* bzTree = BZcreateFromStr("[0x01][0x00][0x88][0xDE][0xAD]");
	Bzet* bzZero = BZcreateFromStr("[0x01][0x00][0x00]");
	Bzet* bzOne = BZcreateFromStr("[0x01][0xFF][0x00]");
	Bzet* result = BZallocate();
	Bzet* bz1;
	Bzet* bz2;
	int p1, p2, rpos, lev = 1;
	int ZERO_T = 1, T_ZERO = 2, ONE_T = 3, T_ONE = 4; //case types to test 
	TreeType tt;
	result->nodes = (BYTE*)malloc(2*sizeof(BYTE));
	result->numNodes = 2;
	result->bufferSize = 2;
	result->nodes[0] = 1;

//AND
	//0T
	p1 = 1; p2 = 1; rpos = 1;
	tt = do_tree_op(DAND, ZERO_T, bzZero, &p1, bzTree, &p2, result, &rpos, lev);
	//BZprint(result);
	assert(tt == ALL_ZERO && p1 == 1 && p2 == 5 && rpos == 1);
	treeOpResetResult(result);
	

	//T0
	p1 = 1; p2 = 1; rpos = 1;
	tt = do_tree_op(DAND, T_ZERO, bzTree, &p1, bzZero, &p2, result, &rpos, lev);
	assert(tt == ALL_ZERO && p1 == 5 && p2 == 1 && rpos == 1);
	treeOpResetResult(result);

	//1T
	p1 = 1; p2 = 1; rpos = 1;
	tt = do_tree_op(DAND, ONE_T, bzOne, &p1, bzTree, &p2, result, &rpos, lev);
	assert(tt == MIXED && p1 == 1 && p2 == 5 && rpos == 5 && BzetEquals(result, "[0x01][0x00][0x88][0xDE][0xAD]"));
	treeOpResetResult(result);

	//T1
	p1 = 1; p2 = 1; rpos = 1;
	tt = do_tree_op(DAND, T_ONE, bzTree, &p1, bzOne, &p2, result, &rpos, lev);
	assert(tt == MIXED && p1 == 5 && p2 == 1 && rpos == 5 && BzetEquals(result, "[0x01][0x00][0x88][0xDE][0xAD]"));
	treeOpResetResult(result);

	BZreleaseMemory(bzTree);
	BZreleaseMemory(bzZero);
	BZreleaseMemory(bzOne);

//target each case in the switch statement explicitly with a special version of do_tree_op
//that allows us to set witch case to process explicitly, rather than by look-up in the binopActions table

	bz1 = BZcreateFromStr("[0x02][0x0F][0x40][0x00][0x80][0xAA]");
	bz2 = BZcreateFromStr("[0x02][0x21][0x80][0xDE][0x00]");
	treeOpResetResult2(result);
	assert(BzetEquals(result, "[0xDE][0xAD][0xBE][0xEF]"));

	//DA1
	p1 = 1; p2 = 1; rpos = 0;
	tt = do_tree_op_DEBUG(DA1, bz1, &p1, bz2, &p2, result, &rpos, 2);
	assert(p1 == 6 && p2 == 1 && rpos == 0 && tt == ALL_ONE && BzetEquals(result, "[0xDE][0xAD][0xBE][0xEF]"));

	//DA0
	p1 = 1; p2 = 1; rpos = 0; 	
	treeOpResetResult2(result);
	tt = do_tree_op_DEBUG(DA0, bz1, &p1, bz2, &p2, result, &rpos, 2);
	assert(p1 == 6 && p2 == 1 && rpos == 0 && tt == ALL_ZERO && BzetEquals(result, "[0xDE][0xAD][0xBE][0xEF]"));

	//DB1
	p1 = 1; p2 = 1; rpos = 0;
	treeOpResetResult2(result);
	tt = do_tree_op_DEBUG(DB1, bz1, &p1, bz2, &p2, result, &rpos, 2);
	assert(p1 == 1 && p2 == 5 && rpos == 0 && tt == ALL_ONE && BzetEquals(result, "[0xDE][0xAD][0xBE][0xEF]"));

	//DB0
	p1 = 1; p2 = 1; rpos = 0;
	treeOpResetResult2(result);
	tt = do_tree_op_DEBUG(DB0, bz1, &p1, bz2, &p2, result, &rpos, 2);
	assert(p1 == 1 && p2 == 5 && rpos == 0 && tt == ALL_ZERO && BzetEquals(result, "[0xDE][0xAD][0xBE][0xEF]"));

	//CA
	p1 = 1; p2 = 1; rpos = 0;
	treeOpResetResult2(result);
	tt = do_tree_op_DEBUG(CA, bz1, &p1, bz2, &p2, result, &rpos, 2);
	assert(p1 == 6 && p2 == 1 && rpos == 5 && tt == MIXED && BzetEquals(result, "[0x0F][0x40][0x00][0x80][0xAA]"));

	//CB
	p1 = 1; p2 = 1; rpos = 0;
	treeOpResetResult2(result);
	tt = do_tree_op_DEBUG(CB, bz1, &p1, bz2, &p2, result, &rpos, 2);
	assert(p1 == 1 && p2 == 5 && rpos == 4 && tt == MIXED && BzetEquals(result, "[0x21][0x80][0xDE][0x00]"));

	//NOTA

	//NOTB

	BZreleaseMemory(result);
	BZreleaseMemory(bz1);
	BZreleaseMemory(bz2);
}

void negateHarn() {
	/*
	Bzet* bz = BZcreateFromStr("[0x00][0xAA]");
	int p, lev;
	Bzet* result = NULL;
	
	p = 1; lev = 0;
	result = _getNegatedSubtree(bz, &p, lev);
	assert(BzetEquals(result, "[0x00][0x55]"));
	BZreleaseMemory(result);
	BZreleaseMemory(bz);

	p = 1; lev = 1;
	bz = BZcreateFromStr("[0x01][0x80][0x40][0xAA]");
	result = _getNegatedSubtree(bz, &p, lev);
	assert(BzetEquals(result, "[0x01][0x3F][0x40][0x55]"));
	BZreleaseMemory(result);
	BZreleaseMemory(bz);

	p = 1; lev = 1;
	bz = BZcreateFromStr("[0x01][0x7F][0x80][0xAA]");
	result = _getNegatedSubtree(bz, &p, lev);
	assert(BzetEquals(result, "[0x00][0x55]"));
	BZreleaseMemory(result);
	BZreleaseMemory(bz);
	*/
}

//-----------------------------------------------------------------

int binopCheckBytes(int opno, const BYTE data1[], const BYTE data2[], int len) {

	int i, areSame, binopDecompressedLength;
	BYTE* checkResult = NULL;
	BYTE* binopDecompressedResult = NULL;
	Bzet* binopResult = NULL;
	Bzet* bz1 = NULL;
	Bzet* bz2 = NULL;
	BYTE* tempCheck = NULL;
	int tempCheckLen;
	FILE* pfLog = NULL;

	if (!data1 || !data2)
		return 0;

	pfLog = fopen("log.txt", "a");

	checkResult = (BYTE*)malloc(len*sizeof(BYTE));

	//Compute the result without bzets
	for (i = 0; i < len; i++) {
		checkResult[i] = do_data_op(opno, data1[i], data2[i]);
	}

	//compute the result with bzets
	bz1 = BZcreateFromByteArray(data1, len);
	//Check that data was compressed correctly
	BZconvertToByteArray(bz1, &tempCheck, &tempCheckLen);
	areSame = (memcmp(tempCheck, data1, len) == 0);
	if (!areSame) {
		fprintf(pfLog, "binopCheckBytes failed conversion check 1\n\nexpected data1:\n\n");
		fprintArray(pfLog, data1, len);
		fprintf(pfLog, "\ndata1 after decompression\n\n");
		fprintArray(pfLog, tempCheck, tempCheckLen);
		fprintf(pfLog, "\nbz1->nodes:\n\n");
		BZfprint(pfLog, bz1);
		free(tempCheck);
		free(checkResult);
		BZreleaseMemory(bz1);
		fclose(pfLog);
		return 0;
	}
	free(tempCheck);

	bz2 = BZcreateFromByteArray(data2, len);
	//check that data was compressed correctly
	BZconvertToByteArray(bz2, &tempCheck, &tempCheckLen);
	areSame = (memcmp(tempCheck, data2, len) == 0);
	if (!areSame) {
		fprintf(pfLog, "binopCheckBytes failed conversion check 1\n\nexpected data2:\n\n");
		fprintArray(pfLog, data2, len);
		fprintf(pfLog, "\ndata2 after decompression\n\n");
		fprintArray(pfLog, tempCheck, tempCheckLen);
		fprintf(pfLog, "\nbz2->nodes:\n\n");
		BZfprint(pfLog, bz2);
		free(tempCheck);
		free(checkResult);
		BZreleaseMemory(bz1);
		BZreleaseMemory(bz2);
		fclose(pfLog);
		return 0;
	}
	free(tempCheck);

	binopResult = binop(opno, bz1, bz2);
	BZconvertToByteArray(binopResult, &binopDecompressedResult, &binopDecompressedLength);

	areSame = 1; //assume true, until we see otherwise
	/*if (binopDecompressedLength > len) {
		//there may be extra zeros at the end.
		//This is ok, but we have to make sure they
		//are actually ALL zero.
		int i;
		for (i = len; i < binopDecompressedLength; i++) {
			if (binopDecompressedResult[i] != 0x00) {
				areSame = 0;
				break;
			}
		}
	}*/

	//compare results.
	areSame = (memcmp(checkResult, binopDecompressedResult, len) == 0);
	if (!areSame) {
		fprintf(pfLog, "binopCheckBytes failed\n\nexpected result:\n\n");
		fprintArray(pfLog, checkResult, len);
		fprintf(pfLog, "\n");
		fprintf(pfLog, "bz1->nodes:\n\n");
		BZfprint(pfLog, bz1);
		fprintf(pfLog, "bz2->nodes:\n\n");
		BZfprint(pfLog, bz2);
		fprintf(pfLog, "Binop->nodes:\n\n");
		BZfprint(pfLog, binopResult);
		fprintf(pfLog, "\n");
		fprintf(pfLog, "binopDecompressedResult:\n\n");
		fprintArray(pfLog, binopDecompressedResult, binopDecompressedLength);
	}

	free(checkResult);
	free(binopDecompressedResult);
	BZreleaseMemory(bz1);
	BZreleaseMemory(bz2);
	BZreleaseMemory(binopResult);
	fclose(pfLog);
	
	return areSame;
	
}

#endif //#ifdef DEBUG

//***********************************************************************************************
//Create byte array from bzet

int BZconvertToByteArray(const Bzet* bz, BYTE** bytes, int* len)
{
    int level = bz->nodes[0];
  
    // allocate nodes for pow8(lev)
    int numTrees = 0;
    int posArr=0;
    int posBzet = 1;

	*bytes = (BYTE*)malloc(pow8(level)*sizeof(BYTE));
    if(level != 0)
       recurseCreatearr(level,&posBzet,posArr,bz,*bytes);
    else
       *bytes[0] = bz->nodes[1];
    *len = pow8(level);
    return 1;
}

BYTE reverseByte(BYTE toBeRev)
{
    BYTE reversed = 0x00;
    int i;

    for(i=0;i<8;i++)
    {
        reversed |= (((toBeRev & (0x01<<(i))) && 0x01) << (7-i));
    }
    return reversed;
}

//-----------------------------------------------------------------

int recurseCreatearr(int lev,int* posBzet,int posArr,const Bzet* bz, BYTE* bytes)
{
    int j,k,stable;
    int branchBytes = pow8(lev-1); //amount of data byte at bottom of each branch
    BYTE dataNode = bz->nodes[*posBzet];
    BYTE treeNode = bz->nodes[*posBzet+1];
	if(lev == 0)
	{
		bytes[posArr] = reverseByte(bz->nodes[*posBzet]);
        posArr++;
        (*posBzet)++;
		return posArr;
	}
    *posBzet += 2;
    
            for(j=0;j<8;j++)//for bits in databyte above tree byte
            {
                   //if(level != 0)
                   // numTrees = count_bits(bz->nodes[bzetPos+1]);
                    if((dataNode & (0x01<<(7-j)) && !(treeNode & (0x01<<(7-j))))) //case all 1s
                    {
                         stable = posArr;
                        for(k=stable;k<branchBytes+stable;k++)
                        {
                            bytes[k] = 0xFF;
                            posArr++;
                        }
                    }
                    else if((treeNode & (0x01<<(7-j)))) //case trees
                    {
                        if(lev-1 != 0)
                        posArr = recurseCreatearr(lev-1,posBzet,posArr,bz,bytes);
                        else
                        {
                            bytes[posArr] = reverseByte(bz->nodes[*posBzet]);
                            posArr++;
                            (*posBzet)++;
                            
                        }
                    //   numTrees = count_bits(bz->nodes[bzetPos+1]);

                    }
                    else // case all 0s
                    {
                        stable = posArr;
                         for(k=stable;k<branchBytes+stable;k++)//not sure if won't update pos arr
                            {
                            bytes[k] = 0x00;
                            posArr++;
                            }
                    }

            }
    return posArr;

}


/************************************************************************/
//COUNT, FIRST, LAST, LIST_T functions with associated recursive helper function

int COUNT(const Bzet* bz)
{
        int numTrue,level,posBzet, posArr,done;

        level = bz->nodes[0];
        numTrue = 0;
        posBzet = 1;
        posArr = 0;
        done = 0;

        numTrue = recurseSearch('c',level,&posBzet,&posArr,bz,numTrue,&done,NULL);

        return numTrue;
}

int FIRST(const Bzet* bz)
{
        int firstBit,level,posBzet,posArr,done;

        level = bz->nodes[0];
        firstBit = 0;
        posBzet = 1;
        posArr = 0;
        done = 0;

        firstBit = recurseSearch('f',level,&posBzet,&posArr,bz,firstBit,&done,NULL);

        return firstBit;
}

int LAST(const Bzet* bz)
{
        int lastBit,level,posBzet,posArr,done;

        level = bz->nodes[0];
        lastBit = 0;
        posBzet = 1;
        posArr = 0;
        done = 0;

        lastBit = recurseSearch('l',level,&posBzet,&posArr,bz,lastBit,&done,NULL);

        return lastBit;
}
void LIST_T(const Bzet* bz,int** indicies,int* numIndicies)
{
        int count,hold,level,posBzet,posArr,done;

        count = COUNT(bz);
        *numIndicies = count;
        *indicies = (int*) malloc((count+1) * sizeof(int));

        level = bz->nodes[0];
        hold = 0;
        posBzet = 1;
        posArr = 0;
        done = 0;

        hold = recurseSearch('p',level,&posBzet,&posArr,bz,hold,&done,*indicies);
        return;
}

//used to recursively search the tree for the COUNT, FIRST,LAST, and LIST_T operations
// arguments for function parameter:
// 'c' for COUNT   'f' for FIRST
// 'L' for LAST    'p' for LIST_T
int recurseSearch(char function, int lev,int* posBzet,int* posArr,const Bzet* bz, int data,int* done,int* true_array)
{
    int j,k,stable; //iterators j,k and placeholder stable
        BYTE currByte;  // used to hold a data byte being examined
    int branchBytes = pow8(lev-1); //amount of data byte at bottom of each branch
    BYTE dataNode; 
    BYTE treeNode;
	if(lev == 0) //case passed a data byte as function will never reach level 0 elsewise
	{
		if(function == 'c')
		{
			return count_bits(bz->nodes[*posBzet]);
		}
		if(function == 'f')
		{
			return first_bit(bz->nodes[*posBzet]);
		}
		if(function == 'l')
		{
			return last_bit(bz->nodes[*posBzet]);
		}
		if(function == 'p')
		{
			  BYTE currByte = bz->nodes[*posBzet];
			  for(k=0; k<8; k++) //iterate through bits
              {
                    if(currByte & (0x01<<(7-k)))
                    {
                            true_array[data] = (*posArr)+k;
                            data++;
                    }
              }
			  return data;
		}
	}

	dataNode = bz->nodes[*posBzet]; 
	treeNode = bz->nodes[*posBzet+1];
    *posBzet += 2; //advance position of the bzet by 2 since looking at data and tree node currently
    
            for(j=0;j<8;j++)//for bits in databyte above tree byte
            {
                if(*done) //if using FIRST functionality and found the first bit already,done so break
                break;

                    if((dataNode & (0x01<<(7-j)) && !(treeNode & (0x01<<(7-j))))) //case all 1s
                    {
                         if(function == 'c') //if COUNT function
							{
							data += 8 * branchBytes;  //all 1's at the bottom level of this sub tree so add them up 
							}

							else if(function == 'f') //if FIRST function
							{
							*done = 1;  //found a bit so finished looking
							return *posArr; // return position of bit
							}

							else if(function == 'l') //if LAST function
							{
							*posArr += 8 * branchBytes; // move pos arr to the end of true bits for case last bit
							data = *posArr-1; // -1 because 8*branchBytes gives pos after last bit
							}

							else if(function == 'p')//if LIST_T function (print true bits)
							{
							stable = *posArr; //hold position
								for(k=stable;k<(branchBytes*8)+stable;k++) //iterate through number of true bits compressed by the current level
								{
										true_array[data] = (*posArr);
										(*posArr)++;
										data++;
								}
                                                        
							}
                    }
                    else if((treeNode & (0x01<<(7-j)))) //case trees
                    {
                        if(lev != 1) // if not level 1
                        {
                            if(function == 'c') // if COUNT function
                            {
                            data = recurseSearch('c',lev-1,posBzet,posArr,bz,data,done,NULL); // recurse down a level
                            }

                            else if(function == 'f') // if FIRST function
                            {
                            data = recurseSearch('f',lev-1,posBzet,posArr,bz,data,done,NULL); //recurse down a level
                                    if(*done)// if found a true bit in level just visited
                                    break;//done break out of the loop 
                            }

                            else if(function == 'l')//if LAST function
                            {
                                    data = recurseSearch('l',lev-1,posBzet,posArr,bz,data,done,NULL); // recurse down a level
                            }

                            else if(function == 'p') //if LIST_T function (print true bits)
                            {
                                        data = recurseSearch('p',lev-1,posBzet,posArr,bz,data,done,true_array); // recurse down a level
                            }
                        }
                        else //if level == 1 (looking at data bytes below now)
                        {
                            if(function == 'c')//if COUNT function
                            {
                                    data += count_bits(bz->nodes[*posBzet]); //add all true bytes from data byte to count
                                (*posBzet)++; // go to next position in bzet array
                            }

                            else if(function == 'f') //if FIRST function
                            {
                                    *done = 1; // there must be a true bit here since this byte is not compressed up 
                                    return *posArr + first_bit(bz->nodes[*posBzet]); // return the index of the first bit 
                            }

                            else if(function == 'l')//if LAST function
                            {
                                    data = *posArr + last_bit(bz->nodes[*posBzet]);  //Set last bit to the last true bit of the data byte
                                    *posArr += 8;                                                                    //move forward in byte array
                                    (*posBzet)++;                                                                    //move forward in bzet array
                            }

                            else if(function == 'p')// if LIST_T function (print true bits)
                            {
                                    currByte = bz->nodes[*posBzet];    //set current byte to be examined

                                    for(k=0; k<8; k++) //iterate through bits
                                    {
                                            if(currByte & (0x01<<(7-k)))
                                            {
                                                    true_array[data] = (*posArr)+k;
                                                    data++;
                                            }
                                    }

                                    *posArr += 8; // move forward in byte array
                                    (*posBzet)++; // move forward in bzet array
                            }
                            
                        }

                    }
                    else // case all 0s
                    {
                          *posArr += 8 * branchBytes; // move forward in byte array according to how many bits compressed
                    }

            }

    return data; //return value 

}


void BZset(Bzet* bz, int n) {

	Bzet* result;
	Bzet* nthBit;

	if (!bz)
		return;

	nthBit = BZcreate(n);
	result = binop(DOR, bz, nthBit);

	free(bz->nodes);
	bz->nodes = result->nodes;
	bz->numNodes = result->numNodes;
	bz->bufferSize = result->bufferSize;

	free(result); //doesn't free result->nodes
	BZreleaseMemory(nthBit);
}

void BZunset(Bzet* bz, int n) {

	Bzet* result;
	Bzet* nthBit;
	Bzet* nthBitComplement;

	if (!bz)
		return;

	nthBit = BZcreate(n);
	nthBitComplement = invert(nthBit);
	result = binop(DAND, bz, nthBitComplement);

	free(bz->nodes);
	bz->nodes = result->nodes;
	bz->numNodes = result->numNodes;
	bz->bufferSize = result->bufferSize;

	BZreleaseMemory(nthBit);
	BZreleaseMemory(nthBitComplement);
	free(result); //doesn't free result->nodes
}

void BZflip(Bzet* bz, int n) {

	Bzet* result;
	Bzet* nthBit;

	if (!bz)
		return;

	nthBit = BZcreate(n);
	result = binop(DXOR, bz, nthBit);

	free(bz->nodes);
	bz->nodes = result->nodes;
	bz->numNodes = result->numNodes;
	bz->bufferSize = result->bufferSize;	

	BZreleaseMemory(nthBit);
	free(result); //doesn't free result->nodes
}

int BZtest(const Bzet* bz, int n) {

	Bzet* nthBit;
	Bzet* result;
	int ret = 0;

	if (!bz)
		return ret;

	nthBit = BZcreate(n);
	result = binop(DAND, bz, nthBit);

	ret = ( ! BZisZero(result) );

	BZreleaseMemory(nthBit);
	BZreleaseMemory(result);
	return ret;
}
