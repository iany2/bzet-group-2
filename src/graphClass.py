import sys

#Graph Class
class graph:
    
    def __init__(self, graph = {}):
        self.graph = graph

    def list_graph(self) :
        return self.graph
        
    def add_node(self,node):
        self.graph[node] = []

    def add_edge(self,node_from,node_to,):
      if(node_from != node_to):
        if (node_from in self.graph) and (node_to in self.graph) :
                    self.graph[node_from].append(node_to)
                    self.graph[node_to].append(node_from)
        else :
            print("add_edge failed: node not in graph")
                     

    def list_neighbors(self,node):
        if node in self.graph.keys() :
            try :
                return self.graph[node]
            except :
                return "None"
        else :
            print("list_neighbors failed: node not in graph")

    def list_nodes(self):
        try :
            return list(self.graph.keys())
        except :
            print("error")

    def remove_edge(self,node_from,node_to):
        if (node_from in self.graph[node_to]) and (node_to in self.graph[node_from]) :
            self.graph[node_from].remove(node_to)
            self.graph[node_to].remove(node_from)
        else:
            print("error : no edge exists between these nodes")

    def remove_node(self,node):
        if node in self.graph.keys() :
            del self.graph[node]
        else :
             print("remove_node failed: node not in graph")

   
            
