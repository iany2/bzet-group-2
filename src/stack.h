#ifndef STACK_H
#define STACK_H

#include <stdlib.h>


////////////////////////////////////////////////////////////////////////////////////
//Stack of type BYTE 
typedef unsigned char BYTE;

/*  The node data in the stack
*	
*/
struct _StackNode {

	struct _StackNode* previous;
	BYTE data;

};

typedef struct _StackNode StackNode;
/*  Stack data structure holds types of BYTE
*   Implemented using a linked list methodology
*/
typedef struct {

	StackNode* top;
	int size;

} Stack;

/*  Initialize stack structure
*	@param s: Pointer of a stack to be initialized
*/
void StackInit(Stack *s);

/* Push a value onto the stack
*  @param s: Pointer to a stack wanting to be changed
*  @param value: BYTE data type to be pushed onto the given stack
*/
int StackPush(Stack* s, BYTE value);

/*  Pop a value from the top of the stack
*	@param s: Pointer to a stack wanting to be changed
*	@param destination: pointer to a BYTE data type to store the value of popped data
*/
int StackPop(Stack* s, BYTE* destination);

/*  Frees all memory allocated within the stack
*   @param s: Pointer to a stack to be freed
*/
void StackDestruct(Stack *s);

//BYTE StackAccess(const Stack *s, int n);


////////////////////////////////////////////////////////////////////////////////////////////////
//Stack of struct type to be used in WALK


/*  The node data in the stack
*	
*/

struct _dataStackNode {

	struct _dataStackNode* previous;
	int pos, newLev, subTrees;

};

typedef struct _dataStackNode dataStackNode;

/*  Stack data structure holds types of BYTE
*   Implemented using a linked list methodology
*/
typedef struct {

	dataStackNode* top;
	int size;

} dataStack;

/*  Initialize stack structure
*	@param s: Pointer of a stack to be initialized
*/
void dataStackInit(dataStack *s);

/* Push a value onto the stack
*  @param s: Pointer to a stack wanting to be changed
*  @param value: BYTE data type to be pushed onto the given stack
*/
int dataStackPush(dataStack* s, int p, int lev, int sub);

/*  Pop a value from the top of the stack
*	@param s: Pointer to a stack wanting to be changed
*	Returns pointer to data type to store the value of popped data\
*/
dataStackNode* dataStackPop(dataStack* s);

/*  Frees all memory allocated within the stack
*   @param s: Pointer to a stack to be freed
*/
void dataStackDestruct(dataStack *s);

dataStackNode* dataStackTop(const dataStack *s);


////////////////////////////////////////////////////////////////////////////////////////////////
//Stack of int array type, old data structure doesn't need to be used.


/*  The node data in the stack
*	
*/
//typedef struct {

//	struct StackArrNode* previous;
//	int* data;

//} StackArrNode;

/*  Stack data structure holds types of BYTE
*   Implemented using a linked list methodology
*/
//typedef struct {

//	StackArrNode* top;
//	int size;

//} StackArr;

/*  Initialize stack structure
*	@param s: Pointer of a stack to be initialized
*/
//void StackArrInit(StackArr *s);

/* Push a value onto the stack
*  @param s: Pointer to a stack wanting to be changed
*  @param value: BYTE data type to be pushed onto the given stack
*/
//int StackArrPush(StackArr* s, int* value);

/*  Pop a value from the top of the stack
*	@param s: Pointer to a stack wanting to be changed
*	@param destination: pointer to a BYTE data type to store the value of popped data
*/
//int StackArrPop(StackArr* s, int* destination);

/*  Frees all memory allocated within the stack
*   @param s: Pointer to a stack to be freed
*/
//void StackArrDestruct(StackArr *s);

//int* StackArrAccess(const StackArr *s, int n);


#endif
