#include "randomBytes.h"
#include "time.h"
#include <stdio.h>

void randomBytes(BYTE bytes[], int length, int sparseness)
{	
	int i;					// iterator
	int chance;				// chance that the bit will be filled
	int bitType;			
	
	if (!bytes)
		return;

	// 0 if we are currently producing a string of 0's, and 1 if a string of 1's
	bitType = rand() % 2;

	// fill each space in bytes
	for (i = 0; i < length; i++) {

		chance = rand() % 100;
		if(chance > sparseness) {
			bitType = bitType ^ 1;
			bytes[i] = 0x00 + rand() % (0xFF + 1);
		}
		else if (bitType == 0) {
			bytes[i] = 0x00;
		}
		else {
			bytes[i] = 0xFF;
		}
	}

}