/* 
 * File:   Victor.h
 * Author: victor
 *
 * Created on October 25, 2011, 11:30 PM
 */

#ifndef VICTOR_H
#define	VICTOR_H

void bznWork(bzNodes* bn, int p, int lev, char actn, char actd);

typedef struct walkNode { //return type of walk contains all data
                          //the python code returns
    char nodeType; // will be a 'D' or 'N'
    int lev;       //levl in the tree where node is
    int p;         // index in bitset for node or data
    int bitNum;    //returns the number of the bit
    int val;       //index value of first bit in subtree
                   //e.g.  2[0x00,0x01] would give 64

} walkNode;

walkNode* bznWalk(bzNodes* bn, int pos, int clev, int ret_n);

int fbit_n(bzNodes* bitset, int pos, int lev, int branch, int xval);
int fbit_d(bzNodes* bitset, int pos, int lev, int branch, int xval);
void count_n(bzNodes* bitset, int pos, int lev, int branch, int xval);
void count_d(bzNodes* bitset, int pos, int lev, int branch, int xval);
void dust_n(bzNodes* bitset, int pos, int lev, int branch, int val);
void last_d(bzNodes* bitset, int pos, int lev, int branch, int xval);
void last_n(bzNodes* bitset, int pos, int lev, int branch, int xval);
void not_d(bzNodes* bitset, int pos, int lev, int branch, int val);
void not_n(bzNodes* bitset, int pos, int lev, int branch, int val);
void get_node(bzNodes* bitset, int pos, int lev, int branch, int val);
void get_data(bzNodes* bitset, int pos, int lev, int branch, int val);



#endif	/* VICTOR_H */

