#include "bzet.h"
#include "file_func.h"
#include <string.h>
#include <stdio.h>
#include <string.h>
#include <direct.h>
#include <Windows.h>
#include <sys/stat.h>



void checkEXT( char *c )
{
	int size, i;

	size = strlen(c);
	for (i = size - 1; i != 0; i--) {
		if (c[i] == '\\' || i == 0) {		//no file extension
			strcat(c, ".bz");
			break;
		}

		if (c[i] == '.') {
			if (i + 3 > size) {
				c = (char*) realloc (c, (size + 3)*sizeof(char));
			}
			c[i+1] = 'b';
			c[i+2] = 'z';
			c[i+3] = '\0';
			break;
		}
	}
	return;
}

int cmdLine( int argc, char* argv[] )
{
	int inSize, vflag, oflag, eflag, cflag, i, opno, check;
	char* fileN;
	FileData* fdp, *fdp1, *result;
	Bzet* bz, *bz1, *bzResult;
	double elapsedTime;
	LARGE_INTEGER frequency, start, end;
	struct stat st;
	struct stat st1;

	vflag = 0;
	eflag = 0;
	cflag = 0;
	oflag = 0;

	fdp = (FileData*) malloc(sizeof(FileData));	
	fdp1 = (FileData*) malloc(sizeof(FileData));

	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&start);

	if ( argc <= 1 ) {
		perror("Usage: Invalid # of arguments\n");
		return 1;
	}

	inSize = strlen( argv[1] );

	if (inSize <= 1 || argv[1][0] != '-' || inSize > 3) {
		perror("Usage: Invalid flag set\n");
		return 1;
	}

	//Find which flags were set
	for ( i = 1; i < inSize; i++ ) {
		if ( argv[1][i] == 'v' )
			vflag = 1;
		else if ( argv[1][i] == 'c' )
			cflag = 1;
		else if ( argv[1][i] == 'e' )
			eflag = 1;
		else if ( argv[1][i] == 'o' )
			oflag = 1;
		else {
			perror("Usage: Invalid flag set, valid options include letters e, o, v, or c\n");
			return 1;
		}
	}

	if ( inSize == 2 && vflag == 1 ) {
		perror("Usage: Invalid flag set, -v is dependent on other flags\n");
		return 1;
	}
	//checking for multiple arguments
	if ( inSize == 3 ) {
		if ( argv[1][1] == argv[1][2] ) {
			perror("Usage: Invalid flag set, can not have multiple flags\n");
			return 1;
		}

		if ( eflag == 1 && oflag == 1 ) {
			perror("Usage: Invalid flag set, non-compatible flags\n");
			return 1;
		}

		if ( eflag == 1 && cflag == 1 ) {
			perror("Usage: Invalid flag set, non-compatible flags\n");
			return 1;
		}

		if ( cflag == 1 && oflag == 1 ) {
			perror("Usage: Invalid flag set, non-compatible flags\n");
			return 1;
		}
	}

	if ( cflag == 1 ) {
		if ( argc > 4 ) {
			perror("Usage: Too many arguments\n");
			return 1;
		}

		check = BZcompressFile(argv[2], fdp);
		if ( check == BZ_COMPRESSION_FAILED ) {
			perror("Compression from file to Bzet failed\n");
			return 1;
		}
		if ( check == FILE_IO_ERROR ) {
			perror("Unable to read file inputted\n");
			return 1;
		}


		//Bzet -c [nameOfFileToCompress]
		if (  argc == 3 ) {
			fileN = argv[2];
			checkEXT(fileN);
			check = BZwriteCompressedContentToFile( fileN, fdp);
			if ( check == FILE_IO_ERROR)	{
				perror("Failue to write the compressed Bzet to a file\n");
				return 1;
			}

			if ( vflag == 0) //BZ_SUCCESS
				return 0;
			else {
				QueryPerformanceCounter(&end);
				if (stat(fileN, &st) != 0 || stat(argv[2], &st1) != 0)
					perror("Could not properly find file size\n");
				else {
					//printf("Ratio of compressed to uncompressed file size is %d:%d bytes\n", st.st_size, st1.st_size);
				}
				elapsedTime = (end.QuadPart - start.QuadPart) * 1000.0 / frequency.QuadPart;
				printf("It took %f ms to compress the Bzet\n", elapsedTime);
			}
		}

		//Bzet -c [nameOfFileToCompress][target]
		if ( argc == 4 ) {
			fileN = argv[3];
			check = BZwriteCompressedContentToFile( fileN, fdp);
			if (check == FILE_IO_ERROR)	{
				perror("Failue to write the compressed Bzet to a file\n");
				return 1;
			}
			if ( vflag == 0)	//BZ_SUCCESS
				return 0;
			else {	//Bzet -cv [nameOfFileToCompress][target]
				QueryPerformanceCounter(&end);
				if (stat(fileN, &st) != 0 || stat(argv[2], &st1) != 0)
					perror("Could not properly find file size\n");
				else {
					//printf("Ratio of compressed to uncompressed file size is %d:%d bytes\n", st.st_size, st1.st_size);
				}
				elapsedTime = (end.QuadPart - start.QuadPart) * 1000.0 / frequency.QuadPart;
				printf("It took %f ms to compress the Bzet\n", elapsedTime);
			}
		}

	}

	//Bzet -e "nameOfFileToExtract" "targetDir"
	if (eflag == 1) {
		if ( argc != 4 ) {
			perror("Usage: Not the correct amount of arguments for flag -e\n");
			return 1;
		}
		
		check = BZreadCompressedFile(argv[2], fdp);
		if (check == BZ_CORRUPT_DATA)	{
			perror("Unable to construct uncompressed Bzet file\n");
			return 1;
		}
		if ( check == FILE_IO_ERROR ) {
			perror("Unable to read file inputted\n");
			return 1;
		}

		check = BZextractToFile(argv[3], fdp);
		if(check == BZ_EXTRACTION_FAILED) {
			perror("Unable to create file from Bzet file\n");
			return 1;
		}
		if ( check == FILE_IO_ERROR ) {
			perror("Unable to read file inputted\n");
			return 1;
		}

		if ( vflag == 0)	//BZ_SUCCESS
			return 0;
		else {	//Bzet -ev "nameOfFileToExtract" "targetDir"
			QueryPerformanceCounter(&end);
			if (stat(argv[2], &st) != 0 || stat(argv[3], &st1) != 0)
				perror("Could not properly find file size\n");
			else {
				//printf("Ratio of compressed to uncompressed file size is %d:%d bytes\n", st.st_size, st1.st_size);
			}
			elapsedTime = (end.QuadPart - start.QuadPart) * 1000.0 / frequency.QuadPart;
			printf("It took %f ms to extract the Bzet\n", elapsedTime);
		}

	}

	//Bzet �o [opcode] [compressedFile1] [compressedFile2] [outputfile]
	if (oflag == 1) {
		if ( argc != 6 ) {
			perror("Usage: Not the correct amount of arguments for flag -o\n");
			return 1;
		}

		check = BZreadCompressedFile(argv[3], fdp);
		if (check == BZ_CORRUPT_DATA) {
			perror("Unable to read compressed Bzet file\n");
			return 1;
		}
		if ( check == FILE_IO_ERROR ) {
			perror("Unable to read file inputted\n");
			return 1;
		}
		
		check = BZreadCompressedFile(argv[4], fdp1);
		if ( check == BZ_CORRUPT_DATA) {
			perror("Unable to read compressed Bzet file\n");
			return 1;
		}
		if ( check == FILE_IO_ERROR ) {
			perror("Unable to read file inputted\n");
			return 1;
		}

		bz = fdp->bz;
		bz1 = fdp1->bz;
		//DAND, DXOR, DOR, DNOTOR, DNOTXOR, DNOTAND

		if (!strcmp(argv[2],"AND"))
			opno = DAND;
		else if (!strcmp(argv[2],"XOR"))
			opno = DXOR;
		else if (!strcmp(argv[2],"OR"))
			opno = DOR;
		else if (!strcmp(argv[2],"NOR"))
			opno = DNOTOR;
		else if (!strcmp(argv[2],"XNOR"))
			opno = DNOTXOR;
		else if (!strcmp(argv[2],"NAND"))
			opno = DNOTAND;
		else {
			perror("Usage: Invalid operation inputted\n");
			return 1;
		}

		bzResult = binop( opno, bz, bz1);

		result = (FileData*) malloc(sizeof(FileData));
		if ( result == 0 ) {
			perror("Failure in allocating memory\n");
			return 1;
		}
		result->bz = bzResult;
		//choose the larger filesize.  the extracted byte array fromt the bzet will always
		//be at least as large as the size of the file it was read from, and since binop
		//aligns bzets before performing an operation, neither bzet will be smaller than the
		//size of the larger file.
		result->filesize = fdp->filesize > fdp1->filesize ? fdp->filesize : fdp1->filesize;

		check = BZwriteCompressedContentToFile(argv[5], result);
		if(check != BZ_SUCCESS) {
			perror("Failure to write result of operation to file\n");
			return 1;
		}

		if (vflag == 0)
			return 0;
		else {	//Bzet �ov [opcode] [compressedFile1] [compressedFile2] [outputfile]
			QueryPerformanceCounter(&end);
			elapsedTime = (end.QuadPart - start.QuadPart) * 1000.0 / frequency.QuadPart;
			printf("It took %f ms to compress the Bzet\n", elapsedTime);
		}
		free(result);
	}
	free(fdp);
	free(fdp1);
	return 0;
}