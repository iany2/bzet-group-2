#include "stdlib.h"
#include "linkedlist.h"

typedef unsigned char BYTE;

void ListInit(List *s)
{
	s->head = NULL;
	s->tail = NULL;
	s->iterator = NULL;
	s->size = 0;
}

int ListAppend(List* s, BYTE value)
{
	ListNode* newTail;
	newTail = NULL;
	s->size++;
	newTail = (ListNode*) malloc( sizeof(ListNode) );
	if ( newTail == NULL ) {
		return 0;
	}
	else {
		newTail->data = value;
		if (s->size == 1)
		{
			s->tail = newTail;
			s->head = s->tail;
			s->iterator = s->head;
			s->tail->previous = 0;
			s->head->previous = 0;
			return 1;
		}
		s->tail->previous = newTail;
		s->tail = newTail;
		return 1;
	}
}

void ListDestruct(List *s)
{
	ListNode* temp;
	int n;
	s->iterator = s->head;
	for (n = 0; n < ListLength(s); n++)
	{
		temp = s->iterator;
		setIterNext(s);
		free ( temp );
	}
	s->size = 0;
}

int setIterNext(List *s)
{
	s->iterator = s->iterator->previous;
	if (s->iterator == NULL)
		return 0;
	else
		return 1;
}

BYTE getValue(List* s)
{
	return s->iterator->data;
}

int ListLength(List* s)
{
	return s->size;
}

BYTE* convertArr(List* s)
{
	int n;
	int i;
	BYTE* arr = (BYTE*) malloc(ListLength(s)*sizeof(BYTE));
	n = ListLength(s);
	s->iterator = s->head;

	for(i = 0; i < n; i++)
	{
		arr[i] = getValue(s);
		setIterNext(s);
	}

	s->iterator = s->head;
	return arr;
}
