from ctypes import *
lib=cdll.LoadLibrary("bzets_octal.dll")

BYTE = c_char

class BZ(Structure):
    _fields_ = [("numNodes",c_int),("bufferSize",c_int),("nodes",POINTER(type(create_string_buffer(1))))]

PBZ=POINTER(BZ)
FILENAME_MAX=20
class FD:
    _fields_=[("bz",PBZ),("filesize",c_int), ("ofname",c_char_p)]

BZcreate=lib.BZcreate
BZcreate.restype=PBZ

BZcreateEmpty=lib.BZcreateEmpty
BZcreateEmpty.restype= PBZ

_range=lib._range
_range.restype=PBZ

BZcopy=lib.BZcopy
BZcopy.restype=PBZ

BZcreateFromByteArray=lib.BZcreateFromByteArray
BZcreateFromByteArray.restype=PBZ

BZconvertToByteArray=lib.BZconvertToByteArray
BZconvertToByteArray.restype=c_int

BZwriteCompressedContentToFile=lib.BZwriteCompressedContentToFile
BZwriteCompressedContentToFile.restype=c_int

BZreadCompressedFile=lib.BZreadCompressedFile
BZreadCompressedFile.restype = c_int

BZextractToFile=lib.BZextractToFile
BZextractToFile.restype=c_int

BZcompressFile=lib.BZcompressFile
BZcompressFile.restype=c_int

binop=lib.binop
binop.restype=PBZ

BZlev=lib.BZlev
BZlev.restype=c_int

BZsize=lib.BZsize
BZsize.restype=c_size_t

BZclean=lib.BZclean
BZclean.restype=None

BZprint=lib.BZprint
BZprint.restype=None

BZcount=lib.COUNT
BZcount.restype=c_int

BZfirst=lib.FIRST
BZfirst.restype=c_int

BZlast=lib.LAST
BZlast.restype=c_int

BZflip=lib.BZflip
BZflip.restype=None

BZset=lib.BZset
BZset.restype=None

BZunset=lib.BZunset
BZunset.restype=None

BZinvert=lib.invert
BZinvert.restype=PBZ

BZ_List_t=lib.LIST_T
BZ_List_t.restype=None

BZtest=lib.BZtest
BZtest.restype=c_int

BZrelease=lib.BZreleaseMemory
BZrelease.restype=None

freeByteArray=lib.freeByteArray
freeByteArray.restype=None

stride = lib.stride
stride.restype = PBZ

class BZet(object):

    def __init__(self):
        """Initializes instances of the class."""
        self.bz=BZ()
        self.fd=FD()
        self.pynode=''
        return
    

    def __del__(self):
        """Provides a way to deallocate memory.Takes no argument and deallocates memory allocated to BZet. Sets self.pynode to to an empty string. Returns None."""

        if(self.isNullPointer(self.bz.nodes)==False):
            BZrelease(byref(self.bz))
            self.bz.nodes=None
            self.pynode=''
            return

    def __getitem__(self,i):
        return self.Test(i)


    def __setitem__(self,i,val):

        if(val==True):

            self.Set(i)
            return

        elif(val==False):

            self.Unset(i)
            return
        
        else:

            print("Wrong argument passed. Argument should be of type Boolean")
            return

    
    def isNullPointer(self,arg):
        """Check if argument is a null pointer. Return True if so, else return False. Takes one argument which is a pointer to an object."""

        if(bool(arg)==0):
            return True

        else:
            return False


    def CreateEmptyBZet(self):
        "Creates an empty BZET. Takes no argument and returns none"""

        if(self.isNullPointer(self.bz.nodes)==True):
            
            bz=pointer(BZ())
            bz=BZcreateEmpty()
            self.bz=bz.contents
            self.pynode=cast(self.bz.nodes.contents,POINTER(c_char * self.bz.numNodes))
            return

        else:

            print("Memory already allocated for BZ. Call deallocate function first !!! Memory Leak can occur this way !!")

        return
     
    #Create a bzet with bits set starting with
    #the bit at index 'start', and extending for
    #'length' bits.
    def RANGE(self, start, length):
        if(self.isNullPointer(self.bz.nodes)==True):       
            bz=pointer(BZ())
            bz=_range(start, length)
            self.bz=bz.contents
            self.pynode=cast(self.bz.nodes.contents,POINTER(c_char * self.bz.numNodes))       

    def stride(self, start, strd, length):
        if(self.isNullPointer(self.bz.nodes)==True):       
            bz=pointer(BZ())
            bz=stride(start, strd, length)
            self.bz=bz.contents
            self.pynode=cast(self.bz.nodes.contents,POINTER(c_char * self.bz.numNodes))           
            
    def CreateBzet(self,n):
        """"Create a BZET with index n set. Requires 1 argument which should be a Python integer which shows which index is set.Returns None. Performs checks for type of argument and
            NULL pointer access and prints error message accordingly."""

        if(isinstance(n, int)):

            nt=c_int(n)

            if(self.isNullPointer(self.bz.nodes)==True):

                bz=pointer(BZ())
                bz=BZcreate(nt)
                self.bz=bz.contents
                self.pynode=cast(self.bz.nodes.contents,POINTER(c_char * self.bz.numNodes))
                return

            else:

                print("Memory already allocated for BZ. Call deallocate function first !!! Memory Leak can occur this way !!")
                return

        else:

            print("Inside CreateBzet. Argument is not of type int")
            return


    def Copy(self,other):
        """Copies other.bz to self.bz. Takes an argument(other of type BZet) which is the source for copying. Checks for NULL pointer access and memory leaks and prints error message
            accordingly."""

        if(self.isNullPointer(self.bz.nodes)==True and other.isNullPointer(other.bz.nodes)==False):
            bz=pointer(BZ())
            bz=BZcopy(byref(other.bz))
            self.bz=bz.contents
            self.pynode=cast(self.bz.nodes.contents,POINTER(c_char * self.bz.numNodes))

        elif(other.isNullPointer(other.bz.nodes)==False):
            print("other.bz.nodes is not allocated. Allocate other.bz.nodes first")

        else:
            print("Self.bz.nodes is allocated. Deallocate self.bz.nodes first")


    def CreateBZetFromArray(self,byte,n):
        """Create a BZet from a array of bits represented as a string. It takes two arguments-byte and n. byte is of type c_char or bytes and n is a Python integer.
           If argument is of correct data type, check if memory has not been allocated. If true, it passes it to BZcreateFromByteArray function in C. Otherwise it prints a memory message.
           If argument is not of required type , a type message is printed. Returns None."""

        if((isinstance(byte, bytes) or isinstance(byte,BYTE * n)) and isinstance(n, int)):

            if(self.isNullPointer(self.bz.nodes)==True):

                n=c_int(n)
                byte_c=(c_char * n.value)()

                for i in range(n.value):
                    byte_c[i]=c_char(byte[i])
                    
                bz=pointer(BZ())
                bz=BZcreateFromByteArray(byte_c,n)
                self.bz=bz.contents
                self.pynode=cast(self.bz.nodes.contents,POINTER(c_char * self.bz.numNodes))
                return

            else:

                print("Memory already allocated for BZ. Call deallocate function first !!! Memory Leak can occur this way !!")
                return

        elif(isinstance(byte, BYTE)):

            print("Inside CreateBZetFromArray. n is not of type int\n")
            return

        else:

            print("Inside CreateBZetFromArray. byte is not of type bytes\n")
            return


    def ConvertBzToByteArray(self):
        """Convert BZ to byte array equivalent.It takes two arguments-byte and n. byte is of type c_char and n is a Python integer.If argument is of correct data type,
           check if memory has been allocated. If yes, it passes it to BZconvertToByteArray function in C. Otherwise it prints a NULL Pointer message. If argument is not of type int, a type message is printed"""

        byte_c=pointer((create_string_buffer(1)))

        if(self.isNullPointer(self.bz.nodes)==False):
            
            l=c_int(0)
            b=BZconvertToByteArray(byref(self.bz),byref(byte_c),byref(l))

            if(l.value<=0):

                print("Error")
                return []

            else:
                byte=cast(byte_c,POINTER(c_char * l.value))
                byte=byte.contents.raw
                freeByteArray(byte_c)
                return byte

        else:

            print("self.bz.nodes is a NULL pointer. NULL Pointer access !!!")
            return


    def AND(self,bz1):
        """Performs AND on self.bz and other.bz .Calls BinaryOp with two arguments - a BZet object and 'and' string.
           Returns a bzet object or none depending on success or failure of BinaryOp"""
        
        bz_res=self.BinaryOp(bz1,'and')
        return bz_res


    def OR(self,bz1):
        """Performs OR on self.bz and other.bz .Calls BinaryOp with two arguments - a BZet object and 'or' string.
           Returns a bzet object or none depending on success or failure of BinaryOp"""
        
        bz_res=self.BinaryOp(bz1,'or')
        return bz_res


    def XOR(self,bz1):
        """Performs XOR on self.bz and other.bz . Calls BinaryOp with two arguments - a BZet object and 'xor' string.
           Returns a bzet object or none depending on success or failure of BinaryOp"""
        
        bz_res=self.BinaryOp(bz1,'xor')
        return bz_res


    def NOR(self,bz1):
        """Performs NOR on self.bz and other.bz . Calls BinaryOp with two arguments - a BZet object and 'nor' string.
           Returns a bzet object or none depending on success or failure of BinaryOp"""
        
        bz_res=self.BinaryOp(bz1,'nor')
        return bz_res


    def NAND(self,bz1):
        """Performs NAND on self.bz and other.bz . Calls BinaryOp with two arguments - a BZet object and 'nand' string.
           Returns a bzet object or none depending on success or failure of BinaryOp"""
        
        bz_res=self.BinaryOp(bz1,'nand')
        return bz_res


    def NOTXOR(self,bz1):
        """Performs NOTXOR on self.bz and other.bz . Calls BinaryOp with two arguments - a BZet object and 'notxor' string.
           Returns a bzet object or none depending on success or failure of BinaryOp"""
        
        bz_res=self.BinaryOp(bz1,'notxor')
        return bz_res


    def BinaryOp(self,bz1,op):
        """This function performs Binary Operations operation on two BZETS. These include AND,XOR,OR,NOR,NOTXOR and NAND. It takes two arguments which are of type BZ and string
           and performs above operations on the self.bz.nodes and bz1.bz.nodes. Type Checks and Null Pointer Checks are performed before calling c-function and error messages are
           printed accordingly. Returns BZet object if checks succeed else returns None"""

        lookup=dict([['AND',2],['XOR',3],['OR',4],['NOR',5],['NOTXOR',6],['NAND',7]])
        
        try:
            opcode=lookup[op.upper()]
        except KeyError:
            print("Key Not Found !! Check Argument 2 !!")
            return

        if(isinstance(bz1, BZet) and isinstance(op,str)):

            if(self.isNullPointer(self.bz.nodes)==False and bz1.isNullPointer(bz1.bz.nodes)==False):

                result=pointer(BZ())
                result=binop(opcode,byref(self.bz),byref(bz1.bz))
                bz3=BZet()
                bz3.bz=result.contents
                bz3.pynode=cast(bz3.bz.nodes.contents,POINTER(c_char * bz3.bz.numNodes))
                return bz3

            else:
                print("One of the nodes variable is a NULL Pointer !!!")
                return

        else:
            print("Argument 2 is not of type BZet\n")
            return


    def Invert(self):
        """Perform a NOT operation on the BZET. Takes no argument and performs NULL pointer check. Returns None if NULL Pointer is accessed else returns BZet object."""
        
        if(self.isNullPointer(self.bz.nodes)==True):

            print("self.bz.nodes not allocated !! NULL Pointer Access !!!")
            return

        else:

            bz=pointer(BZ())
            bz=BZinvert(byref(self.bz))
            bz3=BZet()
            bz3.bz=bz.contents
            bz3.pynode=cast(bz3.bz.nodes.contents,POINTER(c_char*bz3.bz.numNodes))
            return bz3
        

    def CountSetBits(self):
        """This function takes no arguments, checks for NULL Pointer and returns None or the number of set bits in self.bz"""
        
        if(self.isNullPointer(self.bz.nodes)==True):

            print("self.bz.nodes not allocated !! NULL Pointer Access !!!")
            return

        else:

            num=BZcount(byref(self.bz))
            return num


    def ReturnLevel(self):
        """This function takes no arguments, checks for NULL Pointer and returns None or the depth of the octal tree representation."""

        if(self.isNullPointer(self.bz.nodes)):

            print("self.bz.nodes not allocated !! NULL Pointer Access !!!")
            return

        else:

            level=BZlev(byref(self.bz))
            return level


    def Clean(self):
        """This function deallocates memory allocated to self and creates an empty BZet instead. Takes no arguments and returns None."""

        BZclean(byref(self.bz))
        self.pynode=cast(self.bz.nodes.contents,POINTER(c_char * self.bz.numNodes))
        return


    def Print(self):
        """This function prints the nodes of the BZET. Takes no arguments and returns None."""
        
        if(self.isNullPointer(self.bz.nodes)):

            print("self.bz.nodes not allocated !! NULL Pointer Access !!!")
            return

        else:

            BZprint(byref(self.bz))
            return


    def Size(self):
        """This function returns the size of the BZET(the bz structure of class BZET)"""

        size=BZsize(byref(self.bz))
        if(size==-1):

            print("bz not allocated");

        return size
    

    def First(self):
        """This function returns the index of the first True Bit in BZET. Takes no arguments and checks for NULL Pointer and returns None or index accordingly"""
        
        if(self.isNullPointer(self.bz.nodes)):

            print("self.bz.nodes not allocated !! NULL Pointer Access !!!")
            return

        else:

            index=BZfirst(byref(self.bz))
            return index


    def Last(self):
        """This function returns the index of the last True Bit in BZET. Takes no arguments and checks for NULL Pointer and returns None or index accordingly"""

        if(self.isNullPointer(self.bz.nodes)):

            print("self.bz.nodes not allocated !! NULL Pointer Access !!!")
            return

        else:

            index=BZlast(byref(self.bz))
            return index
    

    def Set(self,n):
        """Takes an integer argument n , and sets the nth index bit to 1. Checks for NULL Pointer and returns None."""

        if(self.isNullPointer(self.bz.nodes)):

            print("self.bz.nodes not allocated !! NULL Pointer Access !!!")
            return

        elif(not(isinstance(n,int))):

            print("n is not of type int")
            return
        
        else:

            index=c_int(n)
            BZset(byref(self.bz),n)
            self.pynode=cast(self.bz.nodes.contents, POINTER(c_char * self.bz.numNodes))
            return    


    def Unset(self,n):
        """Takes an integer argument n , and sets the nth index bit to 0. Checks for NULL Pointer and returns None."""

        if(self.isNullPointer(self.bz.nodes)):

            print("self.bz.nodes not allocated !! NULL Pointer Access !!!")
            return

        elif(not(isinstance(n,int))):

            print("n is not of type int")
            return

        else:

            index=c_int(n)
            BZunset(byref(self.bz),n)
            self.pynode=cast(self.bz.nodes.contents, POINTER(c_char * self.bz.numNodes))
            return
            

    def Flip(self,n):
        """Takes an integer argument n , and flips the nth index bit to its opposite. Checks for NULL Pointer and returns None."""

        if(self.isNullPointer(self.bz.nodes)):

            print("self.bz.nodes not allocated !! NULL Pointer Access !!!")
            return

        elif(not(isinstance(n,int))):

            print("n is not of type int")

        else:

            index=c_int(n)
            BZflip(byref(self.bz),n)
            self.pynode=cast(self.bz.nodes.contents, POINTER(c_char * self.bz.numNodes))


    def List_t(self):
        """prints or returns index of every set bit. Takes no arguments and returns None if prnt=1 and returns the array of indices if prnt=0."""

        if(self.isNullPointer(self.bz.nodes)):

            print("self.bz.nodes not allocated !! NULL Pointer Access !!!")
            return
        
        else:
            
            indices=pointer(c_int())
            l=pointer(c_int())
            BZ_List_t(byref(self.bz),byref(indices),l)

            if(l.contents.value<=0):

                print("BZet is empty")
                return []

            else:
                indices=cast(indices,POINTER(c_int * l.contents.value))
                p_list=[]
                for i in range(len(indices.contents)):
                    p_list.append(indices.contents[i])
                freeByteArray(indices)
                indices=None
                del l
                return p_list


    def Test(self,n):
        """Takes an integer argument n , and tests whether the nth index is 1(returns 1 if true, returns 0 otherwise)."""

        if(self.isNullPointer(self.bz.nodes)):

            print("self.bz.nodes not allocated !! NULL Pointer Access !!!")
            return 0

        elif(not(isinstance(n,int))):

            print("n is not of type int")
            return 0

        else:

            index=c_int(n)
            result=BZtest(byref(self.bz),n)
            return result
        


