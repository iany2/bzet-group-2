#ifndef LIST_H
#define LIST_H

#include <stdlib.h>

typedef unsigned char BYTE;

/*  The node data in the list
*	
*/
struct _ListNode{

	struct _ListNode* previous;
	BYTE data;

};

typedef struct _ListNode ListNode;
/*  List data structure holds types of BYTE
*/
typedef struct {

	ListNode* head;
	ListNode* tail;
	ListNode* iterator;
	int size;

} List;

/*  Initialize list structure
*	@param s: Pointer of a list to be initialized
*/
void ListInit(List *s);

/* Append a value onto the end of the list
*  @param s: Pointer to a list wanting to be changed
*  @param value: BYTE data type to be pushed onto the given list
*/
int ListAppend(List* s, BYTE value);

/*  Frees all memory allocated within the list
*   @param s: Pointer to a list to be freed
*/
void ListDestruct(List *s);

/* Sets the built in iterator to the next LinkNode
*	@param s: Pointer to the list
*/
int setIterNext(List *s);

BYTE getValue(List* s);

BYTE* convertArr(List* s);

int ListLength(List* s);

#endif