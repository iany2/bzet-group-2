// file_func.c
// has the functions to deal with file io

// include header file
#include "file_func.h"

int BZcompressFile(const char fname[], FileData* result)
{
// If fname is a complete file path, and result is a pointer to a FileData pointer, result should be set so that it points to a FileData containing the compressed contents of the file pointed to by fname.  
	// If there is a problem finding or reading the file, return FILE_IO_ERROR
	// If there is a problem with compression, return BZ_COMPRESSION_FAILED
	// Else, return BZ_SUCCESS

	// Open file in binary.
	// Read in binary data to BYTE array.
	// Calculate file size.
	// Convert to BYTE array to Bzet.
	// Close file
	// Return correct values.

	// Declare variables
	FILE *fp;					// file pointer
	Bzet* createResult;			// Bzet returned from BZcreate
	unsigned char* buffer;		// buffer to store data from the file
	long lSize;
	int frresult;
	
	// Open the file
	fp = fopen(fname, "rb");
	// If couldn't open the file
	if ( fp == NULL)
	{
		return FILE_IO_ERROR;
	}

	// obtain file size:
	fseek (fp , 0 , SEEK_END);
	lSize = ftell (fp);
	rewind (fp);

	// allocate memory to contain the whole file:
	buffer = (unsigned char*) malloc (sizeof(unsigned char)*lSize);
	if (buffer == NULL) 
	{
		fclose(fp);
		return FILE_IO_ERROR;
	}

	// copy the file into the buffer:
	frresult = fread (buffer,1,lSize,fp);
	if (frresult != lSize) 
	{
		if (buffer)
			free(buffer);
		fclose(fp);
		return FILE_IO_ERROR;
	}

	// set i to the original file size
	result->filesize = lSize;
	
	// Compress the BYTE array to a Bzet
	createResult = BZcreateFromByteArray(buffer, lSize);
	
	//deallocate the buffer
	free(buffer);

	// close the file
	fclose(fp);
	
	// if compression failed
	if(createResult == NULL)
	{
		return BZ_COMPRESSION_FAILED;
	}
	else
	{
		result->bz = createResult;
		return BZ_SUCCESS;
	}
}

int BZextractToFile(const char fname[], const FileData* bz)
{
// Uncompress the bzet and write the represented bitstring to the file whose full path is fname.  
	// If there is a problem opening or writing to the file, return FILE_IO_ERROR.  
	// If there is a problem extracting the bzet, return BZ_EXTRACTION_FAILED.  
	// Else return BZ_SUCCESS.

	// Open the file
	// Uncompress bzet
	// Chop bzet size if necessary 
	// Write the BYTE array to the file
	// Close file
	// Return
	
	// declare variables
	FILE *fp;						// file pointer
	BYTE* extractResult;			// resulting BYTE
	int len;						// length returned by BZconvert
	int returnResult;				// return from BZconvert
	
	// Open the file for writing
	fp = fopen(fname, "wb");
	// If couldn't open the file
	if ( fp == NULL)
	{
		return FILE_IO_ERROR;
	}
	
	// uncompress/extract data
	extractResult = NULL;
	returnResult = BZconvertToByteArray(bz->bz, &extractResult, &len);

	// write to file
	if( bz->filesize > 0 )
	{
		// need to chop
		fwrite (extractResult, sizeof(extractResult[0]) , bz->filesize , fp );
	}
	else
	{
		// no need to chop just write
		fwrite (extractResult , 1 , sizeof(*extractResult) , fp );
	}
	
	//deallocate the buffer
	free(extractResult);

	// close the file
	fclose(fp);
	
	// return
	return returnResult;
}

int BZwriteCompressedContentToFile(const char fname[], const FileData* bz)
{
// Write the bzet’s internal representation to a file with the name fname.

	// open file
	// write file size
	// compress byte array
	// write the bzet to file
	// close file
	// return

	// Declare variables
	FILE* fp;
	
	// Open the file for writing
	fp = fopen(fname, "wb");
	// If couldn't open the file
	if ( fp == NULL)
	{
		return FILE_IO_ERROR;
	}
	
	// write file size
	fwrite ( &(bz->filesize) , sizeof(int) , 1 , fp );

	// write BYTE array
	fwrite (bz->bz->nodes , sizeof(BYTE) , bz->bz->numNodes , fp );

	// close the file
	fclose(fp);
	
	// return
	return BZ_SUCCESS;
}

int BZreadCompressedFile(const char  fname[], FileData* result)
{
// Construct a bzet from a compressed file.  
	// If this is not possible, return BZ_CORRUPT_DATA.
	// (eg. one created with BZwriteCompressedContentToFile).

	// Open File
	// Read in original size from file
	// read in bzet from file
	// store variables from file correctly
	// close file
	// return
	
	// Declare variables
	FILE* fp;						// file pointer
	int frresult;
	BYTE* bytebuffer;				// buffer to store data from the file
	int bzsize;
	int osize;						// original file size
	int fileSize;
	
	// Open the file for writing
	fp = fopen(fname, "rb");
	// If couldn't open the file
	if ( fp == NULL)
	{
		return FILE_IO_ERROR;
	}

	//get the size of the compressed file
	fseek (fp , 0 , SEEK_END);
	fileSize = ftell (fp);
	rewind (fp);
	
	// Read in original size from file
	fread(&osize, sizeof(osize), 1, fp);

	bzsize = fileSize - ftell(fp);

	// If file was empty
	if ( bzsize < 0)
	{
		fclose(fp);
		return FILE_IO_ERROR;
	}

	bytebuffer = (BYTE*) malloc (sizeof(BYTE)*bzsize);
	frresult = fread (bytebuffer, sizeof(BYTE), bzsize, fp);
	if (frresult != bzsize)
	{
		free(bytebuffer);
		fclose(fp);
		return FILE_IO_ERROR;
	}

	// store variables from file correctly
	result->filesize = osize;

	// convert unsigned char to unsigned char
	// bytebuffer to bz->bz->nodes 
	result->bz = BZallocateInit(bytebuffer, bzsize);

	// close file
	fclose(fp);

	// return
	return BZ_SUCCESS; 
}
