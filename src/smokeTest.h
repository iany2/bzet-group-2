#ifndef SMOKETEST_H
#define SMOKETEST_H

#include "bzet.h"

//---------------------- Scaffolding ---------------------------

BYTE charToHex(char c);

/*  Convert a string of the form [0xDD][0xDD]...
*	where D is a hex digit to an array of BYTES
*/
BYTE* strToByteArray(const char str[], int slen, int* blen);


int _BzetEquals(Bzet* bz, BYTE* nodes, int numNodes);

/*  Returns 1 if the contents of the array pointed to
*	by bz->nodes is exactly the same as the contents of
*	nodes, and numNodes == bz->numNodes.  Else returns 0.
*/
int BzetEquals(Bzet* bz, const char nodes[]);

Bzet* BZcreateFromStr(const char str[]);

static int _binopCheckFile(int opno, const char filename1[], const char filename2[]);

void printArray(const BYTE arr[], int len);

void fprintArray(FILE* fp, const BYTE arr[], int len);

//---------------------- Test Harnesses ----------------------

void bzSmoke();

void utilHarn();

void binopHarn();

void conversionHarn();

void bitMutatorHarn();

void rangeHarn();
static void _testRange(Bzet* bz, int start, int len);

void strideHarn();
static void _testStride(Bzet* bz, BIGNUM start, BIGNUM stride, BIGNUM len);

void performanceTest();

#endif