import sys
from BZet import *

class bzGraphGroup:

    def __init__(self, graph = {}):
        self.graph = graph

    def list_graph(self):
        return self.graph

    def add_node(self,node):
        bz = BZet()
        bz.CreateEmptyBZet()
        self.graph[node] = bz

    def add_edge(self,node_from,node_to):
        if (node_from in self.graph.keys()) and (node_to in self.graph.keys()):
             self.graph[node_from][node_to] = True
             self.graph[node_to][node_from] = True
        else :
            print("add_edge failed: node not in graph")

    def list_neighbors(self,node):
        return list(self.graph[node].List_t())

    def list_nodes(self):
        return list(self.graph.keys())
                    

    def remove_edge(self,node_from,node_to):
        if node_from in self.graph.keys():
            self.graph[node_from][node_to] = False
            self.graph[node_to][node_from] = False
        else :
            print("remove_edge failed: node not in graph")
            

