#!/usr/bin/python3
# vi: ts=4 sw=4 et ai sm

#             BZET Pieces of Eight

# Copyright (C) 2011 Robert Uzgalis

version = 'Version 0.9 2011 May 11'

# This set of routines implements a compressed bit-array
# called a bitset. The compression is done using an 8-way tree
# structure that resembles a base 8 or octal notation for
# numbers.  Except in this case the numbers are subtrees rather
# than numbers.  Another way of looking at this is that the
# bitarray is compressed as an oct-tree rather like image
# compression quad-trees. The tree is represented as a top-down,
# depth-first tree traversal to create a linear string of bytes.
# Note that compression of bitsets have generally used
# some form of run-length encoding.  This approach differ by
# having a logarithmic encoding, which is advantages for 
# very large bitsets. 

# Each bitset starts with a single byte which describes the
# depth of tree in this instance. Eight raised to this level
# plus 1 should be just greater than the size of the bitset.
# As implemented bitsets are always a power of 8 big.

# If the depth, or Level, is zero it is followed by 8 bits
# of data; if it is greater than zero it is followed by a
# a two byte structure representing a tree-node which is head
# of an 8-way tree of bits. All tree-nodes including this
# one are represented here as [dd-cc]. Where cc are two
# nibbles(4-bits) of control information. The dd are a
# corresponding two nibbles of data information. Each nibble
# is written as a hex digit. The two control nibbles
# represent information about each of the 8 branches off the
# node. A one bit in the control nibble means that within the
# bitset there are mixed one and zero bits and soon another
# series of nodes and data will describe them, whereas a zero
# bit in the control data means that this tree branch consists
# of a tree whose leaves are either all ones and all zeros.
# Thus a 0 in the control byte and a 1 in the corresponding
# place in the data byte of a control node indicates that
# there are 8**level bits which are True.  If it is a four level
# tree and the first bit at the top level node is 0 and the
# first data bit is one then the first 4096 bits of the bitset
# are True.

# Another way of looking at this is each node in the tree is
# an eight way branch described by 2 bits a control bit and
# a data bit.
#
# DC where C is the control bit and D the corresponding data bit
# 00 says everything in this branch below this level is False
# 01 says there a mixed bits below here
#    and there must be more description to complete the bitset
# 10 says everything in this branch below this level is True
# 11 is unused. And is always set to 01, to make the bitset canonical.

# Thus 1L[00-00] is a bitset with all False bits.
#      4L[ff-00] is a 32768 large bitset with all True bits.

# And 4L[80-00] is a 32768 large bitset: the 1st 4096 bits True
#                and the remainder False

# Finally 4L[00-80][00-80][00-80][00-80]D(80) is the same large
#                bitset with bit zero True and everything else
#                False.
# The normalized form reduces this to: 1L[80-00]D(80)
# by stripping off leading [00-80] nodes and reducing the level
# accordingly. A node of the form [00-80] represents trailing zeros.

# The number of one bits in the control portion of a tree-node
# tells how many subtrees immediately follow that node. A Level
# 1 node is followed by only data bytes, because there is no
# more tree structure to represent. Those data bytes are the 8
# bits representing the tree leaf values at a corresponding
# position. So one could think of each level 0 tree node as
# being represented a normal node with [dd-00], but since
# a level 0 node will always have a zero control byte, it is
# just represented as D(dd).

# Here is a larger example call the bitset in it bst1.

# Memory in hex: [ 04, 00, 05, 00, 80, 00, 04, 01, 10, 34,
#                              00, 40, 00, 70, 00, 10, 1f,
#                                              00, 0c, 2e, e2,
#                                              00, 01, 3b ]

# Pretty print with the Tree-Nodes as [dd-cc] and D-values as D(dd)
# #L L4     L3     L2     L1    L0
# 4L[00-05][00-80][00-04][01-10]D(34)      8+2+1   True bits 11 total
#          [00-40][00-70][00-01]D(1f)      1+4     True bits  5 total
#                        [00-0c]D(2e)D(e2) 1+3+3+1 True bits  8 total
#                        [00-01]D(3b)      2+3     True bits  5 total
#                                          ---------------------
#                                                            29 total

# List of indicies of True bits in bst1
#
#      0:  20,826; 20,827; 20,829; 20,856; 20,857; 
#      5:  20,858; 20,859; 20,860; 20,861; 20,862; 
#     10:  20,863; 29,275; 29,276; 29,277; 29,278; 
#     15:  29,279; 29,346; 29,348; 29,349; 29,350; 
#     20:  29,352; 29,353; 29,354; 29,358; 29,434; 
#     25:  29,435; 29,436; 29,438; 29,439.
# 29 True bits found

# The maximum number of bits in a level 4 tree is 8**5 or 32,768.
# It really has 5 different levels 0..4.

# Note this representation is canonical because if the
# the rules are followed strictly there is only a single
# representation for any bit pattern. (e.g. There can be
# no 0x00 or 0xff data bytes...  these must be compressed
# into the level above.  The 11 non-meaningful state must
# always be set to 01 per the definition.)

# The advantage of this oct-tree compression is that one can
# perform bit manipulation on the oct-tree representation
# directly. One never has to unpack it.  Very large bitsets
# can be manipulated directly.  Manipulation times for most
# operation are linear, on the order of the sum of the length
# of the two bitset representations. Thus operation times are
# often substantially lower than the length of the time to
# operate on the bit arrays themselves.  Compression works
# uniformly compressing a series of 1 bits as well as 0 bits.
# Thus a bit-array and it's complement occupy the same space.
# The NOT operation is very fast and can be done in-place.

# Note that while this implementation is based on a oct-tree,
# there is no reason that the system would not work as well
# for any base greater than 1.  Eight was just picked because
# Python offers packed byte arrays of elements of 8-bits that
# make implementation easier. Byte addressing (of 8 bit groups)
# is common in modern computer architectures so this sweet spot
# is fairly common in modern computers.

# Note also that the representation is not all that compact.
# However by placing a Tree-Node compression tree (say a Huffman
# encoding) at the front of the bitset, common patterns would
# be reduced to 3 or 4 bits and decoding and operating on the
# structure would not be hindered.  It would require another
# two passes after changes to reencode it to try and keep the
# representation at a minimum.

# Level zero bitsets could be bigger than 8 bits.
# They could even be implemented as run-length encoded arrays.
# or possibily implemented simply with Lev, n-bytes, bytes...
# and any bit array under say 8K bits might be stored this way.
# This implementation stays strictly with the oct-tree
# representation.

#------------------------------------------------------------

# What's new in v0.6
# Bitsets are stored in immutable bytes structures.
# NOT and bs_list use a temporary bytearray to do their work.
# Internal operations still work with integers for speed.
# The most important change is that levels are no longer
# a concern of the user.  Additional levels are added to
# adjust for bitsets at differing levels and useless levels
# are deleted from bitsets after Binary Boolean computations.
# Not leaves bitsets unnormalized to preserve ~~A == A

# What's new in v0.7
# Bitsets are encapsulated in a module called buzbits with the class
# Bits.
# Operators for and, or, xor, not, as well as the assignment operators
# for and, or, and xor are not available.
# Bits(int) will give the Bitset value with a bit at that index True.
# Bits(None) yields the empty Bitset.

# What's new in v0.8
# Binary Boolean Operations are now done with a generalized function
# controlled by one table which defines the operation for all 16
# possible Boolean Operations.  This makes any non-trivial Boolean 
# operation about the same cost as another.  Currently only AND, OR,
# and XOR are defined this way.  They are the only table entries that
# have been checked at all; the rest may be nonsense.

# What's new in v0.9
# A major structural change: The node format has been reversed
# with data coming before tree bits. The description given above
# has been modified to reflect this version.
#
# A fast method, RANGE, of setting a range of one bits has been added.
# The COUNT method has been added which returns the number of
# True bits in a bit-set.  This has been integrated into the
# array spec for Bits.  Bits([ idx, idx, (s_idx, e_idx, idx ... ])
# So one can specify a bitset with a series of index values and ranges
# to set to one.

# Subscripting a bitset gives a TRUE/FALSE for that bit.  bs[i]
# Substrings are undefined as are negative subscripts.

# The meaning of len(bzet) is now the 8 ** (lev+1), which is
# The number of bits both 0 and 1 at any level.
# Two new related functions:
# size(bzet) returns the length of the underlying bytes string.
# bzet.LAST() returns the index of the last on bit.

# The == and != operators align the bitset arguments by default. Use:
# bset.__eq__(bsety,align=False) or
# bset.__ne__(bsety,align=False) -- if you don't want this behavior.

# Imperative commands have been added which just operate on the
# bitset in place. All can be used as functions as well.
# Including:  bs.CLEAR() (sets the bitset to empty), bs.DUST()
# (sets the data bit off for matching tree bits), bs.INVERT()
# (nots an entire bitset in place).
# Along with bs.SET(n), bs.UNSET(n), bs.FLIP(n), which modify a bit
# in the bitset. The only real ones done in place are INVERT and DUST.
# The others are just syntactic sugar.

# Assignment to a bitset is supported: bs[ix] = True

# Minor bug fixes.  RANGE didn't work for level 0 bitsets. Neither did
# LIST_T.  Added many tests to the testset.

bc4 = ( 0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4 )    

def bit_count( n ):
    # print( "bit count of ", n, "is", bc4[n & 0xf] + bc4[(n>>4)&0xf] )
    return bc4[n & 0xf] + bc4[(n>>4)&0xf]

def first_bit( bitx ):    
    bitx &= 0xff
    if bitx == 0: return None
    for i in range(8):
        if bitx & 0x80: return i
        bitx <<= 1
    return -1

def last_bit( bitx ):    
    bitx &= 0xff
    if bitx == 0: return None
    for i in range(8):
        if bitx & 0x01: return 7-i
        bitx >>= 1
    return -1

def mkbits( ibit, ebit ):
    abyte = 0
    for ix in range(ibit,ebit+1):
        abyte |= 0x80 >> ix
    return abyte

def byte(x):
    # Returns the integer x as a single
    # bytes value
    return bytes((x,))
    

''' 
# Alternative definition of first_bit
# Hard to know which one would be faster
firstbit4 = ( -1, 3, 2, 2, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0 )
def first_bit ( bits )
    t1 = firstbit4[ (bits>>4) & 0x0f ]
    if t1 != -1: return t1
    t2 = firstbit4[ bits & 0x0f ]
    if t2 != -1: return t2+4
    return None
'''

#            0 3 6  9  12   15    18     21      24       27  bits
#            0 1 2  3   4    5     6      7       8        9 (134 million)
powersof8 = (1,8,64,512,4096,32768,262144,2097152,16777216,134217728)
def pow8(n):
    if n < 0: return None
    if n < 10: return powersof8[n]
    return 8**n

def hex2( n ):
    return( '{0:02x}'.format(n&0xff) )

class Bits:

    #---------------------------------------------
    #
    #  Debugging code.  Convert a bitset or 
    #  various values to a formatted hex string
    #
    #---------------------------------------------
    
    
    # The MT and tbits values are.
    # filled in after the class definition
    MT = ''          # a Bits value of the Empty Bitset
    tbits = type(3)  # the type Bits
    # This MTbytes is the internal Bits.v value
    MTbytes = bytes([0x01,0x00,0x00])
    # Type constants
    tbytes = type(bytes([]))
    tint   = type(3)
    tbarray= type(bytearray([]))
    tbool  = type(True)
    ttuple = type((0,))
   #tbits  = type(Bits(None)) is set just below class definition
    

    @staticmethod
    def hexx( nl ):
        if type(nl) == type(Bits.MT):
            nl = nl.v
        if not len(nl): return '[ ]'
        rv = "[ "
        for abyte in nl:
            rv += hex2(abyte) + ', '
        return rv[:-2] + ' ]'

    @staticmethod
    def hexn( bitx, p ):
        return '[' + hex2(bitx[p]) + '-' + hex2(bitx[p+1]) + ']'

    @staticmethod
    def hexd( bitx, p ):
        return 'D(' + hex2(bitx[p]) + ')'

    @staticmethod
    def RAW( bs ):
        return Bits( bs )

    # Bits Boolean Operations defined by generalized table

    # The op argument is a 8 tuple that in part tells what
    # action to take for each of 6 cases:
    # Case input       Case input
    # 0     D D         4    T 1     
    # 1     0 T         5    T T
    # 2     T 0         6    X X Bad inputs
    # 3     1 T  


    # The case type breaks each trees c & d bits
    # from the left side of the operator and the c & d
    # bits from the right side (total of 4 bits
    # and 16 possibilities) into 6 fundamental cases
    # Trees never have associated data bits thus in
    # the comments an x or error is placed before
    # a 1 data bit that corresponds to a tree bit.
    # These are type 6 data errors and should never
    # occur. This case table maps each of the 16
    # possibilities into the 6 operational cases 0-5
    # and case 6 representing a data error.

    # Inputs  DD      0T     T0     1T     T1    TT
    #        Case0   Case1  Case2  Case3  Case4 Case5
    #
    #                       c1,c2, d1,d2
    case_type = [0,  #  0     0,0,  0, 0  DD
                 0,  #  1     0,0,  0, 1  DD
                 0,  #  2     0,0,  1, 0  DD
                 0,  #  3     0,0,  1, 1  DD
                 1,  #  4     0,1,  0, 0  0T
                 6,  #  5     0,1,  0,x1  0T
                 3,  #  6     0,1,  1, 0  1T
                 6,  #  7     0,1,  1,x1  1T
                 2,  #  8     1,0,  0, 0  T0
                 4,  #  9     1,0,  0, 1  T1
                 6,  # 10     1,0, x1, 0  T0
                 6,  # 11     1,0, x1, 1  T1
                 5,  # 12     1,1,  0, 0  TT
                 6,  # 13     1,1,  0,x1  TT
                 6,  # 14     1,1, x1, 0  TT
                 6,  # 15     1,1, x1,x1  TT
                 ]

    # Any binary operation at a tree level
    # can be defined in terms of actions matching
    # one of the 6 cases.
    # There are 6 possible actions.
    # DAx eliminate 1st argument from result
    #     and set data to x
    # DBx eliminate 2nd argument from result
    #     and set data to x
    # CA  copy 1st argument into result
    # CB  copy 2nd argument into result
    # NA  copy 1st argument negated into result
    # NB  copy 2nd argument negated into result
    # Actions directly on 8-bit data are done
    # with a sequence of primitive machine operations

    # Definitions of all 16 binary Boolean operations.
    # This table drives the computation both of data and tree nodes.
    # Each operation is a 6 tuple with the name of the operation,
    # a machine operation code to do on byte to byte operations,
    # and the action to take for each possible case. Case 0 is
    # the binop row index reindexed by the right and left hand data
    # bits as a number 0-3.

    binop  = (
    # 0        1     2      3      4      5        Tuple Index
    #                                               a= 0011
    #                                        operation ....
    #                                               b= 0101  Case                                 
    #                       index = a<<1 | b  ===>     0123    0               
    ( 'FALSE', '0',  'DB0', 'DA0', 'DB0', 'DA0' ), #00 0000 Result
    ( 'AND',   '&',  'DB0', 'DA0', 'CB',  'CA'  ), #01 0001    |
    ( 'A<-B',  '<-', 'CB',  'CA',  'NB',  'DA0' ), #02 0010    |
    ( 'A',     'a',  'DB0', 'CA',  'DB0', 'CA'  ), #03 0011    V
    ( '????',  'x',  'DB0', 'DA0', 'DB0', 'CA'  ), #04 0100
    ( 'B',     'b',  'CB',  'DA0', 'CB',  'DA0' ), #05 0101
    ( 'XOR',   '^',  'CB',  'CA',  'NB',  'NA'  ), #06 0110
    ( 'OR',    '|',  'CB',  'CA',  'DB1', 'DA1' ), #07 0111
    ( 'NOR',   '~|', 'NB',  'NA',  'DB0', 'DA0' ), #08 1000
    ( 'EQ',    '~^', 'NB',  'NA',  'CB',  'CA'  ), #09 1001
    ( '~B',    '~b', 'NB',  'DA0', 'NB',  'DA0' ), #10 1010
    ( '????',  'x',  'CB',  'CA',  'CB',  'CA'  ), #11 1011
    ( '~A',    '~a', 'DB0', 'NA',  'DB0', 'NA'  ), #12 1100
    ( 'A->B',  '->', 'DB0', 'NA',  'CB',  'CA'  ), #13 1101
    ( 'NAND',  '~&', 'DB1',  'DA1', 'NB',  'NA'  ), #14 1110
    ( 'TRUE',  '1',  'DB1', 'DA1', 'DB1', 'DA1' )) #15 1111
    #       Shortcut  <-- C1 to C4 Actions -->  Recur  Result
    # Name     raw   Case1  Case2  Case3  Case4 Case5  Case 0   
    # Inputs         0T     T0     1T     T1    TT
    # 0        1     2      3      4      5      

    def __init__(self, x):
        if x == None:                 self.v = Bits.MTbytes        
        elif type(x) == self.tbytes:  self.v = x[:]
        elif type(x) == self.tbarray: self.v = bytes(x[:])
        elif type(x) == self.tint:    self.v = Bits._int_(x)
        elif type(x) == type([]):
            r = self.MT
            for ix in x:
                if type(ix) == self.tint:
                    r = r.OR( Bits(ix) )
                elif type(ix) == self.ttuple and len(ix) == 2:
                    s = ix[0] if ix[0] < ix[1] else ix[1]
                    n = abs( ix[1] - ix[0] ) + 1
                    if n > 1:
                        x = Bits.RANGE(s,n)
                        r = r.OR( x )
                    else:
                        r = r.OR( Bits(s) )
                else: raise error
            self.v = r.v
        elif type(x) == self.tbits:   raise error
        else: raise error
        return

    def LEV(self):
        return self.v[0]
    
    def __bool__(self):
        # Test for any bit on returns True
        # All zeros returns False
        if type(self) != Bits.tbits: return False
        return False if self.v == self.MTbytes else True
    
    def __getitem__(self,ix):
        if ix < 0: raise error
        return self.TEST(ix)

    def __setitem__(self,ix, val):
        if ix < 0: raise error
        bval = bool(val)
        if bval:
            self.SET(ix)
        else:
            self.UNSET(ix)
        return self

    def __len__(self):
        # Returns the conceptual length of bitset
        if type(self) != Bits.tbits: raise error
        return pow8(self.LEV()+1)

    def __str__(self):
        # This should really return a string version of
        # a bitset spec.
        if type(self) != Bits.tbits: raise error
        return Bits.HEX( self )

    def __repr__(self):
        if type(self) != Bits.tbits: raise error
        return Bits.HEX( self )

    def __eq__(self,other,align=True):
        if type(self)  != Bits.tbits: return False
        if type(other) != Bits.tbits: return False
        a, b, = self.v, other.v
        if align: a, b = Bits._align_(self.v,other.v)
        return a == b

    def __ne__(self,other,align=True):
        if type(self)  != Bits.tbits: return True
        if type(other) != Bits.tbits: return True
        a, b = self.v, other.v
        if align: a, b = Bits._align_(self.v,other.v)
        return a != b
        
    def __or__    (self,other): return self.OR(other)
    def __and__   (self,other): return self.AND(other)
    def __xor__   (self,other): return self.XOR(other)
    def __invert__(self):       return self.NOT()
    def __ior__   (self,other): return self.OR(other)
    def __iand__  (self,other): return self.AND(other)
    def __ixor__  (self,other): return self.XOR(other)


    #---------------------------------------------
    #    Return a hex string of the bitset
    #---------------------------------------------

    @staticmethod
    def HEX( pbs, x=None, tlev=None, offset=None, debug=False ):
        # The offset argument specified that each upper level
        # should start on a newline at some number of chars
        # from the beginning of the line.  Offset None makes the
        # resulting hex string have no new lines in it.
        # tlev and offset allow pretty printing a substing in a bitset
        rv = ''
        def get_node( bitset, pos, lev, branch, val ):
            nonlocal rv, toplev, lastlev, offset, indent
            r = ''
            if offset != None and lev > lastlev:
                r += '\n' + indent + ((toplev-lev)*7+2)*' '            
            r += Bits.hexn(bitset, pos)        
            rv += r
            lastlev = lev
            return

        def get_data( bitset, pos, lev, branch, val ):
            nonlocal rv, lastlev
            r = Bits.hexd( bitset, pos )        
            rv += r
            lastlev = lev
            return
        
        if   type(pbs) == Bits.tbits:
            bs = pbs.v
        elif type(pbs) == Bits.tbytes:
            bs = pbs
        elif type(pbs) == Bits.tbarray:
            bs = bytes(pbs)
        else:
            print( "Type is ", type(pbs), pbs )
            return
        # bs should be a bytes when we get here
        if type(bs) != Bits.tbytes:
            if type(bs) == Bits.tbits:
                print("Error bs.v is a Bits value not bytes.")
                bs = bs.v
            else:
                print( "OpType is ", type(bs), bs )
                return
            
        indent = ''
        if offset:
            indent = offset * ' '

        if len( bs ) < 1: return "Empty Bitset"

        if bs[0] == 0:        
            return str(bs[0]) + 'L' + Bits.hexd( bs, 1 )

        if bs[2] == 0:
            return str(bs[0]) + 'L' + Bits.hexn( bs, 1 )

        sp = 1
        if x != None: sp = x

        slev = bs[0]
        if tlev != None: slev = tlev

        if debug: print( "Doing:", Bits.hexx(bs) )

        lastlev = slev
        toplev  = slev

        Bits.work( bs, sp, slev, actn=get_node, actd=get_data, debug=debug )

        if debug: print( "return:", rv )

        return str(slev) + 'L' + rv    

    #--------------------------------------------------

    @staticmethod
    def work( bs, p, lev, actn=None, actd=None, debug=False ):
    #   returns the starting position of the next subtree
    #   if provided actn is called for each tree node
    #   if provided actd is called for each data node
    #   They are called with four parameters;
    #   bs  the bitset
    #   p   the index in the bitset for this node or data
    #   lev the level in the tree this node is at
    #   ep  the element number, which branch of the tree
    #       is this element 0-7
    #   val the index value of the first bit represented
        
        for node in Bits.walk( bs, p, lev, debug=debug ):
            t, bs, lev, p, fb, val = node
            if debug:
                print( "Visit:", t, lev, p, fb, val ) 
            if t == "N":
                if actn != None:                
                    r = actn( bs, p, lev, fb, val )
                    if r != None: return r                    
            elif t == 'D':
                if actd != None: 
                    r = actd( bs, p, lev, fb, val )
                    if r != None: return r
        return

    #--------------------------------------------------
    
    @staticmethod
    def walk( bs, pos, clev, ret_n=True, debug=False ):
    #   bs is a bytes string encoding a bzet.
    #   walk is a generator that yields the position of nodes 
    #   and their type for any subtree or data of bs.
    #   Normally node results have the form:
    #       ( NodeType, pos, lev, bit#, val)
    #       where value is the index value of the
    #       first bit in the subtree.
    #
    #   The argument ret_n changes the results returned:
    #       True  yields of each node in turn. 
    #       False yields only the integer end pos of the subtree.
    #   Note the val returned is unrelable for subtrees,
    #   and accurate, for full bitsets.  The other results
    #   are accurate in all cases.  Walk uses a local stack
    #   and is not recursive.

        def push( ctb, bct, val):
            bsstack.append( (ctb, bct, val) )
            return

        def pop():
            return  bsstack.pop()

        def stack():
            r = ''
            for e in bsstack:
                r += "("+hex2(e[0])+","+str(e[1])+str(e[2])+")"
            return r

        def get_index_pos(tlev):
            # bstack[i][0] is really an octal digit
            remlev = tlev - (len(bsstack)+1)
            ixp = 0
            if len(bsstack) < 1: return( 0 )
            for ix in range(len(bsstack)):                
                ixp *= 8
                ixp += bsstack[ix][0]
            # ixp is the value of the stacked digits
            # bct adds the current level digit
            # remlev is how far above the bottom we are
            return( (8*ixp+bsstack[-1][1])*pow8(remlev) )

                

        debug2 = debug
        endbs = len(bs)
        if pos > endbs or pos < 0: raise error
        p = pos
        tlev = clev
        bsstack = []
        val = 0
        levpos = 0
        
        if debug:
            print( '\n\nWalk of', Bits.HEX( bs ) )
            print( "starting at", p, 'lev =', tlev )

        # visit top data
        if tlev == 0:
            # There is no tree so yield data and quit
            if ret_n: 
                yield( 'D', bs, tlev, p, 0, 0 )
                if debug2:
                    print( 'Yield D', tlev, p, 0, Bits.hexd(bs,p), 0 )
            p += 1
            if not ret_n:
                yield p
            return    

        # visit top node
        if ret_n: 
            yield( 'N', bs, tlev, p, 0, pow8(tlev) )
            if debug2:
                print( 'Yield N', tlev, p, 0, Bits.hexn(bs,p), pow8(tlev) )
        # setup next level
        lev = tlev - 1
        ctb = bs[p+1]
        xi  = 0
        p  += 2   # We are done with this node--advance.

        # Walk the tree
        while len(bsstack) or ctb:
            if debug:
                print('Enter lev',lev,p,Bits.hexn(bs,p-2),stack())
            while ctb and xi < 8: # node needs doing
                abit  = 0x80 >> xi
                nbit  = ~abit & 0xff 
                if debug:                    
                    print( 'while ctb: lev=', lev,\
                        'p=', p, "val=", val, 'ctb=', hex2(ctb) )            
                if ctb & abit:
                    ctb &= nbit  # Turn this bit off
                    if lev == 0: # p is location of data
                        val = get_index_pos(tlev)
                        if ret_n: 
                            yield('D',bs,lev,p,xi,val)
                        if debug2:
                            print("Yield D",lev,p,xi,val,\
                                            Bits.hexd(bs,p))
                        p  += 1                        
                        xi += 1
                        continue

                    # else visit node
                    val = get_index_pos(tlev)
                    if ret_n: yield( 'N',bs,lev,p,xi,val )
                    if debug2:
                        print('Yield N',lev,p,xi,val,Bits.hexn(bs,p))
                    xi += 1
                    # The next node must be one level deeper.
                    # Since the DS is a depth first traversal
                    # so update and save this level then
                    # setup next level and break
                    push( ctb, xi, levpos )
                    if debug:
                        print( 'Save Lev', lev, hex2(ctb),\
                                    xi, stack() )
                    # Setup next level
                    lev -= 1 # do the lower level
                    ctb = bs[p+1]
                    p += 2
                    xi = 0
                    break # and jump out of loop
                xi += 1
                
            else: # Level Loop finished normally
                # ctb should be zero at this point
                # and xi could have advanced to 8
                # Done with this level
                if debug:
                    print( "Exit lev =", lev, "ctb=",\
                            hex2(ctb), "p=", p, val, stack() )

                # mark this part done
                ctb &= nbit
                # Continue to work a higher level
                if len(bsstack): ctb, xi, levpos = pop()
                else:            ctb, xi, levpos = (0, 0, 0)                
                lev += 1
                levval = pow8(lev+1)
                
            
            # Break occurred or we fell through
            # In either case we are set up to work
            # on a new level.
            if debug:
                print( "Break lev:", lev, "ctb=", hex2(ctb), "p=", p, stack() )
            # Finished with this level or
            # Starting a new level one lower
            # or redoing a higher one
            
        # if not ret_n :
        #   print( "\nWalk Final Return:", p )
        if p > endbs or p < 0: raise error
        if not ret_n: yield p
        return


    #--------------------------------------------------
    #
    #   All maintenance of the Bset data structure is 
    #   done here using the following routines.
    #   The routine _binop_ does the work.
    #
    #---------------------------------------------------

    #---------------------------------------------
    #         Normalize and Reduce Level
    #---------------------------------------------

    @staticmethod
    def _normalize_( bitset ):
        # Remove unneccessary upper levels
        # that have zeros at the end of the bitset
        # and convert from bytes to Bits.
        # Convert level zero empty to standard empty.
        
        # Check for lev 0 empty or nothing to do.
        if not bitset[0] and not bitset[1]:   return Bits.MT
        if bitset[1:3] != bytes((0x00,0x80)): return Bits( bitset )

        ending_zeros = bytes((0x00,0x80))
        bs = bytes(bitset[1:])  # Strip the level
        lev = bitset[0]         # Get the level
        while bs[:2] == ending_zeros and lev > 1:
            lev -=1
            bs   = bs[2:]
        # check for level one and nothing left
        
        if lev == 1 and bs[:2] == Bits.MTbytes[1:]:    
            return Bits.MT
        else:            
            return Bits( bytes((lev,)) + bs )
        
    #---------------------------------------------
        
    @staticmethod
    def tobytes( b ):
        # Convert from Bits to bytes
        
        if type(b) == Bits.tbits:
            bset = bytes(b.v)
        elif type(b) == Bits.tbytes:
            bset = b
        elif type(b) == Bits.tbarray:
            bset = bytes(b)
        else: bset = b
        
        if type( bset ) != Bits.tbytes:
            print( "tobytes type error:", type(bset), bset )
            raise error

        return bset
    
    #---------------------------------------------

    @staticmethod
    def _align_( bset1, bset2 ):
        # where bsets are internal bytes strings
        # Make the smaller same level as larger
        # An initial (0x00,0x80) node places 8**lev
        # zeros at the end of the bitarray.
        nlev = bset1[0] - bset2[0]
        if nlev:
            if nlev > 0: # bset1 is larger
                lev = bset1[0]
                bset2 = bytes( (lev,) ) + \
                        bytes( nlev*(0x00,0x80)) + \
                        bytes( bset2[1:] ) 
            else:        # bset2 is larger
                nlev = abs(nlev)
                lev = bset2[0]
                bset1 = bytes( (lev,) ) + \
                        bytes( nlev*(0x00,0x80)) + \
                        bytes( bset1[1:] )                
        return bset1, bset2

    #---------------------------------------------

    @staticmethod
    def align( bset1, bset2 ):
        # where the bsets are Bits
        # Return the two aligned unnormalize Bitsets
        x, y = bset1.v[:], bset2.v[:]
        x, y = Bits._align_(x,y)
        rx = Bits( 1 ) # Create two new bitsets
        ry = Bits( 2 )
        rx.v = x       # Save the aligned bitset bytes string
        ry.v = y       # in them.
        # print( "align rx", rx )
        # print( "align ry", ry )
        return rx, ry  # Send the new aligned strings back

    #---------------------------------------------

    @staticmethod
    def stretch( bset, lev ):
        # where the bset is a Bits
        # and lev is a level
        if bset.lev() >= lev: return bset
        nlev = lev - bset.lev() + 1
        vx = bytes((lev,)) + \
             bytes( nlev*(0x00,0x80)) + \
             bytes( bset[1:] ) 
        return Bits(bsetx.v)

    #---------------------------------------------
    #  Generalized Binary Boolean Operations
    #---------------------------------------------
    
    @staticmethod
    def _binop_(opno,b1,p1,b2,p2,lev,debug=False):
        # operates on two bitsets recursively.
        # returns new positions, a flag, and a bitset
        # The flag if true or false is the value of
        # a compressed bitset. if flag is None then
        # the bitset contains the subtree

        def set_node( sbit, cd, dd, cf, df ):
            nonlocal debug
            nbit = ~sbit & 0xff
            cdr = (cd | sbit) if cf else (cd & nbit)
            ddr = (dd | sbit) if df else (dd & nbit)
            if debug:
                print("set_node input :",hex2(cd),hex2(dd), "flags", cf,df, \
                      "mask:", hex2(sbit))
                print("set_node result:",hex2(cdr),hex2(ddr) )
            return cdr, ddr

        def bscopy( bs, p, lev ): # implements actions CA and CB
            nonlocal debug
            # Find the end of a subtree
            # return from start to finish
            # as a substring
            if p >= len(bs):
                est = len(bs)
            else:
                gest = Bits.walk( bs, p, lev, ret_n=False )
                est = list(gest)[0] # extract end of list
            if debug:
                print( 'bscopy of', p, 'to', est )
            return est, bs[p:est]

        def bsneg( bs, p, lev ):  # implements actions NA and NB
            nonlocal debug
            # Find the end of a subtree
            # return negated subtree from start to finish
            # as a substring
            if p >= len(bs): return len(bs), bytes([])
            gest = Bits.walk( bs, p, lev, ret_n=False )
            est = list(gest)[0]
            if debug:
                print( 'bsneg of', p, 'to', est )
            nbs = bytearray(bs[p:est])
            return est, byte(Bits._not_(nbs, 1, lev))

        def bsdrop( bs, p, lev ):  # implements actions DA and DB
            nonlocal debug
            # Find the end of a subtree
            # return next position in tree
            gest = Bits.walk( bs, p, lev, ret_n=False )
            px = list(gest)[0]
            if px >= len(bs):
                px = len(bs)
            if debug:            
                print( "\nDrop of subtree from", p, "to", px )
            return px

        def do_tree_op( op, lev, bs1, p1, bs2, p2 ):  # tree actions
            nonlocal debug
            # Updates positions and returns a bool or a bytes
            # Bool if result a compressed tree
            # Bytes value if it is a mixed tree
            # Now operate on the data
            def mkbool(x): return True if x == '1' else False
            dr = []
            opx = op[0:2]
            if   opx == 'DA': p1, dr = bsdrop( bs1, p1, lev ), mkbool(op[2])
            elif opx == 'DB': p2, dr = bsdrop( bs2, p2, lev ), mkbool(op[2])
            elif opx == 'CA': p1, dr = bscopy( bs1, p1, lev )
            elif opx == 'CB': p2, dr = bscopy( bs2, p2, lev )
            elif opx == 'NA': p1, dr = bsneg ( bs1, p1, lev )
            elif opx == 'NB': p2, dr = bsneg ( bs2, p2, lev )
            else:
                print( "Tree Op action: ", op, lev, p1, p2 )
                raise error
            if debug:
                print( "Do tree op", op, "returns:", p1, p2, dr )
            return p1, p2, dr

        def do_data_op( op, d1, d2 ):  # data actions
            # Now operate on the data
            nonlocal debug
            if   op == '&':  dr = d1 & d2
            elif op == '|':  dr = d1 | d2
            elif op == '^':  dr = d1 ^ d2
            elif op == '~&': dr = ~(d1 & d2)
            elif op == '~|': dr = ~(d1 | d2)
            elif op == '~^': dr = ~(d1 ^ d2)
            elif op == '0':  dr = 0
            elif op == '1':  dr = 1
            else:
                print( "Machine Op action: ", op, d1, d2 )
                raise error
            if debug:
                print( "do data op", op, "Args:", hex2(d1),\
                       hex2(d2), "Returns:", hex2(dr) )
            return dr


        #---------------------------------------------
        #    Body of all binary Boolean operations
        #---------------------------------------------

        op = Bits.binop[opno]
        if debug:
            print( "In binop: Op is", op )
            print( "          lev", lev, "pos ", p1, p2 )
        debug2 = debug
        bset1, bset2 = b1, b2
        cp1,   cp2   = p1, p2
        cdr,   ddr   = 0,  0
        rxset = bytes([])

        # Setup this level
        if lev < 1:
            cd1, cd2 = 0x00, 0x00 # By the def of lev 0
            dd1, dd2 = bset1[cp1], bset2[cp2]
            cp1 += 1
            cp2 += 1
        else:
            cd1, dd1 = bset1[cp1+1], bset1[cp1] #cd1 is tree byte for bzet1, dd1 is the data byte
            cd2, dd2 = bset2[cp2+1], bset2[cp2]
            cp1     += 2
            cp2     += 2
        if debug:
            print ("Setup cd1", hex2(cd1), hex2(dd1), cp1 )
            print ("Setup cd2", hex2(cd2), hex2(dd2), cp2 )

        #--------------------------------------------------------------
        #
        #            Handle all levels
        #
        #---------------------------------------------------------------


        # Optimization with a level with no Subtrees or Level 0
        # Just process the data bytes
        
        if not (cd1 or cd2):        
                
            ddr = do_data_op( op[1], dd1, dd2 )
            if debug:
                print( "No Subtrees process data bytes lev=", \
                       lev, (cp1,hex2(dd1)), (cp2,hex2(dd2)), hex2(ddr) )

            # Compress and return result
            if   ddr == 0xff: return (cp1, cp2), True,  rxset
            elif ddr == 0x00: return (cp1, cp2), False, rxset
            else:
                if lev == 0: return (cp1, cp2), None, bytes([ddr,])     + rxset
                else:        return (cp1, cp2), None, bytes([ddr,0x00]) + rxset

        # Otherwise step through the 8 possible subtrees.
        # And build the new subtree in rxset.
        
        rxset = []
        for bitno in range(8):
            # Calculate the masks to manipulate the cdr and ddr
            # bit by bit for the eight bits in the node.
            sbit = 0x80 >> bitno
            # From coresponding positions of control and data bits

            # Get the bit values for control and data for
            # this subtree of the 8 subtrees at this level
            # sbit shifts over by one for each interation of
            # the loop.
            
            vc1, vc2 = bool(cd1 & sbit), bool(cd2 & sbit)
            vd1, vd2 = bool(dd1 & sbit), bool(dd2 & sbit)

            # Create the 2 bits of cdr and 2 bits of ddr for the
            # return value. There are six cases from the 16 
            # possible combinations of the control and data bits.
            # The case_type array defines them. Here we use them.
            
            cx  = vc1<<3 | vc2<<2 | vd1<<1 | vd2
            case = Bits.case_type[cx]

            if debug:
                print( op[0], "lev", lev, "Bit", bitno, "case", case, \
                      "set1", hex2(vc1), hex2(dd1),\
                      "set2", hex2(vc2), hex2(dd2) )
            flag = None

            # Case 0 
            # No subtrees here just data
            if case == 0:                
                # opno is a number giving the index of the operation
                # but it can also be seen as 4 bits giving the
                # result of that operation for its 16 different inputs.
                # xr will pick the bit which is the result.
                # The control data in this case is always 0
                # So for the operation AND which is 0001
                # a 1&1 will be 3 in binary shifting 0x80 three bits
                # to the right gives 0001 anding it to 0001 yields True
                # any other combination will yield False which 
                # is after all the meaning of AND.
                xr = vd1<<1 | vd2          # two bits -- 0-3
                x  = bool(opno & 0x08>>xr) # bit result of operation
                if debug:
                    print( "case 0", xr, hex2(opno), vd1, vd2, "gives", x )
                cdr, ddr = set_node( sbit, cdr, ddr, 0, x )
            
            # Cases 1 through 4
            # Compressed subtree as constart data vs. a mixed 
            # subtree on the other side.  Either d vs. t or 
            # t vs. d  Where d can be 1 or 0 and t is a subtree.
            
            elif 1 <= case and case <= 4:
                
                opr = op[case+1]
                # There are really four cases here
                # They are distinguished by the opr
                # associated with this case in the table
                
                cp1,cp2,tr = do_tree_op(opr,lev-1,bset1,cp1,bset2,cp2)
                
                if debug:
                    print( "case", case, opr, cp1, cp2, "gives", tr )
                # Establish the cdr and ddr bits for this case
                
                # Compress the result if necessary
                if type(tr) == Bits.tbool:
                    cdr, ddr = set_node( sbit, cdr, ddr, 0, tr )
                else: # there is a subtree result
                    cdr, ddr = set_node( sbit, cdr, ddr, 1, 0 )
                    rxset += tr
                    
                # The pointers into the two argument have already been
                # advanced properly by the do_tree_op function.

            # Case 5
            # Two mixed subtrees.
            
            elif case == 5:
                # Case 5
                # otherwise recur with bset1 and bset2
                # and append result to the subtree
                if debug2: print( op[0], "case 5 Lev", lev, "bit", bitno,\
                            'recur', op[0]+'ing bs1 at',cp1,'bs2 at',cp2 )
                cp,flag,rset=Bits._binop_(opno,bset1,cp1,\
                                               bset2,cp2,lev-1,debug=debug)
                cp1, cp2 = cp
                if debug:
                    print( "Return cp1=", cp1, "cp2=", cp2, end='' )
                    print( " lev:", lev, "sbit:", sbit, "bit:", \
                           bitno, hex2(cd1), hex2(cd2) )
                # update cdr, ddr, and rxset
                if flag == None:
                    # Mixed results came back, along with a subtree
                    rxset += rset
                    cdr, ddr = set_node(sbit,cdr,ddr,1,0)
                else:
                    # the corresponding ctl bit should be set off
                    # and the data bit set to one or zero
                    cdr, ddr = set_node(sbit,cdr,ddr,0,flag)
                    
            # Case 6
            # Bad data
            
            else: # case == 6:
                print( "Bad data found case:", case, "lev:", lev, "bit:", bitno, \
                       "set1", hex2(dd1), hex2(cd1), \
                       "set2", hex2(dd1), hex2(cd2) )
                raise error
            if debug:
                print( "contine bit loop", bitno, cp1, cp2 )
            # end of processing the 8 subtrees

        # Check to see if this tree node should be compressed

        flag = None
        if not cdr: # There are no subtrees in this node
            if   ddr == 0x00: flag = False
            elif ddr == 0xff: flag = True
            else: pass # We still have mixed data at this node
        if debug:
            print( "Return lev", str(lev)+":", cp1, cp2,\
                   "result:", hex2(cdr),hex2(ddr),flag,Bits.hexx(rxset),"\n")
        if flag == None: return (cp1,cp2), None, bytes([ddr,cdr] + rxset)
        else:            return (cp1,cp2), flag, rxset
        
    #---------------------------------------------
    #  AND of two bitsets
    #---------------------------------------------
    
    def AND( self, other, debug=False ):
        # returns a new bitset that logically ANDs bset1 and bset2
        # make sure levels of rhe two bitsets are equal
        # print( "Enter AND" )
        bset1 = Bits.tobytes( self )
        bset2 = Bits.tobytes( other )
        
        # After this point everything should be a bytes structure.
        # print( "AND of", Bits.HEX(bset1, offset=7) )
        # print( "      ", Bits.HEX(bset2, offset=7) )

        # do the trivial cases.
        # Empty set vs anything
        if bset1 == Bits.MTbytes:  return Bits.MT
        if bset2 == Bits.MTbytes:  return Bits.MT

        # print( "Align bitsets" )

        bset1, bset2 = Bits._align_( bset1, bset2 )
        lev = bset1[0]

        # Full set vs anything
        if bset1[1:3] == bytes((0xff,0x00)): return Bits( bset2 )
        if bset2[1:3] == bytes((0xff,0x00)): return Bits( bset1 )
        
        # print( "byte arrays not empty" )

        # Level 0 bitsets
        if lev == 0:
            bs1, bs2 = bset1[1], bset2[1]
            r = bs1 & bs2
            if r: return Bits( bytes( (lev, r) ) )
            else: return Bits.MT
        
        # do the non-trivial cases recursively
        if debug:
            print( "\nAND of", Bits.HEX(bset1, offset=7) )
            print( "      ", Bits.HEX(bset2, offset=7) )
        
        andop = 1
        pos, f, rset = Bits._binop_( andop, bset1,1,bset2,1,lev,debug=debug )
        if debug:
            print( "AND Returned with:", pos, f, Bits.hexx(rset) )
        if f == None:
            ret = byte(lev) + rset            
            # print( "return value =", Bits.HEX(ret) )
            return Bits._normalize_(ret)
        elif f:        
            return Bits( bytes((lev,0xff,0x00)) ) # The full set
              
        else:
            return Bits.MT                        # The empty set

    #---------------------------------------------
    #  OR of two bitsets
    #---------------------------------------------
    
    def OR( self, other, debug=False ):
        # returns a new bitset that logically ORs bset1 and bset2
        # make sure levels of rhe two bitsets are equal
        # print( "Enter OR" )
        bset1 = Bits.tobytes( self )
        bset2 = Bits.tobytes( other )
        
        # After this point everything should be a bytes structure.
        # print( "OR of ", Bits.HEX(bset1, offset=7) )
        # print( "      ", Bits.HEX(bset2, offset=7) )

        # do the trivial cases.
        # Empty set vs anything
        if bset1 == Bits.MTbytes:  return Bits( other.v )
        if bset2 == Bits.MTbytes:  return Bits( self.v )        
        
        # print( "byte arrays not empty" )

        # print( "Align bitsets" )

        bset1, bset2 = Bits._align_( bset1, bset2 )
        lev = bset1[0]

        # Full set vs anything
        full = bytes( [bset1[0], 0xff, 0x00] )
        
        if bset1 == full: return Bits( full )
        if bset2 == full: return Bits( full )

        # Level 0 bits
        if lev == 0:
            bs1, bs2 = bset1[1], bset2[1]
            r = bs1 | bs2
            if r: return Bits( bytes( (lev, r )) )
            else: return Bits.MT
        
        # do the non-trivial cases recursively
        if debug:
            print( "\nOR  of", Bits.HEX(bset1, offset=7) )
            print( "      ", Bits.HEX(bset2, offset=7) )
        
        orop = 7
        pos, f, rset = Bits._binop_( orop, bset1,1,bset2,1,lev,debug=debug )
        if debug:
            print( "OR Returned with:", pos, f, Bits.hexx(rset) )
        if f == None:
            ret = byte(lev) + rset            
            # print( "return value =", Bits.HEX(ret) )
            return Bits._normalize_(ret)
        elif f:        
            return Bits( full ) # The full set
              
        else:
            return Bits.MT                        # The empty set

    #---------------------------------------------
    #  XOR of two bitsets
    #---------------------------------------------
    
    def XOR( self, other, debug=False ):
        # returns a new bitset that logically XORs bset1 and bset2
        # make sure levels of rhe two bitsets are equal
        # print( "Enter VOR" )
        bset1 = Bits.tobytes( self )
        bset2 = Bits.tobytes( other )
        
        # After this point everything should be a bytes structure.
        # print( "AND of", Bits.HEX(bset1, offset=7) )
        # print( "      ", Bits.HEX(bset2, offset=7) )

        # do the trivial cases.
        # Empty set vs anything is the other
        if bset1 == Bits.MTbytes:  return Bits(bset2) 
        if bset2 == Bits.MTbytes:  return Bits(bset1)

        # Full set vs anything is not of the other
        full = bytes( [bset1[0], 0xff, 0x00] )
        if bset1 == full: return Bits.NOT( bset2 )
        if bset2 == full: return Bits.NOT( bset1 )
        
        # print( "byte arrays not empty" )

        # print( "Align bitsets" )

        bset1, bset2 = Bits._align_( self.v, other.v )
        lev = bset1[0]
        
        # Level 0 bits
        if lev == 0:
            bs1, bs2 = bset1[1], bset2[1]
            r = bs1 ^ bs2
            if r: return Bits( bytes( (lev, r )) )
            else: return Bits.MT
        
        # do the non-trivial cases recursively
        if debug:
            print( "\nXOR of", Bits.HEX(bset1, offset=7) )
            print( "      ", Bits.HEX(bset2, offset=7) )
        
        xorop = 6
        pos, f, rset = Bits._binop_( xorop, bset1,1,bset2,1,lev,debug=debug )
        if debug:
            print( "XOR Returned with:", pos, f, Bits.hexx(rset) )
        if f == None:
            ret = byte(lev) + rset            
            # print( "return value =", Bits.HEX(ret) )
            return Bits._normalize_(ret)
        elif f:        
            return Bits( full ) # The full set
              
        else:
            return Bits.MT      # The empty set



    #---------------------------------------------
    #  Return Not of a bitset
    #---------------------------------------------
    @staticmethod
    def _not_( bset, p, lev, debug=False ):
        # bset points somewhere into a bytearray
        # Will not an arbitrary subtree in place
        # Provided bset is a bytearray

        def not_n( bitset, pos, lev, branch, val ):        
            bitset[pos]  = ~bitset[pos]   & 0xff
            bitset[pos] &= ~bitset[pos+1] & 0xff # Turn off tree bits
            return

        def not_d( bitset, pos, lev, branch, val ):
            bitset[pos] = ~bitset[pos] & 0xff        
            return

        if lev == 0:
            bset[p] = ~bset[p]&0xff
            return
            
        if bset[p+1] == 0x00:  # No Tree bits
            bset[p] = ~bset[p] & 0xff
            return  
                                                
        # Not the bitset in place
        Bits.work( bset, p, lev, actn=not_n, actd=not_d )
        return

    def NOT( self, lev=None, debug=False ):
        bs = bytearray( [] )  # create a new bitset
        if lev != None:
            bs = bytearray( stretch( self, lev ).v[:] )
        else:
            bs = bytearray( self.v[:] )
        Bits._not_( bs, 1, bs[0] ) # NOT it inplace
        return Bits( bs )          # return it unnormalized.


    #---------------------------------------------
    #
    #         Produce a bit set encoding for a
    #         integer giving the bit index
    #
    #---------------------------------------------

    @staticmethod
    def _int_(n):
        # n is the integer
        # INT returns internal byte representation of bitset
        # We need two bytes for each higher level
        # and one for the data level

        if n < 0: raise error
        bs = []
        bs.append( 0x80>>n%8 )   # 0 level data byte
        # print( n, hex2(0x80>>n%8) )
        # print( bs )
        
        lev = 0
        if n < 8:
            # Level zero index
            return bytes( (lev,0x80>>(n&0x07)) )

        n //= 8
        while n > 0:
            cb = 0x80>>(n&0x07)
            bs.insert(0,cb)      # tree byte
            bs.insert(0,0x00)    # data byte
            n //= 8
            lev += 1
            # print( bs )
        bs.insert(0,lev)         # beginning levels number
        return bytes(bs)

    @staticmethod
    def INT(n): Bits( n )
    
    #---------------------------------------------
    #
    #         Produce a bit set encoding for a
    #         range of bits given the beginning
    #         index and the length.
    #  
    #---------------------------------------------

    @staticmethod
    def _range_(s, n, debug=False):
        # s is the starting index
        # n is the length

        # last bit is: start + extent - 1
        # result is a bit-set arch.

        # This normalize code col1apses 0xff data elements
        # created by filling in the space between the two end points.
        # 0x00 data elements are never created so it doesn't bother
        # with them.
        # It is not general code... don't use it in any other context.
        # It expects bitsets of a rigid structure that are created by _int_
        #                                            0 12        oe len-1
        # The structure of both sx and ex is always: L DT DT ... DT D
        # The structure of rx can be: L DT DT ... DT D   (size is even) or
        #                             L DT DT ... DT D D (size is odd)
        # The last form only occurs when generating sixteen bits or less.

        def norm( bap ):
            ba = bap[:]
            end_ba = len(ba)
            if end_ba & 0x01:
                # size is odd ... double data at end
                # See if it collapses
                if ba[-1] == 0xff:
                    tfb = first_bit(ba[-3]) # Find bit
                    x   = 0x80 >> tfb # Recreate the first bit
                    y   = ba[-3] ^ x  # Remove first bit, y is second bit
                    ba[-3] ^= y       # Remove second bit from Tree
                    ba[-4] |= y       # Compress result by setting data bit
                    ba = ba[:-1]      # Remove the 0xff data byte
                # If it doesn't collapse -- nothing else can collapese either
                else: return ba 
            
            # Now process the rest... there is no double data any more          
            # Because we either compressed the last byte or we returned
            # knowing no more compression was possible.
            end_ba = len(ba) -1
            for ix in range( end_ba, 1, -2 ):
                if ba[-1] == 0xff:       # Is data full
                    ba[-3] |= ba[-2]     # yes, collapse into prior data
                    ba[-2] = 0           # remove subtree
                    if ix == end_ba:     # lonely D?
                        del ba[-1]       # Delete it
                    else:
                        del ba[-2:-1]    # Full Node, Delete it
                else: break
            return ba

        if debug:
           print( "RANGE from ", s, "to", s+n-1 )

        sx  = Bits._int_(s)
        ex  = Bits._int_(s+n-1)
        sx, ex = Bits._align_(sx,ex)
        
        if debug:
            print( "start", Bits.hexx(sx) )
            print( "last ", Bits.hexx(ex) )    
        
        if sx[0] == 0:  # Level 0
            if s+n < 8:
                return( bytes([0,mkbits(s,s+n-1)]) )
            else: raise error

        # All Levels > 0

        # Small ranges 16 bits or less

        sm8 = s & 0x07
        if sm8 + n <= 8:  # We are still in the same byte
            # One byte result
            rx = bytearray( sx[:-1] )              # Remove single bit
            rx += bytes( [mkbits( sm8, sm8+n ),] ) # Replace with range
            return( bytes( norm(rx) ) )         

        elif sm8+n <= 16:
            # Here one always gets a two data byte result
            rx = bytearray( sx[:-1] )   # Remove single bit
            rx[-1] = (sx[-2] | ex[-2])  # Set tree bits
                                        # Give it 2 data bytes
            rx += bytes( [ mkbits(sm8,8), \
                           mkbits(0,n-((8-sm8)+1))] ) 
            return( bytes( norm(rx) ) )

        # Ranges that split across nodes

        # Merge and fill
        head = bytearray([])
        tail = bytearray([0x00,]) # phony Lev field
        head.append( sx[0] )      # Result depth

        ixs = 1
        ixe = 1
        do_lead = True
        do_sx   = True
        dflag   = False

        while True:
            # print( do_lead, do_sx, dflag, ixs, ixe )
            ends = len(sx)
            if do_lead:
                if sx[ixs+1] == ex[ixe+1]: # Check tree bytes
                    # Both have same subtree
                    # Both must have all zero data bits
                    head.append( sx[ixs]   ) # Data bytes always 0x00             
                    head.append( sx[ixs+1] ) # Tree bit
                    ixs += 2
                    ixe += 2
                    continue
                else:
                    # Subtrees have separated
                    # Fill data portion with ones
                    # between the two trees.
                    do_lead = False
                    dflag = False
                    sbit = first_bit(sx[ixs+1])+1
                    ebit = first_bit(ex[ixe+1])-1
                    if debug:
                        print( "sbit=", sbit, "ebit=", \
                               ebit, hex2(mkbits(sbit,ebit) ) )
                    if ebit - sbit > 0:
                        head.append( mkbits(sbit,ebit) ) # Data
                    else:
                        head.append( 0x00 )              # NaDa
                    head.append( sx[ixs+1] | ex[ixe+1] ) # Table
                    if n <= 64 and ixs == len(sx)-3:
                        # Data has separated but no trees yet
                        # Just two data bytes.
                        # First data byte
                        # print( ixs, hex2( sx[ixs] ), \
                        #             hex2( sx[ixs+1] ),\
                        #             hex2( sx[ixs+2]) )
                        sbit = first_bit(sx[ixs+2])
                        head.append( mkbits(sbit,7) )
                        # Second data byte
                        ebit = first_bit(ex[ixe+2])
                        head.append( mkbits(0,ebit) )
                        # And we are finished
                        rx = bytes( norm( head ) )
                        if debug:
                            print( "start ", Bits.hexx(sx) )
                            print( "last  ", Bits.hexx(ex) )
                            print( "head  ", Bits.hexx(head) )
                            print( "norm  ", Bits.hexx(rx) )
                        return( rx )
                    # print( "done do_lead", Bits.hexx(head) )                    
                    ixs += 2
                    ixe += 2                    
                    continue
            elif do_sx:
                # Copy and post fill the rest of sx
                sbit = first_bit( sx[ixs+1] )
                if dflag:                    
                    head.append(mkbits(sbit,8))   # Data
                    do_sx = False
                    dflag = False
                    continue
                else:
                    head.append(mkbits(sbit+1,8)) # Data
                    head.append(sx[ixs+1])        # Table
                # print( "do_head ", Bits.hexx(head) )

                # Warn me if the end is near!
                # The end of sx always has only a data block
                # with a single bit in it.
                if ixs+3 == ends:
                    dflag = True
                    ixs += 1
                else:
                    ixs += 2
                continue

            else:  # do_ex
                # Copy and pre fill the rest of ex
                ebit = first_bit( ex[ixe+1] )
                if dflag:                    
                    tail.append(mkbits(0,ebit))  # Data
                    break
                else:
                    tail.append(mkbits(0,ebit-1))  # Data
                    tail.append(ex[ixe+1])         # Table
                # print( "do_tail ", Bits.hexx(tail) )
                
                # Warn me if the end is near!
                # Likewise for ex.
                if ixe+3 == len(ex):
                    dflag = True
                    ixe += 1
                else:
                    ixe += 2
                continue
                
        if debug:
            print( "start ", Bits.hexx(sx) )
            print( "last  ", Bits.hexx(ex) )
            print( "head  ", Bits.hexx(head) )
            print( "tail  ", Bits.hexx(tail) )
            
        rx = norm(head) + norm( tail )[1:] # Strip phony Lev field
        if debug:
            print( "norm  ", Bits.hexx(rx) )
            
        return bytes(rx)

    @staticmethod
    def RANGE( s, n, debug=False ):
        # Returns a bitset with ones
        # starting at s and extending n bits
        # where s and n are integers
                
        if n < 0 or s < 0: raise error

        if n == 0: return Bits(None)

        v = Bits._range_( s, n, debug )        
        return Bits(v)
    #---------------------------------------------

    def size( self ):
        # returns a integer that tells how many bytes is
        # being used to represent the compressed bitset.

        return len( self.v )

    #---------------------------------------------

    def EQ( self, other ):
        # returns a new bitset that logically EQs its inputs
        # using the idea that EQ can be built out of XOR

        return self.XOR( other ).NOT()

    #---------------------------------------------
    # Imperative Commands, which shouldn't retun anything.
    # but which do return the argument for convenience, so
    # they can be used like functions as well.
    #---------------------------------------------

    def SET( self, n ):
        # Sets a bitset with nth bit turned on
        self.v = self.OR( Bits(n) ).v
        return self

    #---------------------------------------------

    def UNSET( self, n ):
        # Sets a bitset with the nth bit turned off
        a = self
        b = Bits( n )
        # This is tricky because of NOT
        # Align makes INVERT work properly
        # by extending b to the size of self.
        a, b = Bits.align(a, b)
        b.INVERT()
        c = a & b
        self.v = c.v
        return self

    #---------------------------------------------

    def FLIP( self, n ):
        # Sets a bitset with the nth bit inverted
        if self.TEST(n):
            self.UNSET( n )
        else:
            self.SET( n )
        return self

    #---------------------------------------------

    def INVERT( self ):
        # NOTs an entire bitset in place
        vx = bytearray(self.v)
        Bits._not_(vx, 1, self.v[0])
        self.v = bytes(vx)
        return self

    #---------------------------------------------

    def CLEAR( self ):
        # Zeros a bitset.
        self.v = self.MT.v
        return self

    #---------------------------------------------
    #  Dust a bitset ( in place )
    #---------------------------------------------
    
    def DUST( self, debug=False ):
        # Will rid Bits of bits in data that have a
        # corresponding bit in the control

        def dust_n( bitset, pos, lev, branch, val ):        
            bitset[pos] &= ~bitset[pos+1] & 0xff # Turn on tree bit in data
            return
        bset = bytearray(self.v)                                      
        # Clean the bitset in place
        Bits.work( bset, 1, bset[0], actn=dust_n, actd=None )
        self.v = bytes(bset)
        return self


    #---------------------------------------------
    #---------------------------------------------

    def TEST( self, n ):
        # Returns the nth bit as a truth value
        if self.v == Bits( bytes((0x00,0x00)) ) or self == Bits.MT:
            return False
        return bool( self.AND( Bits(n) ) )

    #---------------------------------------------
    #    Count True bits in bitset
    #---------------------------------------------


    def COUNT( self, debug=False ):

        def count_n( bitset, pos, lev, branch, xval ):
            nonlocal bitc, debug
            bitc += pow8(lev)*bit_count(bitset[pos])
            return    

        def count_d( bitset, pos, lev, branch, xval ):
            nonlocal bitc, debug
            bitc += bit_count(bitset[pos])
            return
        
        bitc = 0
        bs = self.v
        if bs[0] == 0: return bit_count(bs[1]) # Level 0
        if bs[2] == 0: # For any level with no subtrees
        # number of bits is number of bits * 8**lev
            if bs[1] == 0: return 0
            return pow8(bs[0]) * bit_count(bs[1])
        # Otherwise work the tree
        Bits.work( bs, 1, bs[0], actn=count_n, actd=count_d )
        return bitc


    #---------------------------------------------
    #    Find index of First Bit
    #---------------------------------------------


    def FIRST( self, debug=False ):

        def fbit_n( bitset, pos, lev, branch, xval ):
            nonlocal val, debug
            possbits = bitset[pos] | bitset[pos+1]
            fb = first_bit( possbits )
            if fb == None: return None
            tbit = 0x80 >> fb
            if bitset[pos+1] & tbit:
                # Not here but in this subtree
                # save value and get next node
                val += pow8(lev) * fb
                if debug:
                    print( "1stbit node pos:", pos, "val=", val, "fb=", fb,\
                           "branch=", branch )
                return
            else:
                # bit is here
                # Compute value
                return val + pow8(lev) * fb        

        def fbit_d( bitset, pos, lev, branch, xval ):
            nonlocal val
            # bit has to be here
            # The multiplier must be 1
            fb = first_bit( bitset[pos] )
            return val + fb        

        # print( '1st bit of', Bits.HEX(self, offset=11) )
        bs = self.v
        val = 0
        if bs[0] == 0: return first_bit(bs[1]) # Level 0
        if bs[2] == 0: # For any level with no subtrees
            # index of first bit is in the first bit of data
            if bs[1] == 0: return None
            return pow8(bs[0]) * first_bit(bs[1])
        # Otherwise work the tree
        v = Bits.work( bs, 1, bs[0], actn=fbit_n, actd=fbit_d )
        return v


    #---------------------------------------------
    #    Find index of Last Bit
    #---------------------------------------------


    def LAST( self, debug=False ):
        
        def last_n( bitset, pos, lev, branch, xval ):
            nonlocal xbit, debug
            if bitset[pos] and not bitset[pos+1]:
                # Node is a leaf node
                # Calculate index of last bit of sequence
                p8 = pow8(lev)
                if debug:
                    print( "-->N val at ", lev, p8, pos, branch, "is", xval )           
                #      start  Last member
                #      index  of the group
                xbit = xval + p8 - 1
                if debug:
                    print( "-->set xbit to", xbit )
            return    

        def last_d( bitset, pos, lev, branch, xval ):
            nonlocal xbit, debug
            if debug:
                print( "-->D val at ", lev, pos, branch, "is", xval )
            xbit = xval + pow8(lev)*last_bit(bitset[pos])
            if debug:
                print( "-->set xbit to", xbit )
            return
        
        xbit = None
        bs = self.v
        if bs[0] == 0: return last_bit(bs[1]) # Level 0
        if bs == Bits.MT:                     # For the empty bitset
            return None
        # Otherwise work the tree
        Bits.work( bs, 1, bs[0], actn=last_n, actd=last_d, debug=debug )
        return xbit


    #---------------------------------------------
    #   Generate list of bits in a bitset as integers
    #---------------------------------------------
    
    def LIST_T( self, debug=False, dstart=None, limit=None ):
        # a generator that returns a list of bit positions that are set
        bset = self.v
        levs = bset[0]
        intset = []
        if bset[0] == 0:
            bitx = bset[1]
            for ix in range(8):
                if not bitx: break
                if 0x80 & bitx:
                    yield ix
                bitx <<= 1
                bitx &= 0xff
            return
        if bset[2] == 0x00 and bset[1] == 0x00:
            return
        if debug:
            print( "\nLIST:", Bits.hex( bset, offset=6 ) )
        tset = Bits(self.v[:])
        # print( "nset is", nset )
        # print( "Bits.MT", Bits.MT )
        # print( "LIST_T temp is", Bits.hexx( nset ) )
        ix = 0
        if limit == None:
            xlimit = tset.COUNT()
        else:
            xlimit = limit
        while tset:      # Reduce until empty set
            if ix >= xlimit: break
            fbit = tset.FIRST()  # Get index of first bit
            if debug: print( "LIST T yeilds", fbit )
            yield fbit
            tset.UNSET(fbit)
            if debug:
                print( "In LIST ix:", ix )
                print( "In LIST tset:", Bits.HEX(tset, offset=6) )
            ix += 1
        return

# Initialize recursive elements

Bits.MT    = Bits(None)
Bits.tbits = type(Bits(None))

#----------------------------------------------------------------------------
#                          End Bits Class
#----------------------------------------------------------------------------



# Given a Gregorian date return a Julian Day and vice versa
# Method from Wikipedia article on Julian Days

def julian_day(yr, month, day ):
    a = (14-month )//12
    y = yr + 4800 - a
    m = month + 12*a -3
    return int(day + (153*m+2)//5 + 365*y + y//4 - y//100 + y//400 - 32045)

def day_of_week( jd ): return jd % 7 # 0 is Monday

def gregorian_date( jd ):
    j   = int(jd) + 32044
    g   = j // 146097
    dg  = j %  146097
    c   = (3*( dg//36524 + 1)) // 4
    dc  = dg - 36524*c
    b   = dc // 1461
    db  = dc %  1461
    a   = (3*(db/365 + 1)) // 4
    da  = db - 365*a
    y   = 400*g + 100*c + 4*b + a    # Years since 4801BC-4-1
    m   = (5*da + 308)//153 - 2      # Months since x-4-1
    d   = da - (153*(m+4)//5) + 122  # Days since  x-x-1
    yr  = int(y - 4800 + (m+2)//12)
    mo  = int((m + 2)%12 + 1)
    day = int(d + 1)
    return (yr, mo, day)
       

#--------------------------------------------------------------
#
#            Main Program and Test Code
#
#---------------------------------------------------------------


if __name__ == "__main__":
    

    def Bitb(x):
        y = Bits(bytes(x))
        y.DUST()
        return y
    
    def truebits( bitset, head, dstart=None, limit=None ):
        print( "List of indicies of True bits in", head )
        if limit != None:
            print( "Output limited to ", limit, "values." )
        i = 0
        out = ''
        for n in bitset.LIST_T(dstart=dstart,limit=limit):                
            if i % 5 == 0:
                  out +=  '\n ' + '{0:6d}:  '.format(i)
            out += '{0:6,d}; '.format(n)
            i += 1
        if i > 0:
            out = out[:-2] + '.\n'
            out += str(i) + " True bits found"
            print( out )
        else:
            print( "No bits in bitset." )
        return

    def check_sym( a, b, op, opname, debug=False ):
        r1 = op(a,b, debug=debug)
        r2 = op(b,a, debug=debug)
        if r1 != r2:
            print( opname, "failed symetry test" )
            print( "r1 = ", Bits.HEX( r1, offset=6 ))
            print( "r2 = ", Bist.HEX( r2, offset=6 ))
            raise error
        return

    def bitstring( a, cmt ):
        print( cmt, end="" )
        for bit in a:
            if bit:
                print( "1", end="" )
            else:
                print( "0", end="" )
        print()
        return

    print( "Testing:", version )

    one    = Bitb( [0,0x40] )
    seven  = Bitb( [0,0x41 ] )
    print( '\na  =   ', one, "count =", one.COUNT() )
    print( 'b  =   ', seven, "count =", seven.COUNT() )
    r1 = one.AND(seven, debug=False)
    check_sym( one, seven, Bits.AND, "AND")
    r6 = one.XOR(seven, debug=False)
    check_sym( one, seven, Bits.XOR, "XOR")
    r7 = one.OR(seven, debug=False)
    check_sym( one, seven, Bits.OR, "OR")
    print( "a & b =", r1, "count =", r1.COUNT() )
    print( "a | a =", r7, "count =", r7.COUNT() )
    print( 'a ^ b =', r6, "count =", r6.COUNT() )
    print( "beg & end a & b", r1.FIRST(),  r1.LAST() )
    print( "beg & end a | b", r7.FIRST(),  r7.LAST() )
    print( "beg & end a ^ b", r6.FIRST(),  r6.LAST() )

    lev1x = Bitb( [1,0x07,0x00] )
    lev1y = Bitb( [1,0x31,0x00] )
    print( '\na  =   ', lev1x, "count =", lev1x.COUNT() )
    print( 'b  =   ', lev1y,   "count =", lev1y.COUNT() )
    r1 = lev1x.AND(lev1y, debug=False)
    check_sym( lev1x, lev1y, Bits.AND, "AND")
    r6 = lev1x.XOR(lev1y, debug=False)
    check_sym( lev1x, lev1y, Bits.XOR, "XOR" )
    r7 = lev1x.OR(lev1y, debug=False)
    check_sym( lev1x, lev1y, Bits.OR, "OR" )
    print( 'a & b =', r1, "count =", r1.COUNT() )
    print( 'a | b =', r7, "count =", r7.COUNT() )
    print( 'a ^ b =', r6, "count =", r6.COUNT() )
    print( "beg & end a & b", r1.FIRST(),  r1.LAST() )
    print( "beg & end a | b", r7.FIRST(),  r7.LAST() )
    print( "beg & end a ^ b", r6.FIRST(),  r6.LAST() )

    lev2x = Bitb( [2, 0x07,0x80, 0x00,0x84, 0x55, 0xfe ] )
    lev2y = Bitb( [2, 0x01,0x80, 0x00,0x80, 0x05 ] )
    print( '\na  =   ', lev2x, "count =", lev2x.COUNT() )
    print( 'b  =   ', lev2y, "count =", lev2y.COUNT() )
    r1 = lev2x.AND(lev2y, debug=False)
    check_sym( lev2x, lev2y, Bits.AND, "AND")
    r6 = lev2x.XOR(lev2y, debug=False)
    check_sym( lev2x, lev2y, Bits.XOR, "XOR")
    r7 = lev2x.OR(lev2y, debug=False)
    check_sym( lev2x, lev2y, Bits.OR, "OR")
    print( 'a & b =', r1, "count =", r1.COUNT() )
    print( 'a | b =', r7, "count =", r7.COUNT() )
    print( 'a ^ b =', r6, "count =", r6.COUNT() )
    print( "beg & end a & b", r1.FIRST(),  r1.LAST() )
    print( "beg & end a | b", r7.FIRST(),  r7.LAST() )
    print( "beg & end a ^ b", r6.FIRST(),  r6.LAST() )

    lev3x = Bitb( [3, 0x07,0x80, 0x00,0x80, 0x00,0x84, 0x55, 0xfe ] )
    lev3y = Bitb( [3, 0x07,0x80, 0x00,0x80, 0x00,0x84, 0x05, 0xef ] )
    print( '\na  =   ', lev3x, "count =", lev3x.COUNT() )
    print( 'b  =   ', lev3y, "count =", lev3y.COUNT() )
    r1 = lev3x.AND(lev3y, debug=False)
    check_sym( lev3x, lev3y, Bits.AND, "AND")
    r6 = lev3x.XOR(lev3y, debug=False)
    check_sym( lev3x, lev3y, Bits.XOR, "XOR")
    r7 = lev3x.OR(lev3y, debug=False)
    check_sym( lev3x, lev3y, Bits.OR, "OR")
    print( 'a & b =', r1, "count =", r1.COUNT() )
    print( 'a | b =', r7, "count =", r7.COUNT() )
    print( 'a ^ b =', r6, "count =", r6.COUNT() )
    print( "beg & end a & b", r1.FIRST(),  r1.LAST() )
    print( "beg & end a | b", r7.FIRST(),  r7.LAST() )
    print( "beg & end a ^ b", r6.FIRST(),  r6.LAST() )

    num3x = Bitb( [1, 0x00,0xc0, 0x40, 0x20 ] )
    num3y = Bitb( [1, 0x00,0x18, 0x07, 0xf0 ] )
    print( '\na  =   ', num3x, "count =", num3x.COUNT() )
    print( 'b  =   ', num3y, "count =", num3y.COUNT() )
    r1 = num3x.AND(num3y, debug=False)
    check_sym( num3x, num3y, Bits.AND, "AND")
    r6 = num3x.XOR(num3y, debug=False)
    check_sym( num3x, num3y, Bits.XOR, "XOR")
    r7 = num3x.OR(num3y, debug=False)
    check_sym( num3x, num3y, Bits.OR, "OR")
    print( 'a & b =', r1, "count =", r1.COUNT() )
    print( 'a | b =', r7, "count =", r7.COUNT() )
    print( 'a ^ b =', r6, "count =", r6.COUNT() )
    print( "beg & end a & b", r1.FIRST(),  r1.LAST() )
    print( "beg & end a | b", r7.FIRST(),  r7.LAST() )
    print( "beg & end a ^ b", r6.FIRST(),  r6.LAST() )
    truebits( r7, 'Bits in a|b')
    

    print( "\nCheck Last." )
    t1 =  Bits( bytes( (2, 0x40, 0x00) ) )
    print( t1, "beg,end is", t1.FIRST(), t1.LAST(debug=True), t1.COUNT() )
    t2 =  Bits( bytes( (2, 0xc0, 0x00) ) )
    print( t1, "beg,end is", t2.FIRST(), t2.LAST(debug=True), t2.COUNT() )
    t3 =  Bits( bytes( (3, 0x00, 0x80, 0x80, 0x00) ) )
    print( t1, "beg,end is", t3.FIRST(), t3.LAST(debug=True), t3.COUNT() )

    # raise error

    print( "\nCheck range setting." )
    x1 = Bits.RANGE(100,51)
    print( "Bits 100-150:", x1, "count =", x1.COUNT() )
    print( "First bit of 100-150:", x1.FIRST() )
    print( "beg & end a & b", r1.FIRST(),  r1.LAST() )
    truebits( x1, "100-150" )

    x2 = Bits.RANGE(0,255)
    print( "\nBits 0-254:", x2, "count =", x2.COUNT() )
 
    x3 = Bits( [(512,382),] )
    print( "\nBits 512-382:", x3, "count =", x3.COUNT() )

    x4 = Bits.RANGE(0,2)
    print( "\nBits 0-1:", x4, "count =", x4.COUNT() )
    truebits( x4, "0-1" )

    x5 = Bits.RANGE(29,35)
    print( "\nBits 29-63:", x5, "count =", x5.COUNT() )
    truebits( x5, "29-63" )
    

    # raise error

    print( "\nMT =        ", Bits.MT )
    print( "Bits(None) =", Bits(None) )
    print( "Is MT equal to Bits(None)?",\
           "Yes" if Bits.MT == Bits(None) else "No" )
    print( "MT is not equal to Bits(None).",\
           "True they are not equal" if Bits.MT != Bits(None) \
           else "No, they are equal" )
    print( "Bits(3) =          ", Bits(3) )
    print( "Bits(300) =        ", Bits(300) )
    # print( Bits.HEX(Bits(300), debug=True) )
    # raise error
    print( "Is Bits(3) equal to Bits(300)?",\
           "Yes" if Bits(3) == Bits(300) else "No" )
    print( "Is Bits(300) equal to Bitb([2,0x00,0x08,0x00,0x04,0x08])?", \
            "True" if Bits(300) == Bitb(((2,0x00,0x08,0x00,0x04,0x08)))\
            else "False" )
    bst0 = Bitb([0x01, 0x05, 0x05, 0x4f, 0xf4 ])
    print( "\nShow bst0:", Bits.hexx(bst0), "\n", bst0 )
    print( "\nfirst bit of ", Bits.HEX( bst0 ), 'is',  bst0.FIRST() )
    truebits( bst0, "bst0", dstart=47 )

    bst1 = Bitb(
           [0x04,  0x00,0x05,  0x00,0x80,  0x00,0x04,  0x01,0x10, 0x34,\
                               0x00,0x40,  0x00,0x70,  0x00,0x10, 0x1f,\
                                                       0x00,0x0c, 0x2e, 0xe2,\
                                                       0x00,0x01, 0x3b ] )
    print( "\nShow bst1:", Bits.HEX(bst1,offset=11), "\n" )
    truebits( bst1, "bst1" )
    nbst1 = ~bst1            
    print( "\n\nShow not bst1:\n" + Bits.HEX(nbst1, offset=0) )                                                     
    
    ix   = bst1.FIRST()
    bst2 = Bits(ix)
    bst3 = Bitb([0x01, 0x80, 0x00, 0x8a ])
    print( "first bit of bst3", Bits.HEX( bst3 ), 'is', bst3.FIRST() )
    bst4 = Bits(bytes([0x03, 0x08,0x80,  0x00,0x40,  0x23,0x00 ]))
    print( "first bit of bst4", Bits.HEX( bst4 ), 'is', bst4.FIRST() )
    bst5 = bst1 & bst2
    check_sym( bst1, bst2, Bits.AND, "AND" )
    print( "bst1 is ", bst1 )
    print( "bst2 is ", bst2 )
    print( "bst5 is bst1 AND bst2 =", bst5 )
    
    
    bst6 = Bitb([0x4,  0x0f,0x30,  0xf0,0x40,  0x24,0x08,  0x00,0x80, 0x70,\
                       0x01,0x20,  0x02,0x10,  0x00,0x20, 0x38 ])
    
    print( "\nfirst bit of ", Bits.HEX( bst1, offset=14 ), '\nis', bst1.FIRST() )
    print( "\nfirst bit of ", Bits.HEX( nbst1,offset=14 ), '\nis', nbst1.FIRST() )    
    print( "bitset for", ix, Bits.HEX(bst2) )
    print( "bitset for", ix+1, Bits.HEX(Bits(ix+1)) )
    print( "Bitset for", ix-1, Bits.HEX(Bits(ix-1)) )
    
    print( "\nand of bst1 and it's first bit:", Bits.HEX( bst5 ) )
    print( "and of bst1 and bs(20825):", Bits.HEX( bst1.AND(Bits(ix-1) ) ) )
    print( "and of bst1 and bs(20827):", Bits.HEX( bst1.AND(Bits(ix+1) ) ) )
    print( "Is the first bit of bst1 on?",  "Yes" if bst1[ix]   else "No" )
    print( "Is the next bit of bst1 on?",   "Yes" if bst1[ix+1] else "No" )
    print( "Is the bit in front of it on?", "Yes" if bst1[ix-1] else "No" )
    print( "\nBst6 is", Bits.HEX( bst6 ) )
    truebits( bst6, "bst6", limit=50 )
    print( "\nBitsets for the integers 0 to 25" )
    for i in range(25):
        print( i, ":", Bits.HEX(Bits(i), offset=6 ) )

    print( "\nbst1 AND bst6 is", Bits.HEX( bst1.AND( bst6, debug=False ), offset=17 ) )
    print( "bst1 OR bst6 is", Bits.HEX( bst1.OR( bst6 ), offset=16 ) )

    print( "\n\nJulian Day for 2011-2-25 is", julian_day(2011,2,25) )
    print( "Gregorian Date for 2455618 is", gregorian_date(2455618) )

    days = ( 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun' )
    print( "Julian Day 2455618 is", days[ day_of_week(2455618) ] )

    jdbs0 = Bits(4000000)
    nlev = jdbs0.LEV()
    print( "GD 1/1/1 is JD", julian_day(1,1,1) )
    print( "JD 2457000 is", gregorian_date(2457000) )
    print( "JD 2458000 is", gregorian_date(2458000) )
    print( "JD 2459000 is", gregorian_date(2459000) )
    print( "JD 2460000 is", gregorian_date(2460000) )
    print( "JD 2500000 is", gregorian_date(2500000) )
    print( "JD 3000000 is", gregorian_date(3000000) )
    print( "JD 4000000 is", gregorian_date(4000000) )

    print( "A Julian Day in a bitset requires ", nlev, "levels." )
    print( "7 level Julian Days last to:", len(jdbs0),\
           gregorian_date( len(jdbs0) ) )

    jdayx = julian_day(2011,5,5)
    print( "2011 Cinco de Mayo is", jdayx )
    jdbuz = julian_day(1940,9,29)
    print( "\nBuz birthday as Julian day", jdbuz )
    jdayz = Bits( [(jdbuz,jdayx),] )
    print( "my Life is", jdayz, jdayz.COUNT(), "days" )
    print( "Last bit of my life:", jdayz.LAST(), "should be", jdayx )

    wwii = Bits( [(julian_day(1939,9,1),julian_day(1945,8,15))])
    print( "WWII:", wwii, wwii.COUNT(), "days" )

    print( "Buz and WWII", jdayz & wwii, (jdayz&wwii).COUNT(), "days" )

    print( "\nBuz birthday as Julian day", jdbuz )
    print( "Buz was born on a", days[ day_of_week(jdbuz) ] )
    jdbuzbs = Bits( jdbuz )
    print( "\nBuz birthday as Julian bitset", Bits.HEX( jdbuzbs ) )
    someday = julian_day(2011,4,11)    
    dayc = someday-jdbuz + 1    

    print( "\nCalculate bitset SETing each bit" )
    jdbuzbs.CLEAR()
    for i in range(jdbuz,someday+1):
        jdbuzbs.SET(i)   
    print( "Buz alive after ", dayc, "days  ", Bits.HEX( jdbuzbs, offset=30 ) )

    fastjd = Bits.RANGE(jdbuz,dayc)
    print( "\nCalculate same bitset with RANGE method" )
    print( "Buz alive after ", dayc, "days  ", Bits.HEX( fastjd, offset=30 ) )
    print( "Bit count is ", fastjd.COUNT() )


    print( "\n\nFibonacci Set Test" )

    def fib(n):
        x, y, z = 0, 1, 1
        yield 1
        for ix in range(n-1):
            z = x + y
            yield z
            x, y = y, z
        return

    x1 = Bits( [1, 18, (20,35), 610] )
    # x1 = Bits( [1, 10, (29,35), 611] )
    # x1 = Bits( [4, 10, (29,33), 611] )
    print( "nums: [1, 18, (29,35), 562]\nis :", Bits.HEX(x1, offset=5 ) )
    print( "Number of nums:", x1.COUNT() )
    print( "nums: ", end= "")
    for ix in x1.LIST_T():   print( ix, "", end="" )
    print( "\n" )

    fibs = Bits( None )
    for f in fib(21): fibs[f] = True
    print( "Fibonacci set:\nis ", Bits.HEX(fibs, offset=4 ) )
    print( "Number of fibs:", fibs.COUNT() )
    print( "Fibs: ", end= "")
    for fn in fibs.LIST_T(): print( fn, "", end="" )
    print( "\n" )

    # Find Fibonacci Sequence numbers in x1

    check_sym( x1, fibs, Bits.AND, "AND" )
    x2 = x1.AND(fibs)
    check_sym( x1, fibs, Bits.AND, "AND" )
    print( "Fibs in Nums:", Bits.HEX(x2, offset=12), 'Count=', x2.COUNT() )
    truebits( x2, "Fibs in Nums", limit=10 )
    #for ix in x2.LIST_T(): print( "\nFibs in nums:", ix, end="" )

    x2.INVERT()
    print( "\nNOT fibs in nums:\nbitset is", x2, "Count=", x2.COUNT() )
    ix = 0
    print( "true bits are" )
    for fn in x2.LIST_T():        
        if ix%10 == 0: print( "" )
        print( '%03d ' % (fn,), end='' )
        ix += 1
        if ix > 150:
            print( "..." )
            break
        

