#!/usr/bin/python3

import sys
#Graph Class

class graph:
    
    def __init__(self,name="",total_rep = {}):
        self.total_rep = total_rep
        self.name = name

    def list_graph(self):
        return self.total_rep
        
    def add_node(self,node):
        self.total_rep[node] = {}

    def add_edge(self,node_from,node_to):
        if node_from in self.total_rep.keys() :
            try :
                self.total_rep[node_from][node_to] = 1
            except :
                self.total_rep[node_from] = {}
                self.total_rep[node_from][node_to] = 1
        else :
            print("add_edge failed: node not in graph")
                     

    def list_neighbors(self,node):
        if node in self.total_rep.keys() :
            try :
                return self.total_rep[node]
            except :
                return "None"
        else :
            print("list_neighbors failed: node not in graph")

    def list_nodes(self):
        try :
            return list(self.total_rep.keys())
        except :
            print("error")

    def remove_edge(self,node_from,node_to):
            del self.total_rep[node_from][node_to]

    def remove_node(self,node):
        if node in self.total_rep.keys() :
            del self.total_rep[node]
            try :
                for node_to in self.total_rep :
                    if node in self.total_rep[node_to].keys():
                        del self.total_rep[node_to][node]
                #del self.total_rep[node]
            except :
                return print("remove_node failed")
        else :
             print("remove_node failed: node not in graph")

#http://rebrained.com/?p=392
def shortestpath(graph,start,end,visited=[],distances={},predecessors={}): 
    """Find the shortest path between start and end nodes in a graph""" 
    # we've found our end node, now find the path to it, and return
    if start==end: 
        path=[] 
        while end != None: 
            path.append(end) 
            end=predecessors.get(end,None) 
        return distances[start], path[::-1] 
    # detect if it's the first time through, set current distance to zero
    if not visited: distances[start]=0 
    # process neighbors as per algorithm, keep track of predecessors
    for neighbor in graph[start]: 
        if neighbor not in visited: 
            neighbordist = distances.get(neighbor,sys.maxsize) 
            tentativedist = distances[start] + graph[start][neighbor] 
            if tentativedist < neighbordist: 
                distances[neighbor] = tentativedist 
                predecessors[neighbor]=start 
    # neighbors processed, now mark the current node as visited 
    visited.append(start) 
    # finds the closest unvisited node to the start 
    unvisiteds = dict((k, distances.get(k,sys.maxsize)) for k in graph if k not in visited) 
    closestnode = min(unvisiteds, key=unvisiteds.get) 
    # now we can take the closest node and recurse, making it current
    return shortestpath(graph,closestnode,end,visited,distances,predecessors)
     



if __name__ == "__main__":
    
    graph1 = {'a': {'w': 1, 'x': 1, 'y': 1}, 
            'b': {'w': 1, 'z': 1}, 
            'w': {'a': 1, 'b': 1, 'y': 1}, 
            'x': {'a': 1, 'y': 1, 'z': 1}, 
            'y': {'a': 1, 'w': 1, 'x': 1, 'z': 1}, 
            'z': {'b': 1, 'x': 1, 'y': 1}}
    
    a = graph("prac",graph1)
    print(shortestpath(a.list_graph(),'a','b'))
    print(a.list_graph())
    a.add_node(1)
    a.add_edge('a','b')
    print(a.list_graph())
    a.remove_node('a')
    print(a.list_graph())

            
